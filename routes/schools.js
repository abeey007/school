var express = require('express');
var passport = require('passport');
var router = express.Router();
require('../config/passport')(passport);
var schools = require('../app/controllers/schools.js');
var auth = require('../config/auth');


/* School API's */
router.post('/add-school', schools.addSchool);
router.post('/add-class', schools.addClass);
router.post('/searchschool', schools.searchSchool);
router.post('/schoolbyid', schools.schoolById);
router.post('/add-child-school', schools.addChildSchool);
router.post('/add-school-bus', schools.addSchoolBus);
router.get('/get-classes', schools.getClasses);
router.post('/get-all-comments', schools.getAllComments);

module.exports = router;
