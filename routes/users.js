var express = require('express');
var passport = require('passport');
var router = express.Router();
require('../config/passport')(passport);
var user = require('../app/controllers/users.js');
var auth = require('../config/auth');


router.get('/', function(req, res, next) {
  res.render('index.html');
});

router.get('/terms-and-conditions', function(req, res, next) {
  res.render('terms.html');
});

router.get('/privacy-policy', function(req, res, next) {
  res.render('privacy.html');
});

/* To register a user. */
router.post('/signup', user.signUp);
router.post('/login', user.login);
router.post('/otpverify', user.otpVerify);
router.post('/updateprofile', user.updateProfile);
router.post('/device-conflict-verify', user.deviceConflictVerify);

router.post('/sync-contacts', auth.authenticationMiddleware(), user.syncContacts);
router.post('/send-otp-verified-contacts', auth.authenticationMiddleware(), user.senOTPVerContact);
router.post('/final-otp-verify',  user.finalOtpVerify);
router.post('/invite-user', user.inviteUser);
router.post('/add-child', auth.authenticationMiddleware(), user.addChild);
router.post('/update-child', auth.authenticationMiddleware(), user.updateChild);
router.post('/firebase-add', user.firebaseAdd);


module.exports = router;
