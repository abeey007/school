var express = require('express');
var router = express.Router();
var practiceController = require('../app/controllers/practice.js');


/* To register a user. */
router.get('/async-map', practiceController.asyncMap);
router.get('/async-foreachof', practiceController.asyncForEach);
router.get('/async-parallel', practiceController.asyncForEach);


module.exports = router;
