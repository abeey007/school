var express = require('express');
var passport = require('passport');
var router = express.Router();
require('../config/passport')(passport);
var ratings = require('../app/controllers/ratings.js');
var auth = require('../config/auth');


/* School API's */
router.post('/add-ratings', ratings.addRatings);
router.post('/show-monthly-ratings', ratings.monthlyRatings);

module.exports = router;
