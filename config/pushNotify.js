var Promise = require("bluebird"); 
var formidable = require("formidable"); 
var usersObj = require('../app/models/users.js');
var constantObj = require('./constants.js');

var crypto = require('crypto');
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var fs = require('fs');
var apn = require('apn');
var FCM = require('fcm-push');
var twilio = require('twilio');
var nodemailer = require('nodemailer');
var smtp = require("nodemailer-smtp-transport");

var smtpTransport = nodemailer.createTransport(smtp({
    host: constantObj.gmailSMTPCredentials.host,
    secureConnection: constantObj.gmailSMTPCredentials.secure,
    port: constantObj.gmailSMTPCredentials.port,
    auth: {
        user: constantObj.gmailSMTPCredentials.username,
        pass: constantObj.gmailSMTPCredentials.password
    }
}));

var transporter = nodemailer.createTransport();


var sendSMS = function(mobileNo,code,message)
{
    var mobileNo = '+91' + mobileNo;
    console.log(mobileNo);
    var client = new twilio(constantObj.twilioCredentials.ACCOUNTSID, constantObj.twilioCredentials.AUTHTOKEN);
    client.messages.create({
       body: message,
       to: mobileNo,  // Text this number
       from: constantObj.twilioCredentials.TwilioNumber // From a valid Twilio number
    }, function(error, message) {
        // The HTTP request to Twilio will run asynchronously. This callback
        // function will be called when a response is received from Twilio
        // The "error" variable will contain error information, if any.
        // If the request was successful, this value will be "falsy"
        if (!error) {
            // The second argument to the callback will contain the information
            // sent back by Twilio for the request. In this case, it is the
            // information about the text messsage you just sent:
            console.log('Success! The SID for this SMS message is:');
            console.log(message.sid);
            
            console.log('Message sent on:');
            console.log(message.dateCreated);
            return true;
        } else {
            console.log('Oops! There was an error.',error);
            return false;
        }
    });
}


var sendEmail = function(mobileNo)
{
    var mailOptions = {
        from: constantObj.gmailSMTPCredentials.username,
        to: constantObj.gmailSMTPCredentials.username,
        subject: 'New User Registration',
        html: 'Hello,<br><br>' +
              'This mobile no is registered. ' + mobileNo + '<br><br>'
    };
    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: ' + info.response);
        return true;
    });
       
}


var iosPushFunction = function (deviceToken,alert,payload,pushStatus) {
    console.log("wwwwwwwwwwwwww",pushStatus);
    if(pushStatus==true){
        var apnProvider = new apn.Provider({  
             token: {
                key: './APNsAuthKey_9UK286X3S4.p8', // Path to the key p8 file
                keyId: '9UK286X3S4', // The Key ID of the p8 file (available at https://developer.apple.com/account/ios/certificate/key)
                teamId: 'EX3FW9CN2B', // The Team ID of your Apple Developer Account (available at https://developer.apple.com/account/#/membership/)
            },
            production: true // Set to true if sending a notification to a production iOS app
        });

        // Enter the device token from the Xcode console
        var deviceToken = deviceToken;

        // Prepare a new notification
        var notification = new apn.Notification();

        // Specify your iOS app's Bundle ID (accessible within the project editor)
        notification.topic = 'com.referralMax';

        // Set expiration to 1 hour from now (in case device is offline)
        notification.expiry = Math.floor(Date.now() / 1000) + 3600;

        // Set app badge indicator
        notification.badge = 1;

        // Play ping.aiff sound when the notification is received
        notification.sound = 'ping.aiff';

        // Display the following message (the actual notification text, supports emoji)
        notification.alert = alert;

        // Send any extra payload data with the notification which will be accessible to your app in didReceiveRemoteNotification
        notification.payload = {id: payload,'content-available':1};

        // Actually send the notification
        apnProvider.send(notification, deviceToken).then(function(result) {  
            // Check the result for any failed devices
            console.log(result);
        });
    }    
};


var androidPushFunction = function (deviceToken,message,pushStatus,key) {

    if(pushStatus==true){
        var serverKey = 'AAAAnAB3eN8:APA91bFyPhXGji6Kbfobjie6Ka4ie2Kn4nv5O2OmXAQhkkthJlXHysRuJQLElMC-eYMKdUxqfkLCMg8FnL45RWBNO4NPA5_TVM0ZdieJ2IDzdnewwXQJy-INvU_KB-jPpuyziR9qkr_b';
        var fcm = new FCM(serverKey);

        var message = {
            to: deviceToken, // required fill with device token or topics
            notification: {
                title: 'Referral Max',
                body: message
            },
            data : {
             title: 'Referral Max',
             body : message,
             key: key
           }
        };

        //callback style
        fcm.send(message, function(err, response){
            if (err) {
                console.log("android",err);
            } else {
                console.log("Successfully sent with response: ", response);
            }
        });

    }
   
   
};




exports.sendSMS = sendSMS;
exports.sendEmail = sendEmail;
exports.iosPushFunction = iosPushFunction;
exports.androidPushFunction = androidPushFunction;
