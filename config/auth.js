var users = require('../app/models/users');
exports.authenticationMiddleware = function () {
    return function (req, res, next) {
        if (req.headers && req.headers.authorization) {
            var parted = req.headers.authorization.split('.');
            users.findOne({mobileNo: parted[0],deviceId: parted[1]}, function (err, userData) { 

                   if(userData){
                        return next();
                   }else{
                        res.jsonp({status: 501, msg: "Headers not matched."});
                   }
                 
            });
        } else {
            res.jsonp("No headers found");
        }
    };
};

//        if (req.isAuthenticated()) {
//          return next();
//        } else{
//            res.jsonp("Oops ! Something went wrong.");
//        }