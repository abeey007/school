
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/schoolapp', { useMongoClient: true });

//check if we are connected successfully or not
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));

db.once('open',function callback(){
    console.log("Mongodb is connected");
});
