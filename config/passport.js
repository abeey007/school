var JwtStrategy = require('passport-jwt').Strategy;
// load up the user model
var users = require('../app/models/users');
var config = require('./passport_config'); // get passport config file
 
module.exports = function(passport) {
  var opts = {};
  opts.secretOrKey = config.secret;
  opts.exp = '1s';
  passport.use('jwt',new JwtStrategy(opts, function(jwt_payload, done) {
    users.findOne({id: jwt_payload.id}, function(err, user) {
          if (err) {
              return done(err, false);
          }
          if (user) {
//              console.log("Success",user);
              done(null, user);
          } else {
              done(null, false);
          }
      });
  }));
};