const messages = {
        "errorRetreivingData": "Error occured while retreiving the data from collection.",
        "errorVerification": "Error occured while fetching the data.",
        "successVerification": "Saved successfully.",
        "UpdateSuccess": "Updated successfully.",
        "UpdateFailure": "Error occured while updating.",
        "DeleteFailure": "Error occured while deleting. ",
        "DeleteSuccess":"Deleted successfully.",
        "successSendingForgotPasswordEmail": "Password sent successfully."
        }

const gmailSMTPCredentials = {
        "type": "SMTP",
        "service": "Gmail",
        "host": "smtp.gmail.com",
        "port": 587,
        "secure": false,
        "username": "wildnetschoolapp@gmail.com",
        "password": "wildnet1234"
}

const facebookCredentials = {
        "app_id" : "",
        "secret":"",
        "token_secret": process.env.token_secret || 'JWT Token Secret'
}

const twitterCredentials = {
        "consumer_key" : "",
        "consumer_secret" : ""
        }

const googleCredentials = {
        "client_secret_key" : ""
        }

const twilioCredentials = {
    "TwilioNumber" : "+19286083018",
    "ACCOUNTSID"   : "AC40df27e00a6b27207a7818ae15a28d30",
    "AUTHTOKEN"    : "5be465831bcd264969c6cdd1a0c8774e"
}

const contact = {
    "useremail" : "abhijeet@wildnettechnologies.com",
}

const appLinks = {
    "ANDROID" : "https://play.google.com/store/apps/details?id=com.wildnet.guardiansvoice",
    "IOS" : "dummy link",
}


const imagePaths = {
    "user": "/../../public/images/user/avatar/original",
    "userResize": "/../../public/images/user/avatar/thumbnail",
    "url": "/images/user/avatar/original",   
    "offer": "/../../public/images/offer",
    "offerurl": "/images/offer"
}

var obj = {messages:messages, gmailSMTPCredentials:gmailSMTPCredentials, facebookCredentials:facebookCredentials, twitterCredentials : twitterCredentials, googleCredentials : googleCredentials, twilioCredentials: twilioCredentials,imagePaths: imagePaths,contact: contact,appLinks:appLinks};
        module.exports = obj; 