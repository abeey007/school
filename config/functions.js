/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Promise = require("bluebird"); 
var formidable = require("formidable"); 
var usersObj = require('../app/models/users.js');
var ratingObj = require('../app/models/ratings.js');
var schoolObj = require('../app/models/schools.js');
var ratingObj = require('../app/models/ratings.js');

var crypto = require('crypto');
var nodemailer = require('nodemailer');
var smtp = require("nodemailer-smtp-transport");
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var fs = require('fs');
var moment = require('moment');
var mongoose = require('mongoose');
var firebase = require('firebase');
var config = {
    apiKey: "AIzaSyDxJTZMsjc8U5gjrwpTEzIm2RhSFtJ18P4",
    authDomain: "school-app-87383.firebaseapp.com",
    databaseURL: "https://school-app-87383.firebaseio.com",
    projectId: "school-app-87383",
    storageBucket: "",
    messagingSenderId: "401014923215"
};
firebase.initializeApp(config);
var ref = firebase.app().database().ref();

var transporter = nodemailer.createTransport();

// smtp settings

mongoose.set('debug', true);
var endOfCurrentMonth   = moment().endOf('month').format('YYYY-MM-DD');
var startDate = moment().format('YYYY')-1+"-"+"07-01";
var endOfLastMonth = moment().subtract(1,'months').endOf('month').format('YYYY-MM-DD');


/*________________________________________________________________________
 * @Date:      	08 Feb,2017
 * @Method :   	getMatchedContacts
 * Created By: 	Abhijeet Singh
 * Modified On:	-
 * @Purpose:   	This function is to give all matched contacts.
 _________________________________________________________________________
 */


var getMatchedContacts = function (mobile) {

      return new Promise(function (resolve, reject) {
        usersObj.findOne({
            'mobileNo': mobile
        }, function (error, userData) {
            if (error) {
                reject(error);
                return;
            }
            
            if(userData){
              resolve({
                  mobileNo: userData.mobileNo,
                  first_name: userData.first_name,
                  last_name: userData.last_name,
                  status: "matched"
              });
            }else{
              resolve();
            }
        });
    });

};




var getAllSchoolData = function (schooldata) {

      return new Promise(function (resolve, reject) {
          console.log("last date",startDate);
          var conditions = [
              { "$match": { 
                            "school_id": mongoose.Types.ObjectId(schooldata._id),
                            "date" : {
                                    "$gte" : startDate+"T00:00:00.000Z", 
                                    "$lte" : endOfCurrentMonth+"T00:00:00.000Z" 
                                 } 
                          } 
              },
              { "$group": { 
                  "_id": "$rating_code",
                  "rating_number": { $avg: "$rating_number" }
              }}
          ];
          ratingObj.aggregate(conditions, function (error, schoolRatingsTillNow) {
              if (error) {
                  reject(error);
                  return;
              }

              var conditionLastMonth = [
                  { "$match": { 
                                "school_id": mongoose.Types.ObjectId(schooldata._id),
                                "date" : {
                                        "$gte" : startDate+"T00:00:00.000Z", 
                                        "$lte" : endOfLastMonth+"T00:00:00.000Z" 
                                     } 
                              } 
                  },
                  { "$group": { 
                      "_id": "$rating_code",
                      "rating_number": { $avg: "$rating_number" }
                  }}
              ];

              ratingObj.aggregate(conditionLastMonth, function (error, schoolRatingsTillLast) {
                    if (error) {
                        reject(error);
                        return;
                    }
                    

                    resolve({
                        schooldata:schooldata,
                        schoolRatings: { lastMonth: schoolRatingsTillLast,currentMonth:schoolRatingsTillNow}
                        
                    });
                    
                });
              
          });
    });

};



var getMatchedComments = function (value) {

      return new Promise(function (resolve, reject) {
        ratingObj.find({
            'comment_id': mongoose.Types.ObjectId(value._id),
        }, function (error, userData) {
            if (error) {
                reject(error);
                return;
            }
            
            if(userData){
              resolve(userData);
            }else{
              resolve();
            }
        });
    });

};


var createCommunity = function (user,verifiedUserData) {

    if(user.child_comunity_code){ 

      ref.child(user.child_comunity_code).once('value', function(snapshot) {
          var exists = (snapshot.val() !== null);
          console.log(exists);
          if(exists == false)
          {
              console.log(verifiedUserData);
              var usersRef = ref.child(user.child_comunity_code);
              var userRef = usersRef.push({
                   mobileNo: verifiedUserData.mobileNo,
                   first_name: verifiedUserData.first_name,
                   last_name: verifiedUserData.last_name,
                   childfirst_name: user.childfirst_name,
                   childlast_name: user.childlast_name,
                   profileImg: verifiedUserData.profileImg,
                   moderator: true
              });

          }else{

              var usersRef = ref.child(user.child_comunity_code);
              var userRef = usersRef.push({
                   mobileNo: verifiedUserData.mobileNo,
                   first_name: verifiedUserData.first_name,
                   last_name: verifiedUserData.last_name,
                   childfirst_name: user.childfirst_name,
                   childlast_name: user.childlast_name,
                   profileImg: verifiedUserData.profileImg,
                   moderator: false
              });

          }
      });

    } 
    

    if(user.child_bus_comunity_code){ 
      
      ref.child(user.child_bus_comunity_code).once('value', function(snapshot) {
          var exists = (snapshot.val() !== null);
          console.log("check bus",exists);
          if(exists == false)
          {
              console.log(verifiedUserData);
              var busRef = ref.child(user.child_bus_comunity_code);
              var busRef = busRef.push({
                   mobileNo: verifiedUserData.mobileNo,
                   first_name: verifiedUserData.first_name,
                   last_name: verifiedUserData.last_name,
                   childfirst_name: user.childfirst_name,
                   childlast_name: user.childlast_name,
                   profileImg: verifiedUserData.profileImg,
                   moderator: true
              });

          }else{

              var busRef = ref.child(user.child_bus_comunity_code);
              var busRef = busRef.push({
                   mobileNo: verifiedUserData.mobileNo,
                   first_name: verifiedUserData.first_name,
                   last_name: verifiedUserData.last_name,
                   childfirst_name: user.childfirst_name,
                   childlast_name: user.childlast_name,
                   profileImg: verifiedUserData.profileImg,
                   moderator: false
              });

          }
      }); 

   }    

};

var updateCommunity = function (user,doc) {

      if(user.child_comunity_code!=user.child_old_comunity_code)
        {
            ref.child(user.child_comunity_code).orderByChild('user_id').equalTo(user.mobileNo).on('child_added', (snapshot) => {
                 snapshot.ref.remove()
            });
            
            ref.child(user.child_comunity_code).once('value', function(snapshot) {
                var exists = (snapshot.val() !== null);
                console.log(exists);
                if(exists == false)
                {
                    var usersRef = ref.child(user.child_comunity_code);
                    var userRef = usersRef.push({
                         mobileNo: doc.mobileNo,
                         first_name: doc.first_name,
                         last_name: doc.last_name,
                         childfirst_name: user.childfirst_name,
                         childlast_name: user.childlast_name,
                         profileImg: doc.profileImg,
                         moderator: true
                    });

                }else{

                    var usersRef = ref.child(user.child_comunity_code);
                    var userRef = usersRef.push({
                         mobileNo: doc.mobileNo,
                         first_name: doc.first_name,
                         last_name: doc.last_name,
                         childfirst_name: user.childfirst_name,
                         childlast_name: user.childlast_name,
                         profileImg: doc.profileImg,
                         moderator: false
                    });

                }
            });


        }


        if(user.child_bus_comunity_code!=user.child_bus_old_comunity_code)
        {
            ref.child(user.child_bus_comunity_code).orderByChild('user_id').equalTo(user.mobileNo).on('child_added', (snapshot) => {
                 snapshot.ref.remove()
            });
            
            ref.child(user.child_bus_comunity_code).once('value', function(snapshot) {
                var exists = (snapshot.val() !== null);
                console.log(exists);
                if(exists == false)
                {
                    var busRef = ref.child(user.child_bus_comunity_code);
                    var busRef = busRef.push({
                         mobileNo: doc.mobileNo,
                         first_name: doc.first_name,
                         last_name: doc.last_name,
                         childfirst_name: user.childfirst_name,
                         childlast_name: user.childlast_name,
                         profileImg: doc.profileImg,
                         moderator: true
                    });

                }else{

                    var busRef = ref.child(user.child_bus_comunity_code);
                    var busRef = busRef.push({
                         mobileNo: doc.mobileNo,
                         first_name: doc.first_name,
                         last_name: doc.last_name,
                         childfirst_name: user.childfirst_name,
                         childlast_name: user.childlast_name,
                         profileImg: doc.profileImg,
                         moderator: false
                    });

                }
            });


        }

};


var insertSchoolComment = function (ratingReq) {

     return new Promise(function (resolve, reject) {

     var subDocumentId = mongoose.Types.ObjectId();
     var fields = { 
          "comments": { 
              _id : subDocumentId,
              mobileNo: ratingReq.mobileNo,
              comment: ratingReq.comment,
              comment_title: ratingReq.comment_title
            } 
        };
     schoolObj.update({"_id": mongoose.Types.ObjectId(ratingReq.school_id)}, {$push: fields}, function (error, commentUpdateData) {
              if (error) {
                  reject(error);
                  return;
              }
              
              if(commentUpdateData){
                resolve({
                    subDocumentId: subDocumentId
                });
              }else{
                resolve();
              }
          });
    });

};



exports.getMatchedContacts = getMatchedContacts;
exports.getAllSchoolData = getAllSchoolData;
exports.createCommunity = createCommunity;
exports.updateCommunity = updateCommunity;
exports.insertSchoolComment = insertSchoolComment;
exports.getMatchedComments = getMatchedComments;



