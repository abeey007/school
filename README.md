Referral Pal 
------------------

################################################### Prerequisites #####################################################
Make sure you have installed all these prerequisites on your development machine.

##Node.js v6.2.2
    sudo curl https://raw.githubusercontent.com/creationix/nvm/v0.30.1/install.sh | bash
    source ~/.bashrc
    nvm install v6.2.2
    nvm alias default v6.2.2
    
##MongoDB v3.2
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
sudo apt-get update
sudo apt-get install -y mongodb-org
    
    
################################################# Repositery #######################################################

sudo apt-get install git

git clone git@bitbucket.org:algoworks-dev/snatchbackend.git
OR
git clone https://algoworks-anil-jangra@bitbucket.org/algoworks-dev/snatchbackend.git


##install pm2
npm install pm2 -g

To install Node.js dependencies,run this inside application folder in the command-line:
$ npm install

