/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');
var moment = require('moment');
var currDate = function(){
    return moment().format('DD MMM,YYYY');
};

var classesSchema = new mongoose.Schema({
    className: {type: String},
});

var classes= mongoose.model('classes', classesSchema);

module.exports = classes;