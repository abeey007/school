/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');
var moment = require('moment');
var currDate = function(){
    return moment().format('YYYY-MM-DD');
};

var ratingsSchema = new mongoose.Schema({
    school_id: {type:mongoose.Schema.Types.ObjectId},
    class_name: {type: String},
    section_name: {type: String},
    mobileNo: {type: String},
    comment_id: {type:mongoose.Schema.Types.ObjectId},
    rating_code: {type: String},
    rating_number: {type: Number},
    date:{type: String, default: currDate},
    createdDate:{type:Date, default: currDate},
});

var ratings= mongoose.model('ratings', ratingsSchema);

module.exports = ratings;