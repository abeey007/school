/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var uniqueValidator = require('mongoose-unique-validator');

SALT_WORK_FACTOR = 10;
var userSchema = new mongoose.Schema({
    mobileNo: {type: Number,required : 'Please enter the mobile no.', unique: true},
    email: {type: String},
    age: {type: Number},
    occupation: {type: String},
    deviceToken: {type : String},
    deviceId: {type : String},
    first_name: {type : String},
    last_name: {type : String},
    profileImg: {type : String},
    pushStatus:{
        type: Boolean,
        default: true
    },
    address: {
        address:{type:String},
        city: {type:String},
        state: {type:String},
        zipcode: {type:String},
        country:{type:String},
        location: {type: String}
    },
    userStatus: { 
        type: Boolean, 
        'default': true 
    },
    verificationToken: {type: String},
    verifyMobile: {
        code: {type: String},
        isverify: {type: Boolean, default: false}    
    },
    verifyUser: {
        code: {type: String},
        isverify: {type: Boolean, default: false}    
    },
    createdDate:{type:Date, default: Date.now},
    updatedAt: {type:Date, default: Date.now},
    isDeleted: { 
        type: Boolean, 
        'default': false 
    },
    childrens : [ 
        { 
          first_name : {type: String},
          last_name : {type: String},
          school_id : {type:mongoose.Schema.Types.ObjectId , ref: 'schools'},
          school_address : {type: String},
          comunity_code : {type: String},
          bus_comunity_code : {type: String},
        }  
      ]
});

userSchema.plugin(uniqueValidator);
userSchema.plugin(uniqueValidator, {message: "Mobile no. already exists"});
var users= mongoose.model('users', userSchema);

module.exports = users;