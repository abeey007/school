/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');
var moment = require('moment');
var currDate = function(){
    return moment().format('YYYY-MM-DD');
};

var schoolsSchema = new mongoose.Schema({
    schoolName: {type: String},
    schoolImage: {type: String},
    schoolWebsite: {type: String},
    schoolContactInfo: {type: String},
    about_school: {type: String},
    address: {type: String},
    city: {type: String,},
    state: {type: String,},
    country: {type: String},
    zipcode: {type: String},
    loc: {
        type: [Number],  // [<longitude>, <latitude>]
        index: '2d'      // create the geospatial index
    },
    additional_info: {type: String},
    classes : [ 
        { 
          class_name : {type: String},
          sections : [ 
                { 
                  section_name : {type: String}
                }  
            ]
        }  
      ],
    buses : [ 
        { 
          bus_no : {type: String},
        }  
      ],
    comments : [ 
        { 
          mobileNo: {type:String, ref: 'users'},
          comment_title : {type: String},
          comment : {type: String},
          rating : [],
          createdDate:{type:String, default: currDate}
        }  
      ],  
    createdDate:{type:Date, default: currDate},
    isDeleted: { 
        type: Boolean, 
        'default': false 
    }
});

var schools= mongoose.model('schools', schoolsSchema);

module.exports = schools;