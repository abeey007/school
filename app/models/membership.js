/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');

var membershipSchema = new mongoose.Schema({
	_id: {type:mongoose.Schema.Types.ObjectId},
    plan_name: {type: String},
    amount: {type: Number},
    offers_limit: {type: Number},
    refferer_limit: {type: Number},
    duration: {type: String},
    sowner_limit: {type: Number},
    history: {type: String},
    other_details: {type: String},
    other_details_2: {type: String},
    plan_type: {type: String},
    
    isDeleted: { 
        type: Boolean, 
        'default': false 
    }
});

var membership = mongoose.model('membership', membershipSchema);

module.exports = membership;