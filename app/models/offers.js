/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var mongoose = require('mongoose');
var moment = require('moment');
var currDate = function(){
    return moment().format('DD MMM,YYYY');
};

var offersSchema = new mongoose.Schema({
    offerName: {type: String},
    offerImage: {type: String,default: 'http://35.154.148.17:3000/images/add-new-offer-1.png'},
    offerAmount: {type: Number},
    startDate: {type: String, default: currDate},
    endDate: {type: String, default: currDate},
    description: {type: String},
    offerImages: [{
        image: {type: String}
    }],
    publishStatus: {type: String, default: "unpublished"},
    offerVideos: [{
        video: {type: String}
    }],
    discount: {type: Number},
    discount_description: {type: String},
    remuneration: {type: Number},
    remuneration_description: {type: String},
    offerCategory: {type: String},
//    offerStartTime: {type: Date},
//    offerEndTime: {type: Date},
    storeLocator: {type: String},
    termsConditions: {type: String},
    
    userId:{type:mongoose.Schema.Types.ObjectId, ref: 'users'},
    salesUserId:{type:mongoose.Schema.Types.ObjectId, ref: 'users'},
    createdDate:{type:Date, default: Date.now},
    updatedAt: {type:Date, default: Date.now},
    isDeleted: { 
        type: Boolean, 
        'default': false 
    }
});

var offers= mongoose.model('offers', offersSchema);

module.exports = offers;