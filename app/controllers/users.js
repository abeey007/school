/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var formidable = require("formidable");
var usersObj = require('../models/users.js');
var constantObj = require('../../config/constants.js');
var passport = require('../../config/passport.js');
var cfg = require('../../config/passport_config.js');
var pushNotifyObj = require('../../config/pushNotify.js');
var functionsObj = require('../../config/functions.js');

var nodemailer = require('nodemailer');
var smtp = require("nodemailer-smtp-transport");
var crypto = require('crypto');
var async = require("async");
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var fs = require('fs');
var formidable = require('formidable');
var mongoose = require('mongoose');
var moment = require('moment');
var otp = require('otplib/lib/totp');


var transporter = nodemailer.createTransport();



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    login
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to authenticate user.
 _________________________________________________________________________
 */

var login = function (req, res) {
    
    Promise.resolve().then(function() {
        var secret = otp.utils.generateSecret();
        return otp.generate(secret);
    }).then(function(otpKey) {
            usersObj.findOne({mobileNo: req.body.mobileNo}).populate('childrens.school_id').exec(function (err, data) {
                if (err) {
                    outputJSON = {status: 203, msg: "Please enter a valid mobile no."};
                    res.jsonp(outputJSON);
                } else {
                    console.log(data);
                    if (data) {
                         
                         if(req.body.mobileNo == data.mobileNo && req.body.deviceId == data.deviceId)
                           {
                                if(data.verifyMobile.isverify == false){

                                    var message = data.verifyMobile.code + ' : use this otp code to verify your account.'
                                    pushNotifyObj.sendSMS(data.mobileNo,data.verifyMobile.code,message);
                                    
                                    if(typeof data.first_name=="undefined")
                                    {
                                       res.jsonp({status: 302, msg: 'device id changed',data:data,page:"register_next_screen"}); 
                                    }else{
                                        res.jsonp({status: 301, msg: 'device id changed',data:data,page:"single_otp_screen"});
                                    }

                                    
                                    //res.jsonp({status: 302, msg: "Your mobile no. is not verified yet.",page:"register_next_screen"});
                                } else{
                                    res.jsonp({status: 200, message: "You have logged in successfully", data: data});

                                }
                           }else{

                                Promise.resolve().then(function() {
                                     var message = otpKey + ' : use this otp code to verify your account.'
                                     return pushNotifyObj.sendSMS(req.body.mobileNo,otpKey,message);
                                }).then(function(responseMobile) {
                                        console.log(responseMobile);
                                        var fields = {
                                            verifyMobile:{
                                                isverify: false,
                                                code: otpKey,
                                            },
                                            verifyUser: {
                                                isverify: false   
                                            }
                                        };

                                        usersObj.findOneAndUpdate({mobileNo: req.body.mobileNo}, {$set: fields}, {new: true}).populate('childrens.school_id').exec( function (err, updatedData) {
                                            if (err) {
                                                console.log("aaaaaaaaaa",err);
                                                res.jsonp(err);
                                            } else {

                                                if(data.first_name=="")
                                                {
                                                   res.jsonp({status: 302, msg: 'device id changed',data:updatedData,page:"register_next_screen"}); 
                                                }else{
                                                    res.jsonp({status: 301, msg: 'device id changed',data:updatedData,page:"single_otp_screen"});
                                                }

                                                //res.jsonp({status: 301, msg: 'device id changed',data:updatedData,page:"single_otp_screen"});
                                            }
                                        });
                                  });
                               
                           }
                         
                    }else{
                       console.log(otpKey);
                       var user = {};
                       user = req.body.usersObj || req.body; 
                       user.verifyMobile = {};
                       user.verifyMobile.code = otpKey;
                       usersObj(user).save(function (err, data) {
                            if (err) {
                                //console.log("aaaa",err.name); 
                                switch (err.name) {
                                    case 'ValidationError':
                                        for (field in err.errors) {
                                            console.log("bbbbb",err.errors);
                                            if (err.errors[field].path === 'mobileNo') {
                                                errorMessage = 'This is not a valid mobile no. or this mobile is already registered.';
                                            }
                                            
                                        }//for
                                        break;
                                }//switch
                                res.jsonp({status: 201, msg: errorMessage});
                            } else {
                                console.log("success");
                                Promise.resolve().then(function() {
                                    var message = otpKey + ' : use this otp code to verify your account.'
                                     return pushNotifyObj.sendSMS(req.body.mobileNo,otpKey,message);
                                  }).then(function(responseMobile) {
                                                var mailOptions = {
                                                       from: constantObj.gmailSMTPCredentials.username,
                                                        to: constantObj.contact.useremail,
                                                        subject: 'New User Registration',
                                                        html: 'Hello,<br><br>' +
                                                              'This mobile no is registered. ' + req.body.mobileNo + '<br><br>'
                                                };
                                                // send mail with defined transport object
                                                transporter.sendMail(mailOptions, function (error, info) {
                                                    if (error) {
                                                        return console.log(error);
                                                    }

                                                    console.log('Message sent: ' + info.messageId);

                                                });

                                       res.jsonp({status: 302, msg: "User registered successfully. OTP sent on your mobile no.",data:user,page:"register_next_screen"});
                                  });     
                                
                            }
                        });
                        
                    }
                }
            });
    });
};



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    signUp
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to register new user.
 _________________________________________________________________________
 */


var signUp = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    user.verifyMobile = {};
    errorMessage = "";
    Promise.resolve().then(function() {
        var secret = otp.utils.generateSecret();
        return otp.generate(secret);
    }).then(function(otpKey) {
            user.verifyMobile.code = otpKey;
            usersObj(user).save(function (err, data) {
                if (err) {
                    //console.log("aaaa",err.name); 
                    switch (err.name) {
                        case 'ValidationError':
                            for (field in err.errors) {
                                console.log("bbbbb",err.errors);
                                if (err.errors[field].path === 'mobileNo') {
                                    errorMessage = 'This is not a valid mobile no. or this mobile is already registered.';
                                }
                                
                            }//for
                            break;
                    }//switch
                    res.jsonp({status: 201, msg: errorMessage});
                } else {
                    console.log("success");
                    Promise.resolve().then(function() {
                        var message = otpKey + ' : use this otp code to verify your account.'
                         return pushNotifyObj.sendSMS(req.body.mobileNo,otpKey,message);
                      }).then(function(responseMobile) {
                                    var mailOptions = {
                                           from: constantObj.gmailSMTPCredentials.username,
                                            to: constantObj.contact.useremail,
                                            subject: 'New User Registration',
                                            html: 'Hello,<br><br>' +
                                                  'This mobile no is registered. ' + req.body.mobileNo + '<br><br>'
                                    };
                                    // send mail with defined transport object
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            return console.log(error);
                                        }

                                        console.log('Message sent: ' + info.messageId);

                                    });

                           res.jsonp({status: 302, msg: "User registered successfully. OTP sent on your mobile no.",data:data,page:"register_next_screen"});
                      });     
                    
                }
            });


    });   


    
};


/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    otpverify
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to verify otp.
 _________________________________________________________________________
 */


var otpVerify = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    errorMessage = "";
    Promise.resolve().then(function() {
       return usersObj.findOne({mobileNo: user.mobileNo}).populate("childrens.school_id");
    }).then(function(userData) {
        console.log(userData);
        if(userData){
            if(userData.verifyMobile.code == user.code)
            {
                var urlImage = "";
                if(req.body.file){
                    var bitmap = new Buffer(req.body.file, 'base64');
                    // write buffer to file
                    //console.log(req.body.file.filename);
                    var name = Math.floor(Date.now() / 1000).toString() + '.' + 'png';
                    fs.writeFileSync(name, bitmap);
                    uploadDir = __dirname + constantObj.imagePaths.user;
                    
                    fs.renameSync(name, uploadDir + "/" + name);
                    var urlImage = 'http://' + req.headers.host + constantObj.imagePaths.url+ '/' + name; 
                    console.log("dynamic url:::",urlImage);
                }

                 var fields = {
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    email: req.body.email,
                    age: req.body.age,
                    occupation: req.body.occupation,
                    profileImg:urlImage,
                    verifyMobile:{
                        isverify: true,
                        code: user.code,
                    }
                };

                usersObj.findOneAndUpdate({mobileNo: user.mobileNo}, {$set: fields}, {new: true}).populate('childrens.school_id').exec( function (err, updatedData) {
                    if (err) {
                        console.log("aaaaaaaaaa",err);
                        res.jsonp(err);
                    } else {
                        res.jsonp({status: 200, msg: 'updated successfully.', data: updatedData});
                    }
                });

            }else{
                res.jsonp({status: 201, msg: "You have entered wrong OTP."});  
            }

        }else{
            res.jsonp({status: 201, msg: "This mobile no. not exist."});  
        }
    });   


    
};



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    deviceConflictVerify
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to deviceConflictVerify otp.
 _________________________________________________________________________
 */


var deviceConflictVerify = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    errorMessage = "";
    Promise.resolve().then(function() {
       return usersObj.findOne({mobileNo: user.mobileNo}).populate("childrens.school_id");
    }).then(function(userData) {
        console.log(userData);
        if(userData){
            if(userData.verifyMobile.code == user.code)
            {
                 var fields = {
                    deviceId: user.deviceId,
                    verifyMobile:{
                        isverify: true,
                        code: user.code,
                    }
                };

                usersObj.findOneAndUpdate({mobileNo: user.mobileNo}, {$set: fields},{new: true}).populate('childrens.school_id').exec( function (err, updatedUser) {
                    if (err) {
                        console.log("aaaaaaaaaa",err);
                        res.jsonp(err);
                    } else {
                        res.jsonp({status: 200, msg: 'updated successfully.', data: updatedUser});
                    }
                });

            }else{
                res.jsonp({status: 201, msg: "You have entered wrong OTP."});  
            }

        }else{
            res.jsonp({status: 201, msg: "This mobile no. not exist."});  
        }
    });   
    
};


var syncContacts = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    console.log("this is array", user);
    usersObj.find({mobileNo: { "$in": req.body.data }}).populate('childrens.school_id').exec(function (err, findData) {
        if (err) {
            console.log("aaaaaaaaaa",err);
            res.jsonp(err);
        } else {
            res.jsonp({status: 200,msg:"contacts result", data: findData});
        }
    });

};


var senOTPVerContact = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
   
    Promise.resolve().then(function() {
       return usersObj.findOne({mobileNo: user.verifiedMobileNo}).populate("childrens.school_id");
    }).then(function(verifiedUserData) {
        console.log(verifiedUserData);
        if(verifiedUserData){
            Promise.resolve().then(function() {
                var secret = otp.utils.generateSecret();
                return otp.generate(secret);
            }).then(function(otpKey) {

                Promise.resolve().then(function() {
                    var message = otpKey + ' : use this otp code to verify your account.'
                     return pushNotifyObj.sendSMS(verifiedUserData.mobileNo,otpKey,message);
                }).then(function(responseMobile) {
                        console.log(responseMobile);
                        var fields = {
                            verifyUser:{
                                isverify: false,
                                code: otpKey,
                            }
                        };

                        usersObj.findOneAndUpdate({mobileNo: req.body.mobileNo}, {$set: fields},{new: true}).populate('childrens.school_id').exec( function (err, updatedData) {
                            if (err) {
                                console.log("aaaaaaaaaa",err);
                                res.jsonp(err);
                            } else {
                                res.jsonp({status: 200, msg: 'OTP sent to user',data:updatedData,page:"2nd_verification_otp_screen"});
                            }
                        });
                  });

            });

            
        }else{
            res.jsonp({status: 201, msg: "This mobile no. not exist."});  
        }
    });

};


/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    finalOtpVerify
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to verify otp.
 _________________________________________________________________________
 */


var finalOtpVerify = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    errorMessage = "";
    console.log(user);
    Promise.resolve().then(function() {
       return usersObj.findOne({mobileNo: user.mobileNo}).populate("childrens.school_id");
    }).then(function(userData) {
        console.log(userData);
        if(userData){
            if(userData.verifyUser.code == user.code)
            {
                 var fields = {
                    verifyMobile:{
                        isverify: true,
                    },
                    verifyUser: {
                        isverify: true   
                    }
                };

                usersObj.findOneAndUpdate({mobileNo: user.mobileNo}, {$set: fields},{new: true}).populate('childrens.school_id').exec( function (err, updatedUserData) {
                    if (err) {
                        console.log("aaaaaaaaaa",err);
                        res.jsonp(err);
                    } else {
                        res.jsonp({status: 200, msg: 'updated successfully.', data: updatedUserData});
                    }
                });

            }else{
                res.jsonp({status: 201, msg: "You have entered wrong OTP."});  
            }

        }else{
            res.jsonp({status: 201, msg: "This mobile no. not exist."});  
        }
    });   


    
};



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    addChild
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to add child.
 _________________________________________________________________________
 */


var addChild = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    errorMessage = "";
    console.log(user);

    var fields = { "childrens": { first_name: user.childfirst_name, 
                                  last_name: user.childlast_name,
                                  school_id: user.child_school_id, 
                                  school_address: user.child_school_address, 
                                  comunity_code: user.child_comunity_code,
                                  bus_comunity_code: user.child_bus_comunity_code
                                } 
                 };
    
    usersObj.update({mobileNo: user.mobileNo}, {$push: fields}, function (err, userData) {
        if (err) {
            console.log("aaaaaaaaaa",err);
            res.jsonp(err);
        } else {
            Promise.resolve().then(function() {
               return usersObj.findOne({mobileNo: user.mobileNo}).populate("childrens.school_id");
            }).then(function(verifiedUserData) {
                console.log(verifiedUserData);
                functionsObj.createCommunity(user,verifiedUserData);
                res.jsonp({status: 200, msg: 'updated successfully.', data: verifiedUserData});
            }); 
            
        }
    });      


    
};


/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    UpdateChild
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to update child.
 _________________________________________________________________________
 */


var updateChild = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    errorMessage = "";
    console.log(user);
    
    usersObj.findOneAndUpdate(
        { "mobileNo": user.mobileNo, "childrens._id": user.child_id },
        { 
            "$set": {
                "childrens.$.first_name": user.childfirst_name,
                "childrens.$.last_name": user.childlast_name,
                "childrens.$.school_id": user.child_school_id,
                "childrens.$.school_address": user.child_school_address, 
                "childrens.$.comunity_code": user.child_comunity_code,
                "childrens.$.bus_comunity_code": user.child_bus_comunity_code,
            }
        },{new:true})
        .populate('childrens.school_id').exec(
            function(err,doc) {
                if(err){
                    console.log("aaaaaaaaaa",err);
                    res.jsonp(err);
                }

                usersObj.findOne({ "mobileNo": user.mobileNo, "childrens._id": user.child_id }).populate('childrens.school_id').exec(function (err, findData) {
                    if (err) {
                        console.log("aaaaaaaaaa",err);
                        res.jsonp(err);
                    } else {
                        functionsObj.updateCommunity(user,doc);
                        console.log("do we get new data",JSON.stringify(findData));
                        res.jsonp({status: 200, msg: 'updated successfully.', data: findData});
                    }
                });

              
                 
            }
    );     


    
};



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    updateProfile
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to verify otp.
 _________________________________________________________________________
 */


var updateProfile = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    errorMessage = "";
    Promise.resolve().then(function() {
       return usersObj.findOne({mobileNo: user.mobileNo}).populate("childrens.school_id");
    }).then(function(userData) {
        console.log(userData);
        if(userData){
                var urlImage = userData.profileImg;
                if(req.body.file){
                    var bitmap = new Buffer(req.body.file, 'base64');
                    // write buffer to file
                    //console.log(req.body.file.filename);
                    var name = Math.floor(Date.now() / 1000).toString() + '.' + 'png';
                    fs.writeFileSync(name, bitmap);
                    uploadDir = __dirname + constantObj.imagePaths.user;
                    
                    fs.renameSync(name, uploadDir + "/" + name);
                    var urlImage = 'http://' + req.headers.host + constantObj.imagePaths.url+ '/' + name; 
                    console.log("dynamic url:::",urlImage);
                }

                 var fields = {
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    email: req.body.email,
                    age: req.body.age,
                    occupation: req.body.occupation,
                    profileImg:urlImage
                };

                usersObj.findOneAndUpdate({mobileNo: user.mobileNo}, {$set: fields}, {new: true}).populate('childrens.school_id').exec( function (err, updatedData) {
                    if (err) {
                        console.log("aaaaaaaaaa",err);
                        res.jsonp(err);
                    } else {
                        res.jsonp({status: 200, msg: 'updated successfully.', data: updatedData});
                    }
                });

        }else{
            res.jsonp({status: 201, msg: "This mobile no. not exist."});  
        }
    });   


    
};



var inviteUser = function (req, res) {
    var user = {};
    user = req.body.usersObj || req.body;
    user.verifyUser = {};
    errorMessage = "";
    Promise.resolve().then(function() {
        var secret = otp.utils.generateSecret();
        return otp.generate(secret);
    }).then(function(otpKey) {
            user.verifyUser.code = otpKey;
            usersObj(user).save(function (err, data) {
                if (err) {
                    //console.log("aaaa",err.name); 
                    switch (err.name) {
                        case 'ValidationError':
                            for (field in err.errors) {
                                console.log("bbbbb",err.errors);
                                if (err.errors[field].path === 'mobileNo') {
                                    errorMessage = 'This mobile no is already invited.';
                                }
                                
                            }//for
                            break;
                    }//switch
                    res.jsonp({status: 201, msg: errorMessage});
                } else {
                    console.log("success");
                    Promise.resolve().then(function() {
                        var message = otpKey + ' : use this otp code to verify your account. '+ 
                                      'Android app link: '+ constantObj.appLinks.ANDROID +
                                      ' IOS app link: '+ constantObj.appLinks.ANDROID
                         return pushNotifyObj.sendSMS(req.body.mobileNo,otpKey,message);
                      }).then(function(responseMobile) {
                           res.jsonp({status: 306, msg: "invitation sent successfully.",data:data});
                      });     
                    
                }
            });


    });

};



var firebaseAdd = function (req, res) {
   
   //create group and insert data
   /* var usersRef = ref.child('dps_1_11th');
    var userRef = usersRef.push({
     user_id: 1
    }); */

    //ref group exist
    /* ref.child("asfsaf").once('value', function(snapshot) {
        var exists = (snapshot.val() !== null);
        console.log(exists);
      }); */
 
    //check data exist
    /* ref.child("dps_1_11th").orderByChild("user_id").equalTo(1).once("value",snapshot => {
        const userData = snapshot.val();
        if (userData){
          console.log(userData);
        }
    }); */


};



//  functions
exports.signUp = signUp;
exports.otpVerify = otpVerify;
exports.login = login;
exports.deviceConflictVerify = deviceConflictVerify;
exports.syncContacts = syncContacts;
exports.senOTPVerContact = senOTPVerContact;
exports.finalOtpVerify = finalOtpVerify;
exports.addChild = addChild;
exports.updateChild = updateChild;
exports.firebaseAdd = firebaseAdd;
exports.updateProfile = updateProfile;
exports.inviteUser = inviteUser;
















