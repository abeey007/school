/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 var fs = require('fs');
 var path = require('path');
 var async = require('async');


/*mapping the returned data*/
var asyncMap = function(req,res){
   
    async.map(
        ['package.json','package-other.json'], 
        function (name,callback){
            fs.readFile(path.join(__dirname,"../../",name),'utf-8', function (err, data) {
              if (err){
                return callback(err);
              }
            
              try{  
                var object =  JSON.parse(data);
              }catch(ex){
                return callback(err);
              }

             callback(null,object.name);
          

            });
        }, function(err, results) {
            console.log(err);  
           res.jsonp(results);
        });

}


/* best to use when iterate with a for loop*/

var asyncForEach = function(req,res){
   
    var obj = {dev: "package.json", test: "package-other.json"};
    var configs = {};

    async.forEachOf(obj, function (value, key, callback) {
        fs.readFile(path.join(__dirname,"../../",value), "utf8", function (err, data) {
            if (err) return callback(err);
            try {
                var object = JSON.parse(data);
                configs[key] = {"name":object.name,"version":object.version};
            } catch (e) {
                return callback(e);
            }
            callback();
        });
    }, function (err) {
        if (err) console.error(err.message);
        // configs is now a map of JSON data
         res.jsonp(configs);
    });

}

exports.asyncMap = asyncMap;
exports.asyncForEach = asyncForEach;
















