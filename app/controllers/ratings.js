/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var formidable = require("formidable");
var schoolObj = require('../models/schools.js');
var usersObj = require('../models/users.js');
var ratingObj = require('../models/ratings.js');
var constantObj = require('../../config/constants.js');
var passport = require('../../config/passport.js');
var cfg = require('../../config/passport_config.js');
var pushNotifyObj = require('../../config/pushNotify.js');
var functionsObj = require('../../config/functions.js');

var nodemailer = require('nodemailer');
var smtp = require("nodemailer-smtp-transport");
var crypto = require('crypto');
var async = require("async");
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var fs = require('fs');
var formidable = require('formidable');
var mongoose = require('mongoose');
var moment = require('moment');
var otp = require('otplib/lib/totp');
var _ = require('lodash');

mongoose.set('debug', true);
var smtpTransport = nodemailer.createTransport(smtp({
    host: constantObj.gmailSMTPCredentials.host,
    secureConnection: constantObj.gmailSMTPCredentials.secure,
    port: constantObj.gmailSMTPCredentials.port,
    auth: {
        user: constantObj.gmailSMTPCredentials.username,
        pass: constantObj.gmailSMTPCredentials.password
    }
}));

var transporter = nodemailer.createTransport();
const startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
const endOfMonth   = moment().endOf('month').format('YYYY-MM-DD');



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    addRatings
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is to add ratings.
 _________________________________________________________________________
 */


var addRatings = function (req, res) {
    ratingReq = req.body.ratingObj || req.body;
    errorMessage = "";
    console.log(ratingReq);
    if(ratingReq){

    Promise.resolve().then(function() {
        if(ratingReq.isComment == 1){
          return functionsObj.insertSchoolComment(ratingReq);
        }else{
          return false;
        }
    }).then(function(commentId) {
          console.log("commentData",commentId);
        _.forEach(ratingReq.ratings, function(value, key) {

           var conditions = {
                school_id:  mongoose.Types.ObjectId(ratingReq.school_id),
                mobileNo:  ratingReq.mobileNo,
                date : {
                        "$gte" : startOfMonth+"T00:00:00.000Z", 
                        "$lte" : endOfMonth+"T00:00:00.000Z" 
                     },
                rating_code:key
            }
            ratingObj.findOne(conditions).exec(function (err, dataExist) {
                if (err) {
                    res.jsonp(err);
                } else {
                     if(dataExist && ratingReq.isComment!=1){
                       // console.log("rrr",dataExist); 
                        var fields = {
                            "rating_code":key,
                            "rating_number":value,
                            "comment_id":commentId.subDocumentId,
                            "date":new Date(moment().format('YYYY-MM-DD')).toISOString()
                        };

                        ratingObj.update({_id: mongoose.Types.ObjectId(dataExist._id)}, {$set: fields}, function (err, staff) {
                            if (err) {
                                console.log("aaaaaaaaaa",err);
                                res.jsonp(err);
                            } else {
                                //res.jsonp({status: 200, msg: 'updated successfully.', data: userData});
                            }
                        });
                     }else{

                            var ratingJson = {
                                "mobileNo":ratingReq.mobileNo,
                                "school_id":ratingReq.school_id,
                                "class_name":ratingReq.class_name,
                                "section_name":ratingReq.section_name,
                                "rating_code":key,
                                "rating_number":value,
                                "comment_id":commentId.subDocumentId,
                                "date":new Date(moment().format('YYYY-MM-DD')).toISOString()
                            }

                            ratingObj(ratingJson).save(function (err, data) {
                                if (err) {
                                    console.log("aaaa",err.name);
                                    res.jsonp({status: 201, msg: err.name});
                                } else {
                                    console.log("success");
                                }
                            });

                     }
                }
            });

        }); 

          res.jsonp({status: 200, msg: "data updated successfully"});

       });
   
    }else{
        res.jsonp({status: 201, msg: "No data found"});
    }
};



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    addRatings
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is to add ratings.
 _________________________________________________________________________
 */


var monthlyRatings = function (req, res) {
    ratingReq = req.body.ratingObj || req.body;
    errorMessage = "";
    console.log(ratingReq);
    if(ratingReq){
        var startDate = moment([ratingReq.year, ratingReq.month-1]);
        var endDate = moment(startDate).endOf('month');
        console.log(startDate.format('YYYY-MM-DD'));
        console.log(endDate.format('YYYY-MM-DD'));

        if(ratingReq.mobileNo){

          var conditions = [
                { "$match": { 
                              "school_id": mongoose.Types.ObjectId(ratingReq.school_id),
                              "mobileNo": mongoose.Types.ObjectId(ratingReq.mobileNo),
                              "date" : {
                                      "$gte" : startDate.format('YYYY-MM-DD')+"T00:00:00.000Z", 
                                      "$lte" : endDate.format('YYYY-MM-DD')+"T00:00:00.000Z" 
                                   } 
                            } 
                },
                { "$group": { 
                    "_id": "$rating_code",
                    "rating_number": { $avg: "$rating_number" }
                }}
            ];

        }else{



          var conditions = [
                { "$match": { 
                              "school_id": mongoose.Types.ObjectId(ratingReq.school_id),
                              "date" : {
                                      "$gte" : startDate.format('YYYY-MM-DD')+"T00:00:00.000Z", 
                                      "$lte" : endDate.format('YYYY-MM-DD')+"T00:00:00.000Z" 
                                   } 
                            } 
                },
                { "$group": { 
                    "_id": "$rating_code",
                    "rating_number": { $avg: "$rating_number" }
                }}
            ];


          }
          ratingObj.aggregate(conditions, function (error, schoolRatingsTillNow) {
              if (error) {
                  reject(error);
                  return;
              }
             res.jsonp({status: 200, msg: "data found",data:schoolRatingsTillNow});                
          });


    }else{
        res.jsonp({status: 201, msg: "No data found"});
    }
};


//  functions
exports.addRatings = addRatings;
exports.monthlyRatings = monthlyRatings;

















