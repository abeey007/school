/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var formidable = require("formidable");
var schoolObj = require('../models/schools.js');
var usersObj = require('../models/users.js');
var classesObj = require('../models/classes.js');
var constantObj = require('../../config/constants.js');
var passport = require('../../config/passport.js');
var cfg = require('../../config/passport_config.js');
var pushNotifyObj = require('../../config/pushNotify.js');
var functionsObj = require('../../config/functions.js');
var ratingObj = require('../models/ratings.js');

var nodemailer = require('nodemailer');
var smtp = require("nodemailer-smtp-transport");
var crypto = require('crypto');
var async = require("async");
var bcrypt = require('bcrypt-nodejs');
var jwt = require('jwt-simple');
var fs = require('fs');
var formidable = require('formidable');
var mongoose = require('mongoose');
var moment = require('moment');
var otp = require('otplib/lib/totp');
var _ = require('lodash');

mongoose.set('debug', true);
var smtpTransport = nodemailer.createTransport(smtp({
    host: constantObj.gmailSMTPCredentials.host,
    secureConnection: constantObj.gmailSMTPCredentials.secure,
    port: constantObj.gmailSMTPCredentials.port,
    auth: {
        user: constantObj.gmailSMTPCredentials.username,
        pass: constantObj.gmailSMTPCredentials.password
    }
}));

var transporter = nodemailer.createTransport();



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    addSchool
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to register new user.
 _________________________________________________________________________
 */


var addSchool = function (req, res) {
    console.log("aaaaaaaaaaaaaa");
    school = req.body.schoolObj || req.body;
    errorMessage = "";
            schoolObj(school).save(function (err, data) {
                if (err) {
                    console.log("aaaa",err.name);
                    switch (err.name) {
                        case 'ValidationError':
                            for (field in err.errors) {
                                console.log("bbbbb",err.errors[field].path);
                                
                            }//for
                            break;
                    }
                } else {
                    console.log("success");
                     res.jsonp({status: 200, msg: "success",data:data}); 
                    
                }
            });

    
};



var addClass = function (req, res) {
    console.log("aaaaaaaaaaaaaa");
    school = req.body.schoolObj || req.body;
    errorMessage = "";
            classesObj(school).save(function (err, data) {
                if (err) {
                    console.log("aaaa",err.name);
                    switch (err.name) {
                        case 'ValidationError':
                            for (field in err.errors) {
                                console.log("bbbbb",err.errors[field].path);
                                
                            }//for
                            break;
                    }
                } else {
                    console.log("success");
                     res.jsonp({status: 200, msg: "success",data:data}); 
                    
                }
            });

    
};



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    searchSchool
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to register new user.
 _________________________________________________________________________
 */


var searchSchool = function (req, res) {
    school = req.body.schoolObj || req.body;
    errorMessage = "";
    var skipVal = (parseInt(req.body.pageNo)- 1)*(parseInt(req.body.limit));
    if(req.body.search_type=="cords"){

        var coords = [];  
        coords[0] = req.body.longitude || 0;  
        coords[1] = req.body.latitude || 0;  

        var maxDistance = req.body.distance || 8;
        maxDistance /= 6371; 

        schoolObj.find({ loc: {  $near: coords, $maxDistance: maxDistance }}).skip(skipVal).limit(parseInt(req.body.limit)).exec(function(err, locations) {
            if (err) {
                return res.json(500, err);
            }

            schoolObj.count({ loc: {  $near: coords, $maxDistance: maxDistance }}).exec(function(err, countData) {
                if (err) {
                    return res.json(500, err);
                }

                Promise.all(locations.map(function (dataeach) {
                        return functionsObj.getAllSchoolData(dataeach);
                    })).then(function (outsArray) {
                        //console.log(outsArray);
                        res.jsonp({status: 200, msg: "get all schools.", data: outsArray,count:countData});
                    });

                 // res.jsonp({status: 200, msg: "get all schools.", data: locations,count:countData});
            });

           
        });

    }

    if(req.body.search_type=="name"){
        var searchName = new RegExp(req.body.searchData , 'i');
        var conditions = {
          $or: [
                   {"schoolName": searchName},
                   {"city": searchName},
                   {"address": searchName}

               ]
          };

        schoolObj.find(conditions).skip(skipVal).limit(parseInt(req.body.limit)).exec(function (err, data) {
                if (err) {
                    res.jsonp(err);
                } else {

                    schoolObj.count(conditions).exec(function(err, countData) {
                        if (err) {
                            return res.json(500, err);
                        }

                        Promise.all(data.map(function (dataeach) {
                            return functionsObj.getAllSchoolData(dataeach);
                        })).then(function (outsArray) {
                            //console.log(outsArray);
                            res.jsonp({status: 200, msg: "get all schools.", data: outsArray,count:countData});
                        });

                         
                    });

                   /* schoolObj.count(conditions).exec(function (err, countData) {
                        if (err) {
                            res.jsonp(err);
                        } else {
                            res.jsonp({status: 200, msg: "get all schools.", data: data,count:countData});
                        }
                    }); */

                    
                }
            });

    }


    
};


/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    schoolById
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to register new user.
 _________________________________________________________________________
 */


var schoolById = function (req, res) {
    school = req.body.schoolObj || req.body;
    errorMessage = "";
    schoolObj.findOne({_id:  mongoose.Types.ObjectId(school.id)}, function (err, currentSchool) { 

           if(currentSchool){
                Promise.resolve().then(function() {
                   return functionsObj.getAllSchoolData(currentSchool);
                }).then(function(schoolData) {
                    res.jsonp({status: 200, msg: "get all schools.", data: schoolData});
                })
                
           }else{
                res.jsonp({status: 201, msg: "No data found"});
           }
         
    }); 
    
};


/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    addChildSchool
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to register new user.
 _________________________________________________________________________
 */


var addChildSchool = function (req, res) {
    school = req.body.schoolObj || req.body;
    errorMessage = "";
    Promise.resolve().then(function() {
       return schoolObj.findOne({"_id": mongoose.Types.ObjectId(school.school_id)});
    }).then(function(schoolData) {
        var comunity_code_string = school.school_name+"____"+school.class_name+"____"+school.section_name;
        var comunity_code = comunity_code_string.replace(/\s+/g, '___').toLowerCase();
        console.log("comunity_code_string",school.school_name+" "+school.class_name+" "+school.section_name);
        console.log("comunity_code",school.school_name+" "+school.class_name+" "+school.section_name);
        if(schoolData){ //if school data is exist
            schoolObj.findOne({ "_id": mongoose.Types.ObjectId(school.school_id),
                classes: { $elemMatch: { class_name: school.class_name } } },{ 'classes.$': 1 }, 
                function(err, classData) {
                if(err){
                    console.log("class find",err);
                    res.jsonp(err);
                }

                if(classData) //if class data is exist
                {
                   var matchedSection =  _.filter(classData.classes[0].sections, function(o) { 
                            return o._id == school.section_id; 
                    });
                    console.log(matchedSection);
                    if(matchedSection.length > 0){ //here school class section. all data matched 
                        res.jsonp({status: 200, msg: "school class section exist.", school_id: school.school_id, comunity_code:comunity_code});
                    }else{ // section data inserted from here

                        var fields = { "classes.$.sections": { section_name: school.section_name } };
                                schoolObj.findOneAndUpdate({"_id": mongoose.Types.ObjectId(school.school_id),
                                    "classes.class_name": school.class_name}, {$push: fields}, function (err, sectionUpdateData) {
                                    if (err) {
                                        console.log("section add",err);
                                        res.jsonp(err);
                                    } else {
                                      console.log("11111",comunity_code);  
                                      res.jsonp({status: 200, msg: "school class exist.", school_id: school.school_id,comunity_code:comunity_code});
                                        
                                    }
                                });
                    } 
                    
                    


                }else{  //if class data not exist

                    var fields = { "classes": { class_name: school.class_name } };
                    schoolObj.update({"_id": mongoose.Types.ObjectId(school.school_id)}, {$push: fields}, function (err, classUpdateData) {
                        if (err) {
                            console.log("class add",err);
                            res.jsonp(err);
                        } else { //then also section needs to insert

                            console.log("section data is new and class is new",classUpdateData);
                            var fields = { "classes.$.sections": { section_name: school.section_name } };
                            schoolObj.findOneAndUpdate({"_id": mongoose.Types.ObjectId(school.school_id),
                                "classes.class_name": school.class_name}, {$push: fields}, function (err, sectionUpdateData) {
                                if (err) {
                                    console.log("section add",err);
                                    res.jsonp(err);
                                } else {

                                   res.jsonp({status: 200, msg: "school conly exist.", school_id: school.school_id,comunity_code:comunity_code});
                                    
                                }
                            });

                            
                        }
                    });

                }

                
            });



        }else{ 

             res.jsonp({status: 201, msg: "School Not found"});

        }

        
    }); 
   
};



/*________________________________________________________________________
 * @Date:       10 Sept,2017
 * @Method :    addSchoolBus
 * Created By:  Abhijeet Singh
 * Modified On: -
 * @Purpose:    This function is used to register new user.
 _________________________________________________________________________
 */


var addSchoolBus = function (req, res) {
    school = req.body.schoolObj || req.body;
    errorMessage = "";
    Promise.resolve().then(function() {
       return schoolObj.findOne({"_id": mongoose.Types.ObjectId(school.school_id)});
    }).then(function(schoolData) {
        var bus_code_string = school.school_name+"____"+school.bus_no;
        var bus_comunity_code = bus_code_string.replace(/\s+/g, '___').toLowerCase();
        if(schoolData){ //if school data is exist
            schoolObj.findOne({ "_id": mongoose.Types.ObjectId(school.school_id),
                buses: { $elemMatch: { bus_no: school.bus_no } } },{ 'buses.$': 1 }, 
                function(err, BusData) {
                if(err){
                    console.log("Bus find",err);
                    res.jsonp(err);
                }

                if(BusData) //if class data is exist
                {
                   res.jsonp({status: 200, msg: "school bus exist.", school_id: school.school_id,bus_comunity_code:bus_comunity_code});
                }else{  //if class data not exist

                    var fields = { "buses": { bus_no: school.bus_no } };
                    schoolObj.update({"_id": mongoose.Types.ObjectId(school.school_id)}, {$push: fields}, function (err, busUpdateData) {
                        if (err) {
                            console.log("class add",err);
                            res.jsonp(err);
                        }
                            
                        res.jsonp({status: 200, msg: "school bus inserted.", school_id: school.school_id,bus_comunity_code:bus_comunity_code});
                    });

                }

                
            });



        }else{ 

             res.jsonp({status: 201, msg: "School Not found"});

        }

        
    }); 
   
};


var getClasses = function (req, res) {
    Promise.resolve().then(function() {
       return classesObj.find();
    }).then(function(classData) {
       res.jsonp({status: 200, msg: 'updated successfully.', data: classData});
    });   


    
};


var getAllComments = function (req, res) {
    school = req.body.schoolObj || req.body;
    errorMessage = "";
    var configs = [];
    var skipVal = (parseInt(req.body.pageNo)- 1)*(parseInt(req.body.limit));
    schoolObj.findOne({_id:  mongoose.Types.ObjectId(school.id)}, function (error, commentCount) {
              if (error) {
                  reject(error);
                  return;
              }
        var countComments = commentCount.comments.length;
        schoolObj.findOne({_id:  mongoose.Types.ObjectId(school.id)}, { comments: { $slice: [skipVal,parseInt(req.body.limit) ] } }, function (err, currentComments) { 
               async.forEachOf(currentComments.comments, function (value, key, callback) {
                    var conditions = {
                        comment_id:  mongoose.Types.ObjectId(value._id),
                    }

                    ratingObj.find(conditions).exec(function (err, dataExist) {
                        if (err) return callback(err);
                        
                        try {
                           value.rating = dataExist;
                        } catch (e) {
                            return callback(e);
                        }

                        //console.log(dataExist);
                        callback();
                    });

                }, function (err) {
                    if (err) console.error(err.message);
                    // configs is now a map of JSON data
                    res.jsonp({status: 200, comments: currentComments.comments,commentCount:countComments});
                    console.log(currentComments.comments);
                });


        /*   _.forEach(currentComments.comments, function(value, key) {
                console.log(value._id);
                var conditions = {
                    comment_id:  mongoose.Types.ObjectId(value._id),
                }

                ratingObj.find(conditions).exec(function (err, dataExist) {
                    if (err) {
                        res.jsonp(err);
                    }

                    console.log(dataExist);
                });
            }); */

            }); 
         
    }); 
    
};



//  functions
exports.addSchool = addSchool;
exports.addClass = addClass;
exports.searchSchool = searchSchool;
exports.schoolById = schoolById;
exports.addChildSchool = addChildSchool;
exports.addSchoolBus = addSchoolBus;
exports.getClasses = getClasses;
exports.getAllComments = getAllComments;


















