var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var auth = require('./config/passport_config.js');
var jwt = require('jwt-simple');
var fs = require('fs');
var mongoose = require('mongoose');


var users = require('./routes/users');
var schools = require('./routes/schools');
var ratings = require('./routes/ratings');
var practice = require('./routes/practice');


process.env.NODE_ENV = 'production';


var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.engine('html', function (path, opt, fn) {
    fs.readFile(path, 'utf-8', function (err, str) {
        if (err)
            return str;
        return fn(null, str);
    });
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));

app.use(bodyParser({limit: '50mb'}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Use the passport package in our application
app.use(passport.initialize());


app.use('/', users);
app.use('/', schools);
app.use('/', ratings);
app.use('/', practice);
app.all('*', function(req, res) {
    res.sendfile('views/index.html');
  });
//app.get('/', function (req, res) {
//    console.log("Server started");
//   res.render('index.html');
//});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// 
// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
