(function () {
    'use strict';
    angular
            .module('referral-app')
            .directive('imageUploader', imageUploader);
    
    imageUploader.$inject = ['$parse','Upload','$state','toastr'];
    
    function imageUploader($parse,Upload,$state,toastr) {
        return {
            require: "ngModel",
            scope: "=",
            link: function(scope, element, attributes, ngModel) {

              var onChange = $parse(attributes.imageUploader);
                element.on('change', function (event) {
                    //console.log("message",scope.vm.offer);
                    //scope.vm.offer.images.push(event.target.files[0]);
                    console.log($state.current.name);
                    var state = $state.current.name;
                    var webserviceUrl = "";
                    if(state == "anon.addoffer" || state == "anon.edit-offer")
                    {
                        webserviceUrl = webservices.uploadofferimg;
                    }
                   
                    var file = event.target.files[0];
                    console.log(event.target.files);
                    if(file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/jpg" ){
                        file.upload = Upload.upload({
                            url:  webserviceUrl,
                            data: {file: file}
                        });
                        file.upload.then(function (response) {
                              console.log(response.data.data);
                               if(state == "anon.addoffer" || state == "anon.edit-offer")
                                {
                                    console.log(response.data.data);
                                    scope.vm.offer.offerImage =  response.data.data;
                                }
                         
                            });
                     }else{
                        toastr.error('Error! File is not valid format.');
                     }
                });
            }
        };
    };
})();

