(function () {
    'use strict';
    angular
            .module('referral-app')
            .directive('fileReader', fileReader);
    
    fileReader.$inject = ['$parse','Upload','$state','toastr','$localStorage','$http'];
    
    function fileReader($parse,Upload,$state,toastr,$localStorage,$http) {
        return {
                scope: {
                  fileReader:"="
                },
                link: function(scope, element) {
                  $(element).on('change', function(changeEvent) {
                    //var files = changeEvent.target.files;
                    var file = changeEvent.target.files[0];
                    var query = {};
                    var userId = $localStorage.currentUser.info._id;
                    if($localStorage.currentUser.info.role == "s_owner")
                    {
                       var userId = $localStorage.currentUser.info.addedBy;
                       var salesUserId = $localStorage.currentUser.info._id;
                    } 
                    console.log(file.type);
                    if(file.type == "text/csv" || file.type == "application/vnd.ms-excel"){
                        file.upload = Upload.upload({
                            url:  webservices.importoffers,
                            data: {file: file, userId: userId, salesUserId: salesUserId}
                        });
                        file.upload.then(function (response) {
                              if(response.data.status==200)
                                    {
                                      toastr.success('Success! Offers imported successfully.');
                                       $state.go($state.current, {}, {reload: true});
                                    }else{
                                      toastr.error('Error!.');
                                    }
                            });
                     }else{
                        toastr.error('Error! Please upload csv format.');
                     }
                  });
                }
              };
    };
})();
