(function () {
    'use strict';
    angular
            .module('referral-app')
            .directive('fileUploader', fileUploader);
    
    fileUploader.$inject = ['$parse','Upload','$state','toastr'];
    
    function fileUploader($parse,Upload,$state,toastr) {
        return {
            require: "ngModel",
            scope: "=",
            link: function(scope, element, attributes, ngModel) {

              var onChange = $parse(attributes.fileUploader);
                element.on('change', function (event) {
                    //console.log("message",scope.vm.offer);
                    //scope.vm.offer.images.push(event.target.files[0]);
                    console.log($state.current.name);
                    var state = $state.current.name;
                    var webserviceUrl = "";
                    if(state == "anon.mngprofile" )
                    {
                        webserviceUrl = webservices.uploadprofileimg;
                    }
                    else if(state == "anon.addoffer" || state == "anon.edit-offer")
                    {
                        webserviceUrl = webservices.uploadofferimg;
                    }
                    else if(state == "anon.dashboard" || state == "anon.view-sales-info")
                    {
                        webserviceUrl = webservices.uploadprofileimg;
                    }

                    

                    var file = event.target.files[0];
                    console.log(event.target.files);
                    if(file.type == "image/jpeg" || file.type == "image/png" || file.type == "image/jpg" ){
                        file.upload = Upload.upload({
                            url:  webserviceUrl,
                            data: {file: file}
                        });
                        file.upload.then(function (response) {
                              console.log(response.data.data);
                              if(state == "anon.mngprofile")
                                {
                                    scope.vm.user.personalInfo.profileImg = response.data.data;
                                }
                                else if(state == "anon.addoffer" || state == "anon.edit-offer")
                                {
                                    scope.vm.offer.offerImages.push({image: response.data.data});
                                }
                                else if(state == "anon.dashboard" || state == "anon.view-sales-info")
                                {
                                    scope.vm.newuser.personalInfo.profileImg = response.data.data;
                                    scope.vm.editUser.personalInfo.profileImg = response.data.data;
                                    scope.vm.userProfile = response.data.data;
                                    scope.vm.file = response.data.data;
                                    scope.vm.file1 = response.data.data;
                                }


                            });
                     }else{
                        toastr.error('Error! File is not valid format.');
                     }
                });
            }
        };
    };
})();

