(function () {
    'use strict';

    angular
            .module('referral-app')
            .factory('AuthService', AuthService);

    AuthService.$inject = ['$http', '$localStorage', '$state','$auth','$rootScope','toastr'];
    function AuthService($http, $localStorage, $state, $auth, $rootScope, toastr) {

        var service = {
            registerUser: registerUser,
            loginUser: loginUser,
            logoutUser: logoutUser,
            forgotpwd: forgotpwd,
            verifyEmail : verifyEmail,
            resetpwd : resetpwd,
            getCategories: getCategories,
            registerCard : registerCard,
            viewSalesowner: viewSalesowner,
            getPlansFromAdmin:getPlansFromAdmin
        };

        return service;

        function registerUser(postData, callback) {
            $http.post(webservices.usersignup, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else {
                        callback(successCallback.data);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }


        function loginUser(postData, callback) {
            $http.post(webservices.userlogin, postData)
                    .then(function (successCallback, errorCallback) {
                        if (successCallback.data.status === 200) {
//                            $localStorage.currentUser = {token: successCallback.data.token, type: response.type, id: response.data._id, email: response.data.email, info: response.data.personalInfo};
                            if(successCallback.data.data.role === 'b_owner'){
                                $localStorage.currentUser = {token: successCallback.data.token,info: successCallback.data.data};
                                $localStorage.loginstatus = true;
                                $rootScope.rolewise = true;
                            }else if(successCallback.data.data.role === 's_owner'){
                                $localStorage.currentUser = {token: successCallback.data.token,info: successCallback.data.data};
                                $localStorage.loginstatus = true;
                                $rootScope.rolewise = false;
                            } else if(successCallback.data.data.role === 'admin' || successCallback.data.data.role === 'referrer'){
                                $localStorage.loginstatus = false;
                                toastr.error("Permission denied !")
                            }
                            if(successCallback.data.data){
                                $rootScope.loggeduser = successCallback.data.data;
                                if($rootScope.loggeduser.personalInfo.profileImg === 'user.png'){
                                    $rootScope.loggeduser.personalInfo.profileImg =  "/images/thumbnail.jpg";
                                }
                            }
                            $http.defaults.headers.common.Authorization = 'JWT ' + successCallback.data.token;
                            callback(successCallback.data);
                        } else {
                            callback(successCallback.data);
                        }
                        if(errorCallback){
                            console.log(errorCallback);
                        }
                    });
        }

        function logoutUser() {
            if($localStorage.currentUser) {
                delete $localStorage.currentUser;
            }
            $localStorage.loginstatus = false;
            $http.defaults.headers.common.Authorization = undefined;
            $state.go('anon.home');
        }

        function forgotpwd(postData, callback) {
            $http.post(webservices.forgotpwd, postData)
                .then(function (successCallback, errorCallback) {
                    console.log(successCallback.data)
                    if (successCallback.data.status === 200) {
                            callback(true);
                    } else {
                            callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        
        function verifyEmail(postData, callback) {
           
            var serviceUrl = '/verifyemail' + '/' + postData;
            console.log("servicce:",serviceUrl);
            return $http.get(serviceUrl)
                    .success(function (response) {
                        console.log("aaaaaa",response);
                        if (response.status === 200) {
                            callback(response);
                        } else {
                            callback(response);
                        }
                    })
                    .error(function () {
                        callback();
                    });
        }
        
        function resetpwd(postData, callback) {
            $http.post(webservices.resetpwd, postData)
                .then(function (successCallback, errorCallback) {
                    console.log(successCallback.data)
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function getCategories(callback) {
           
            var serviceUrl = '/get-category';
            console.log("servicce:",serviceUrl);
            $http.get(serviceUrl)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data);
                        callback(successCallback.data);
                    } else{
                        callback(errorCallback);
                    }
                });
            
        }

        function registerCard(postData, callback) {
            $http.post(webservices.registercard, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(successCallback.data);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function viewSalesowner(postData, callback) {
            var serviceUrl = '/view-staff' + '/' + postData.staffId;
            console.log("servicce:",serviceUrl);
            $http.get(serviceUrl)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data.data);
                        callback(successCallback.data.data);
                    } else{
                        callback(errorCallback);
                    }
                });
        }


        function getPlansFromAdmin(postData,callback) {
            $http.post(webservices.getPlansFromAdmin, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status==200) {
                        callback(successCallback.data);
                    }else if (successCallback.data.status==201) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }


    }
})();
