(function () {
    'use strict';

    angular
            .module('referral-app')
            .controller('OfferController', OfferController);

    OfferController.$inject = ['$state', '$rootScope', 'AuthService', 'OfferService','DashboardService', '$localStorage', 'toastr','$compile','$stateParams','$location',"$sce"];


    function OfferController( $state, $rootScope, AuthService, OfferService,DashboardService, $localStorage, toastr,$compile,$stateParams,$location,$sce) {

        var vm = this;
        vm.init = init;
        vm.open1 = open1;
        vm.open2 = open2;

        vm.videoLoad = videoLoad;

        vm.addNewImage = addNewImage;
        vm.removeImage = removeImage;

        vm.addNewVideo = addNewVideo;
        vm.removeVideo = removeVideo;
        vm.listCurrentOffers = listCurrentOffers;
        vm.listPastOffers = listPastOffers;
        vm.countCurrentOffers = countCurrentOffers;
        vm.countCurrentOffersPast = countCurrentOffersPast;
        vm.editOffer = editOffer;
        vm.updateOffer = updateOffer;
        vm.deleteOfferMedia = deleteOfferMedia;
        vm.deleteOffer = deleteOffer;
        vm.deletePop = deletePop;
        vm.otherDetailPop = otherDetailPop;
        vm.openVideoPopup =openVideoPopup;
        vm.Close2 = Close2;
        vm.Close3 = Close3;
        vm.viewOffer = viewOffer;
        vm.searchCurrentOffer = searchCurrentOffer;
        vm.searchPastOffer = searchPastOffer;
        vm.totalOfferReff = totalOfferReff;
        vm.totalReffsInOffer = totalReffsInOffer;
        vm.totalOfferPendingVar = {};
        vm.totalReffsInOfferVar = {};
        vm.offferImagescount = "";
        
        vm.images = [{id: 'image1'}];
        vm.videos = [{id: 'video1'}];

        vm.currentPage = 1;
        vm.maxsize = 2;
        vm.itemsPerPage = 5;
        vm.conditions = {};
        vm.pageChanged = pageChanged;

        vm.currentPagePast = 1;
        vm.maxsizePast = 2;
        vm.itemsPerPagePast = 5;
        vm.conditionsPast = {};
        vm.pageChangedPast = pageChangedPast;


        vm.offer ={};
        vm.couponData= {};
        vm.totalofferRefVar= {};
        vm.offerForm = {};
        vm.saleoffers = [];
        vm.pastsaleoffers =[];
        vm.offer.images =[];
        vm.offer.offerImages = [];
        vm.offer.offerVideos =[];
        vm.offerImages =[];
        vm.offerVideos = [];
        vm.offerVideosIframe = [];
        vm.media ={};
        vm.id = '';
        vm.offerIndex = '';
        vm.offertab = '';
        vm.couponCode = '';
        

        init();
        vm.popup1 = { opened: false };
        vm.popup2 = { opened: false };
        vm.format = 'MM-dd-yyyy';
        vm.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
          };

        vm.dateOptions2 = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            minDate: new Date(),
            startingDay: 1
          };  
        
       vm.addOffer = addOffer;
     
        // Initialisation
        function init() {
           if( $localStorage.currentUser){
                $rootScope.loggeduser = $localStorage.currentUser.info;
                listCurrentOffers();
                countCurrentOffers();
            }
             $('#myCarousel').carousel({ interval: 2000, cycle: true });
            $(".placepicker").placepicker();
            $('[data-toggle="tooltip"]').tooltip(); 
            
            if($stateParams.offerId){
                editOffer();
            }

            if($stateParams.viewofferId){
                viewOffer();
                //viewOfferRefferalsCount();
            }
            

        }

        function open1 () {
            vm.popup1.opened = true;
        };
        
        function open2 () {
            vm.popup2.opened = true;
        };

        function addNewImage() {
            var newItemNo = vm.images.length+1;
            vm.images.push({'id':'image'+newItemNo});
        };
            
        function removeImage() {
            var lastItem = vm.images.length-1;
            vm.images.splice(lastItem);
            vm.offer.offerImages.splice(lastItem);
        };

       function addNewVideo() {
         var newItemNo = vm.videos.length+1;
         vm.videos.push({'id':'choice'+newItemNo});
        };
    
        function removeVideo() {
            var lastItem = vm.videos.length-1;
             vm.videos.splice(lastItem);
             vm.offer.offerVideos.splice(lastItem);
        };

        function videoLoad(file){
             if(typeof(file) !="undefined"){   
                vm.offer.offerVideos.push({video: file});
                console.log(vm.offer.offerVideos);
              }
        }

        function addOffer() {
            console.log(vm.offer);
            console.log(new Date(vm.offer.endDate));
            //console.log(vm.offer.images);
            vm.offer.storeLocator = $(".placepicker").val();
            vm.offer.userId= $localStorage.currentUser.info._id;
            if($localStorage.currentUser.info.role == "s_owner")
            {
               vm.offer.userId= $localStorage.currentUser.info.addedBy;
               vm.offer.salesUserId= $localStorage.currentUser.info._id;
            }
            vm.offer.offerCategory= $localStorage.currentUser.info.business.category;
            OfferService.addOffer(vm.offer, function (result) {
                    if (result === false) {
                        toastr.error('Error while adding new offer.');
                    }else if(result.status == 501){
                         toastr.error('Error! Your offer limit has been reached or your membership has been expired. Please upgrade your membership');   
                    }else if(result.status == 505){
                         toastr.error('Error! Please renew your membership.');   
                    } else {
                        vm.offer = {};
                        vm.offerForm.$setPristine();
                        vm.offerForm.$setUntouched();
                        toastr.success('Success! Offer added successfully.');
                        $location.path("/offers");
                    }
                }); 
        }

        function countCurrentOffers(){
            vm.userId= $localStorage.currentUser.info._id;
            if($localStorage.currentUser.info.role == "s_owner")
            {
               vm.userId = $localStorage.currentUser.info.addedBy;
            }
            
            OfferService.countCurrentOffers({userId: vm.userId}, function (count) {
                vm.totalItems  = count;
                vm.noofPages  = Math.ceil(vm.totalItems / vm.itemsPerPage);
                console.log("vm.totalItems",vm.totalItems);
                console.log("vm.noofPages",vm.noofPages);
            });
        }

        function listCurrentOffers() {
            vm.conditions.skip = ((vm.currentPage - 1) * vm.itemsPerPage);
            vm.conditions.limit = 5;
            vm.conditions.userId= $localStorage.currentUser.info._id;
            if($localStorage.currentUser.info.role == "s_owner")
            {
               vm.conditions.userId= $localStorage.currentUser.info.addedBy;
            }
            
            countCurrentOffers();
            OfferService.listCurrentOffers(vm.conditions,function (result) {
                   console.log(result);
                   vm.saleoffers =  result;
                });
        }

        function listPastOffers() {
            vm.conditionsPast.skip = ((vm.currentPagePast - 1) * vm.itemsPerPagePast);
            vm.conditionsPast.limit = 5;
            vm.conditionsPast.userId= $localStorage.currentUser.info._id;
            if($localStorage.currentUser.info.role == "s_owner")
            {
               vm.conditionsPast.userId= $localStorage.currentUser.info.addedBy;
            }
            countCurrentOffersPast();
            OfferService.listPastOffers(vm.conditionsPast,function (result) {
                   console.log(result);
                   vm.pastsaleoffers =  result;

                });
        }

        function countCurrentOffersPast(){
            vm.userId= $localStorage.currentUser.info._id;
            if($localStorage.currentUser.info.role == "s_owner")
            {
               vm.userId= $localStorage.currentUser.info.addedBy;
            }
            OfferService.countPastOffers({userId: vm.userId}, function (count) {
                vm.totalItemsPast  = count;
                vm.noofPagesPast  = Math.ceil(vm.totalItemsPast / vm.itemsPerPagePast);
                console.log("vm.totalItems",vm.totalItemsPast);
                console.log("vm.noofPages",vm.noofPagesPast);
            });
        }

        function pageChanged(){
            listCurrentOffers();
        }

        function pageChangedPast(){
            listPastOffers();
        }
        
        function editOffer() {
           vm.hiddenStatus = '';
           OfferService.editOffer({offerId: $stateParams.offerId}, function (result) {
                if (result) {
                    vm.offer = result;
                    vm.offer.startDate = new Date(vm.offer.startDate);
                    vm.offer.endDate = new Date(vm.offer.endDate);
                    vm.hiddenStatus = vm.offer.publishStatus;
                    if(vm.offer.publishStatus=="published")
                    {
                       vm.offer.publishStatus = true;     
                    }else{
                       vm.offer.publishStatus = false;
                    }

                    $(".placepicker").val(vm.offer.storeLocator);
                    vm.offerImages = vm.offer.offerImages;
                    vm.offerVideos = vm.offer.offerVideos;
                    for(var i=0;i< vm.offer.offerVideos.length;i++)
                    {
                        var video_id = "";
                        var video_id = vm.offer.offerVideos[i].video;
                        if(typeof(video_id) != "undefined"){
                            vm.offerVideos[i].video = video_id.split('v=')[1].split('&')[0];
                        }
                        //
                    }
                    vm.offer.offerImages = [];
                    vm.offer.offerVideos = [];

                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }

        function updateOffer() {
            vm.offer.storeLocator = $(".placepicker").val();
            
            vm.offer.userId= $localStorage.currentUser.info._id;
            if($localStorage.currentUser.info.role == "s_owner")
            {
               vm.offer.userId= $localStorage.currentUser.info.addedBy;
               vm.offer.salesUserId= $localStorage.currentUser.info._id;
            }
            vm.offer.hiddenStatus= vm.hiddenStatus;
            console.log(vm.offer);
            OfferService.updateOffer(vm.offer, function (result) {
                if (result === false) {
                    toastr.error('Error while adding new offer.');
                } else {
                    toastr.success('Success! Offer updated successfully.');
                    $state.reload();
                }
            });
            
        }

        function deleteOfferMedia(id,media_id,type,indexId)
        {
            vm.media.id= id;
            vm.media.media_id = media_id;
            vm.media.type = type;
            OfferService.deleteOfferMedia(vm.media, function (result) {
                if (result === false) {
                    toastr.error('Error while adding new offer.');
                } else {
                    if(type == "image")
                    {
                         vm.offerImages.splice(indexId, 1);
                    }else{
                        vm.offerVideos.splice(indexId, 1);
                    }
                    toastr.success('Success! Media removed successfully.');
                }
            });
        }

        function deleteOffer(id)
        {
            var userId=  $localStorage.currentUser.info._id;
            
            OfferService.deleteOffer({_id: vm.id,userId: userId}, function (result) {
                if (result === false) {
                    toastr.error('Error while deleting new offer.');
                } else {
                    toastr.success('Success! Offer removed successfully.');
                    if(vm.offertab == "saleoffers")
                    {
                        vm.saleoffers.splice(vm.offerIndex, 1);
                    } 

                    if(vm.offertab == "pastsaleoffers")
                    {
                        vm.pastsaleoffers.splice(vm.offerIndex, 1);
                    } 
                       

                    $(".popup2, .overlay").hide();
                    if($state.current.name=="anon.view-offer")
                    {
                         $location.path("/offers")
                    }
                }
            });
        }


        function deletePop(id,index,type){
            console.log("delete popup",id);
            vm.id = id;
            vm.offerIndex = index;
            vm.offertab = type;
            $(".popup5").show();
            //$(".popup2").show();
            $(".overlay").show();
            $('.button.dropdown').removeClass('open');
            $('#areatoggle').removeAttr('aria-expanded');
            $('.dropdown-menu.over').hide();
            $('#areatoggle').removeClass('dropdown-toggle');
            return false;
        }

        function openVideoPopup(video_id){
            console.log(video_id);
            var embedVideoId ='<iframe class="pop-video" height="315" scrolling="no" allowtransparency="true" allowfullscreen="true" src="http://www.youtube.com/embed/'+ video_id +'?rel=0&wmode=transparent&showinfo=0" frameborder="0"></iframe>';            $(".popup3").show();
            $("#popupiframe").html(embedVideoId);
            $(".popup3").show();
            $(".overlay").show();
            $('.button.dropdown').removeClass('open');
            $('#areatoggle').removeAttr('aria-expanded');
            $('.dropdown-menu.over').hide();
            $('#areatoggle').removeClass('dropdown-toggle');
            return false;
        }
        

        function Close2() {
            $(".popup2, .overlay").hide();
            $(".popup5, .overlay").hide();
            $(".popup3, .overlay").hide();
            $('.dropdown-menu.over').show();
            $('#areatoggle').addClass('dropdown-toggle');
        }

        function viewOffer() {
           console.log($state.current.name);
           OfferService.viewOffer({offerId: $stateParams.viewofferId}, function (result) {
                if (result) {
                    vm.offer = result.data;
                    vm.offer.remuneration =  parseFloat(vm.offer.remuneration).toFixed(2);
                    vm.offer.discount = parseFloat(vm.offer.discount).toFixed(2);
                    vm.offerImages = vm.offer.offerImages;
                    vm.offferImagescount =  vm.offerImages.length;
                    vm.offerVideos = vm.offer.offerVideos;
                    vm.offerVideosIframe = vm.offer.offerVideos;
                    for(var i=0;i< vm.offer.offerVideos.length;i++)
                    {
                        var video_id = "";
                        var video_id = vm.offer.offerVideos[i].video;
                        if(typeof(video_id) != "undefined"){
                            vm.offerVideos[i].video = video_id.split('v=')[1].split('&')[0];
                            vm.offerVideosIframe[i].video = $sce.trustAsResourceUrl("https://www.youtube.com/embed/"+video_id.split('v=')[1].split('&')[0]);
                        }
                        //
                    }
                    
                    vm.offer.offerImages = [];
                    vm.offer.offerVideos = [];

                    if($stateParams.coupon){
                        vm.couponCode = $stateParams.coupon;
                        var query = {};
                        query.couponCode =  vm.couponCode;
                        DashboardService.searchCoupon(query, function (resultCoupon) {
                            console.log("ppppp",resultCoupon);
                            vm.couponData = resultCoupon[0];
                            vm.couponData.offerDetails.discount = parseFloat(vm.couponData.offerDetails.discount).toFixed(2);
                        });
                    }

                    vm.offer.userRole= $localStorage.currentUser.info.role;

                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }

        // To search sales manager info
        function searchCurrentOffer(searchQuery){
             
            var query = {};
                query.addedBy = $localStorage.currentUser.info._id;
                query.searchData = searchQuery;
                if($localStorage.currentUser.info.role == "s_owner")
                {
                   query.addedBy = $localStorage.currentUser.info.addedBy;
                }
                console.log(query);
            OfferService.searchCurrentOffer(query, function (result) {

                if (result.length > 0) {
                     vm.saleoffers =  result;
                    countCurrentOffers();
                    toastr.success('Success !');
                   
                } else if(result.length === 0){
                    toastr.error('No offer found !'); 
                   
                }else{
                    toastr.error('No offer found !'); 
                }
            });
        }


        // To search sales manager info
        function searchPastOffer(searchQuery){
            console.log("search successfully.");
            var query = {};
                query.addedBy = $localStorage.currentUser.info._id;
                query.searchData = searchQuery;
                if($localStorage.currentUser.info.role == "s_owner")
                {
                   query.addedBy = $localStorage.currentUser.info.addedBy;
                }
            OfferService.searchPastOffer(query, function (result) {

                if (result.length > 0) {
                    vm.pastsaleoffers =  result;
                    countCurrentOffersPast();
                    toastr.success('Success !');
                   
                } else if(result.length === 0){
                    toastr.error('No offer found !'); 
                   
                }else{
                    toastr.error('No offer found !'); 
                }
            });
        }


        function otherDetailPop(id){
            totalOfferReff(id);
            totalOfferReffPending(id);
            totalReffsInOffer(id);
            $(".popup4").show();
            $(".overlay").show();
            return false;
        }

        function Close3() {
            $(".popup4, .overlay").hide();
            $('.dropdown-menu.over').show();
            $('#areatoggle').addClass('dropdown-toggle');
        }

        function totalOfferReff(offerId)
        {
            var query = {};
            query.offerId = offerId;
            OfferService.totalOfferReff(query, function (result) {
                console.log(result);
                 vm.totalofferRefVar = result[0];
                 vm.totalofferRefVar.referrerEarning = parseFloat(vm.totalofferRefVar.referrerEarning).toFixed(2);
            });

        }

        function totalReffsInOffer(offerId)
        {
            var query = {};
            query.offerId = offerId;
            OfferService.totalReffsInOffer(query, function (result) {
                 vm.totalReffsInOfferVar = result;
            });

        }


        function totalOfferReffPending(offerId)
        {
            var query = {};
            query.offerId = offerId;
            OfferService.totalOfferReffPending(query, function (result) {
                vm.totalOfferPendingVar = result;
            });

        }

        
        
    }
})();
