(function () {
    'use strict';

    angular
            .module('referral-app')
            .factory('OfferService', OfferService);

    OfferService.$inject = ['$http','$localStorage','$state'];
    function OfferService($http, $localStorage, $state) {

        var service = {
            uploadpic: uploadpic,
            addOffer: addOffer,
            listCurrentOffers: listCurrentOffers,
            countCurrentOffers : countCurrentOffers,
            listPastOffers: listPastOffers,
            countPastOffers: countPastOffers,
            editOffer: editOffer,
            updateOffer: updateOffer,
            deleteOfferMedia: deleteOfferMedia,
            deleteOffer:deleteOffer,
            viewOffer: viewOffer,
            searchCurrentOffer: searchCurrentOffer,
            searchPastOffer: searchPastOffer,
            totalOfferReff: totalOfferReff,
            totalReffsInOffer:totalReffsInOffer,
            totalOfferReffPending: totalOfferReffPending,
        };

        return service;
        
        function uploadpic(postData, callback) {
            $http.post(webservices.uploadpic, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function addOffer(postData, callback) {
            $http.post(webservices.addOffer, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    }else if(successCallback.data.status === 501){
                         callback(successCallback.data);
                    }else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function listCurrentOffers(postData,callback) {
            $http.post(webservices.listCurrentOffers, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function listPastOffers(postData,callback) {
            $http.post(webservices.listPastOffers, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function countCurrentOffers(postData, callback) {
            $http.post(webservices.countCurrentOffers, postData)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data.data);
                        callback(successCallback.data.data);
                    } else{
                        callback(errorCallback);
                    }
                });
        }

        function countPastOffers(postData, callback) {
            $http.post(webservices.countPastOffers, postData)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data.data);
                        callback(successCallback.data.data);
                    } else{
                        callback(errorCallback);
                    }
                });
        }

        function editOffer(postData, callback) {
            var serviceUrl = '/edit-offer' + '/' + postData.offerId;
            console.log("servicce:",serviceUrl);
            $http.get(serviceUrl)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data.data);
                        callback(successCallback.data.data);
                    } else{
                        callback(errorCallback);
                    }
                });
        }

        function updateOffer(postData, callback) {
            $http.post(webservices.updateOffer, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function deleteOfferMedia(postData, callback) {
            $http.post(webservices.deleteOfferMedia, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function deleteOffer(postData, callback) {
            $http.post(webservices.deleteOffer, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function viewOffer(postData, callback) {
            var serviceUrl = '/view-offer' + '/' + postData.offerId;
            console.log("servicce:",serviceUrl);
            $http.get(serviceUrl)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function searchCurrentOffer(postData,callback) {
            $http.post(webservices.searchCurrentOffer, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }


        function searchPastOffer(postData,callback) {
            $http.post(webservices.searchPastOffer, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function totalOfferReff(postData,callback) {
            $http.post(webservices.totalOfferReff, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function totalReffsInOffer(postData,callback) {
            $http.post(webservices.totalReffInOffers, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        

        function totalOfferReffPending(postData,callback) {
            $http.post(webservices.totalOfferReffPending, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        
        
        
       ;
    }

})();