(function () {
    'use strict';

    angular
            .module('referral-app')
            .factory('ProfileService', ProfileService);

    ProfileService.$inject = ['$http','$localStorage','$state'];
    function ProfileService($http, $localStorage, $state) {

        var service = {
            uploadpic: uploadpic,
            updateprofile: updateprofile,
            getUserInfo: getUserInfo,
            totalBusinessReff:totalBusinessReff,
            totalReffs: totalReffs,
            totalBusinessReffPending:totalBusinessReffPending
        };

        return service;
        
        function uploadpic(postData, callback) {
            $http.post(webservices.uploadpic, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        
        function updateprofile(postData, callback) {
            $http.post(webservices.updateprofile, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        function getUserInfo(postData, callback) {
            $http.post(webservices.getUserInfo, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function totalBusinessReff(postData,callback) {
            $http.post(webservices.totalBusinessReff, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function totalBusinessReffPending(postData,callback) {
            $http.post(webservices.totalBusinessReffPending, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function totalReffs(postData,callback) {
            $http.post(webservices.totalReffs, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        
        ;
    }

})();