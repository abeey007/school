(function () {
    'use strict';

    angular
            .module('referral-app')
            .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['$state', '$rootScope', 'AuthService', 'ProfileService', '$localStorage', 'toastr'];


    function ProfileController( $state, $rootScope, AuthService, ProfileService, $localStorage, toastr) {

        var vm = this;
        vm.init = init;
        vm.onLoad = onLoad;
        vm.updateProfile = updateProfile;
        vm.logout = logout;
        vm.userInfo = userInfo;
        vm.otherDetailPop = otherDetailPop;
        vm.Close3 = Close3;
        vm.totalBusinessReff = totalBusinessReff;
        vm.totalReffs = totalReffs;
        vm.totalReffsVar = {};
        vm.mngprofile = {};
        vm.totalbusinessRefVar = {};
        vm.totalBusinessPendingVar = "";
        vm.user = {};
        vm.userProfile = '';
        init();
        
        // Initialisation
        function init() {
            
            userInfo();
        }
        
        function userInfo() {
            ProfileService.getUserInfo({_id: $localStorage.currentUser.info._id}, function (result) {
                if(result){
                    vm.user = result;
                    if(vm.user.personalInfo.profileImg === 'user.png'){
                        vm.user.personalInfo.profileImg =  "/images/thumbnail.jpg";
                    }
                    $localStorage.currentUser.info = result;
                    if($localStorage.currentUser){
                      $rootScope.loggeduser = $localStorage.currentUser.info;
                    }
                }
            });
        }
        
        function onLoad(file){
            console.log("uploaded",file)
            ProfileService.uploadpic({file: file}, function (result) {
                if (result.status == 200) {
                    console.log("result:",result.data);
                    vm.userProfile = result.data;
                    vm.user.personalInfo.profileImg = vm.userProfile;
                    toastr.success('Image uploaded successfully.');
                } else {
                    toastr.error('Error! Please try again.');
                    
                }
            });
        }
        
        function updateProfile(){
            console.log("User ----:=",vm.user);
            if(vm.user.personalInfo.profileImg === ''){
                vm.user.personalInfo.profileImg = vm.userProfile;
            }
           
            ProfileService.updateprofile(vm.user, function (result) {
                if (result === true) {
                    vm.user = {};
                    $localStorage.currentUser.newpassword = '';
                    $localStorage.currentUser.confirmpassword= '';
                    vm.mngprofile.$setPristine();
                    vm.mngprofile.$setUntouched();
                    toastr.success('Success! user profile updated successfully.');
//                    userInfo();
                    if($localStorage.currentUser.info.role == 'b_owner'){
                         $state.go('anon.dashboard');
                    } else if($localStorage.currentUser.info.role == 's_owner'){
                         $state.go('anon.s_dashboard');
                    }
                   
                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }

        function logout() {
            AuthService.logoutUser();
        }


        function otherDetailPop(id){
            console.log(id);
            totalBusinessReff(id);
            totalReffs(id);
            totalBusinessReffPending(id);
            $(".popup4").show();
            $(".overlay").show();
            return false;
        }

        function Close3() {
            $(".popup4, .overlay").hide();
            $('.dropdown-menu.over').show();
            $('#areatoggle').addClass('dropdown-toggle');
        }


        
        function totalBusinessReff(businessOwner)
        {
            var query = {};
            query.businessOwner = businessOwner;
            ProfileService.totalBusinessReff(query, function (result) {
                console.log(result);
                 vm.totalbusinessRefVar = result[0];
                 vm.totalbusinessRefVar.referrerEarning = parseFloat(vm.totalbusinessRefVar.referrerEarning).toFixed(2);
            });

        }

        function totalBusinessReffPending(businessOwner)
        {
            var query = {};
            query.businessOwner = businessOwner;
            ProfileService.totalBusinessReffPending(query, function (result) {
                console.log(result);
                vm.totalBusinessPendingVar = result;
               
            });

        }

        function totalReffs(businessOwner)
        {
            var query = {};
            query.businessOwner = businessOwner;
            ProfileService.totalReffs(query, function (result) {
                vm.totalReffsVar = result;
            });

        }

    }
})();
