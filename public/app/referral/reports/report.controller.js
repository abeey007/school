(function () {
    'use strict';

    angular
            .module('referral-app')
            .controller('ReportController', ReportController);

    ReportController.$inject = ['$state', '$rootScope', 'AuthService', 'ReportService', '$localStorage', 'toastr','$compile','$stateParams','$location',"$sce"];


    function ReportController( $state, $rootScope, AuthService, ReportService, $localStorage, toastr,$compile,$stateParams,$location,$sce) {

        var vm = this;
        vm.init = init;

        vm.users = {};
        vm.offers ={};
        vm.sales = {};

        vm.open1 = open1;
        vm.open2 = open2;

        vm.totalReffs = totalReffs;
        vm.totalReffsVar = {};

        vm.totalBusinessReff = totalBusinessReff;
        vm.totalbusinessRefVar = {};


        vm.totalBusinessPendingVar = "";
        vm.totalPendingEarningVar = "";

        vm.exportOverallReferal =exportOverallReferal;

        vm.tab2Functions = tab2Functions;
        vm.acceptedRefferers = acceptedRefferers;

        vm.viewReport = viewReport;
        vm.viewEarningReport = viewEarningReport;
        vm.reportResponse = {};


        vm.popup1 = { opened: false };
        vm.popup2 = { opened: false };
        vm.format = 'MM-dd-yyyy';
        vm.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            startingDay: 1
          };

        vm.dateOptions2 = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            startingDay: 1
          };    
        
        init();
        // Initialisation
        function init() {
            console.log("asfasfaf");
           if( $localStorage.currentUser){
                $rootScope.loggeduser = $localStorage.currentUser.info;
            }
            $( "#accordion" ).accordion();
            totalReffs();
            totalBusinessReff();
            totalBusinessReffPending();
            totalPendingEarning();

        }


        function totalReffs()
        {
            var query = {};
            query.businessOwner = $localStorage.currentUser.info._id;
            ReportService.totalReffs(query, function (result) {
                vm.totalReffsVar = result;
            });

        }


        function totalBusinessReff()
        {
            var query = {};
            query.businessOwner = $localStorage.currentUser.info._id;
            ReportService.totalBusinessReff(query, function (result) {
                console.log(result);
                 vm.totalbusinessRefVar = result[0];
                 vm.totalbusinessRefVar.referrerEarning = parseFloat(vm.totalbusinessRefVar.referrerEarning).toFixed(2);
            });

        }


        function totalBusinessReffPending()
        {
            var query = {};
            query.businessOwner = $localStorage.currentUser.info._id;
            query.cond = "pending";
            ReportService.totalBusinessReffPending(query, function (result) {
                //console.log("pend",result);
                vm.totalBusinessPendingVar = result;
               
            });

        }

        function totalPendingEarning()
        {
            var query = {};
            query.businessOwner = $localStorage.currentUser.info._id;
            query.type = "business";
            ReportService.totalPendingEarning(query, function (result) {
               result[0].referrerEarning =  parseFloat(result[0].referrerEarning).toFixed(2);
               vm.totalPendingEarningVar =result[0];
            });
        }

        function exportOverallReferal(data1,data2,data3,data4,data5){
            var query = {};
            query.data1 = data1;
            query.data2 = data2;
            query.data3 = data3;
            query.data4 = data4;
            query.data5 = data5;

            ReportService.exportOverallReferal(query, function (result) {
               console.log(result);
               window.location = "/downloadcsv/"+result;
            });
        }

        function open1 () {
            vm.popup1.opened = true;
        };
        
        function open2 () {
            vm.popup2.opened = true;
        };


        function tab2Functions(){
            acceptedRefferers();
            allOffers();
            allSalesPerson();

        }

        function acceptedRefferers()
        {
            //vm.conditions.skip = ((vm.currentPage - 1) * vm.itemsPerPage);
            //vm.conditions.limit = 5;
           var query = {};
           query.approved_by = $localStorage.currentUser.info._id;
           
            ReportService.acceptedRefferers(query , function (result) {
               vm.users =result;
            });
        }


        function allOffers()
        {
            //vm.conditions.skip = ((vm.currentPage - 1) * vm.itemsPerPage);
            //vm.conditions.limit = 5;
           var query = {};
           query.userId = $localStorage.currentUser.info._id;
           
            ReportService.allOffers(query , function (result) {
               console.log("rrr",result);
               vm.offers =result;
            });
        }


        function allSalesPerson()
        {
            //vm.conditions.skip = ((vm.currentPage - 1) * vm.itemsPerPage);
            //vm.conditions.limit = 5;
           var query = {};
           query.addedBy = $localStorage.currentUser.info._id;
           query.pagination = false;
           
            ReportService.allSalesPerson(query , function (result) {
               vm.sales = result;
            });
        }


        function viewReport()
        {
            vm.report.businessOwner = $localStorage.currentUser.info._id;
            //console.log(vm.report);
            ReportService.viewReport(vm.report , function (result) {
               //console.log("rrr",result);
               if(result=="empty"){
                    toastr.error('Error ! No result found.');
               }else{
                    window.location = "/downloadcsv/"+result;
               }
            });

        }

        function viewEarningReport()
        {
            vm.report.businessOwner = $localStorage.currentUser.info._id;
            console.log(vm.report);
            ReportService.viewEarningReport(vm.report , function (result) {
               console.log("rrr",result);
               if(result=="empty"){
                    $("#resultReports").hide();
                    toastr.error('Error ! No result found.');
               }else{

                   $("#resultReports").show(); 
                   vm.reportResponse = result;
                    vm.reportResponse.dataEarning[0].referrerEarning =  parseFloat( vm.reportResponse.dataEarning[0].referrerEarning).toFixed(2); 
                    vm.reportResponse.dataPending[0].referrerEarning =  parseFloat( vm.reportResponse.dataPending[0].referrerEarning).toFixed(2);
                   window.location = "/downloadcsv/"+result.csvFileName;
               }
            });

        }

        
       
        
    }
})();
