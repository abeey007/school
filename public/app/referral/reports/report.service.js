(function () {
    'use strict';

    angular
            .module('referral-app')
            .factory('ReportService', ReportService);

    ReportService.$inject = ['$http','$localStorage','$state'];
    function ReportService($http, $localStorage, $state) {

        var service = {
            totalReffs: totalReffs,
            totalBusinessReff:totalBusinessReff,
            totalBusinessReffPending:totalBusinessReffPending,
            totalPendingEarning:totalPendingEarning,
            exportOverallReferal:exportOverallReferal,
            acceptedRefferers:acceptedRefferers,
            allOffers:allOffers,
            allSalesPerson:allSalesPerson,
            viewReport:viewReport,
            viewEarningReport:viewEarningReport,
        };

        return service;
        
        function totalReffs(postData,callback) {
            $http.post(webservices.totalReffs, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function totalBusinessReff(postData,callback) {
            $http.post(webservices.totalBusinessReff, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }


        function totalBusinessReffPending(postData,callback) {
            $http.post(webservices.totalBusinessReffPending, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        console.log(successCallback.data.data);
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function totalPendingEarning(postData, callback) {
            $http.post(webservices.pendingearning, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function exportOverallReferal(postData, callback) {
            $http.post(webservices.exportOverallReferal, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function acceptedRefferers(postData, callback) {
            $http.post(webservices.acceptedrefferers, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function allOffers(postData, callback) {
            $http.post(webservices.alloffers, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function allSalesPerson(postData, callback) {
            $http.post(webservices.viewallSalesowners, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function viewReport(postData, callback) {
            $http.post(webservices.viewreport, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function viewEarningReport(postData, callback) {
            $http.post(webservices.viewearningreport, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        
        



        
       ;
    }

})();