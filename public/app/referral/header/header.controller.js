(function () {
    'use strict';

    angular
            .module('referral-app')
            .controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$state','$timeout','$localStorage','$rootScope' ,'AuthService','toastr','ProfileService'];
       

    function HeaderController($state, $timeout, $localStorage,$rootScope ,AuthService, toastr,ProfileService) {

        var vm = this;
       
        vm.init = init;
        vm.signupPopup = signupPopup;
        vm.loginPopup = loginPopup;
        vm.forgotPasswordPopup = forgotPasswordPopup;
        vm.hideOverlay = hideOverlay;
        vm.close = close;
        vm.closeForgot = closeForgot;
        vm.register = register;
        vm.login = login;
        vm.logout = logout;
        vm.forgotPwd = forgotPwd;
        vm.openPaymentPop = openPaymentPop;
        vm.closePaymentPop = closePaymentPop;
        vm.addPayment = addPayment;
        vm.selectPlanType = selectPlanType;
        vm.viewSalesowner = viewSalesowner;


        vm.forgotpwdform = {};
        vm.signup = {};
        vm.newuser = {};
        vm.status = {};
        vm.user = {};
        vm.forgotpwd = {};
        vm.loginForm ={};
        vm.categories= {};
        vm.membership = {};
        vm.plan = {};
        vm.membershipDetails={};
        vm.ShowMemeberShipPlan = {};

        vm.getCategories = getCategories;

        vm.clearAll = clearAll;


//        $rootScope.rolewise = false;
        $rootScope.allow_offers = true;
        $rootScope.allow_refferers = true;
        $rootScope.allow_reports = false;
        $rootScope.allow_redeem_coupon = true;
        init();
        // Initialisation
        function init() {
            if($localStorage.currentUser){
                
                if($localStorage.currentUser.info.role === 'b_owner'){
                    $rootScope.rolewise = true;
                    viewBusinessowner();
                }else{
                    $rootScope.rolewise = false;
                }

                if($localStorage.currentUser.info.role === 's_owner'){
                    viewSalesowner();
                }
            }
            $("#loginDiv").click(function (e) {
                console.log("ffffff");
                if ($(".button.dropdown").css('visibility') == 'visible') {
                    $(this).addClass('open');
                    $(this).attr('aria-expanded', 'true');
                    return false;
                }
            });

            if($state.current.name == "anon.signup") { getCategories(); }
            getPlansFromAdmin("monthly");

        }
        
        // To open the sign up pop
        function signupPopup(){
            console.log($state.current.name);
            $('.dropdown-menu.over').hide();
            $(".popup").hide();
            $(".overlay").hide();
            $('#areatoggle').removeAttr('aria-expanded');
            $('#areatoggle').removeClass('dropdown-toggle');
            //$state.go("anon.signup");
            
             $state.go("anon.signup");
            
            
            
        }
        
        // To open the login pop
        function loginPopup(){
//            console.log("Hi ,I am login event");
//            if ($(".button.dropdown").css('visibility') == 'visible') {
           console.log("eeeeeee");
            vm.loginForm.$setPristine();
            vm.loginForm.$setUntouched();
            close();
            $('.dropdown-menu.over').show();
            $('button.dropdown').addClass('open');
            $('button.dropdown').attr('aria-expanded', 'true');
//                $('#areatoggle').addClass('aria-expanded');
            $(".overlay").show();
//                console.log($('.dropdown-menu.over').attr('style')+'---');
            $('.dropdown-menu.over').removeClass('hide');
//                console.log($('.dropdown-menu.over').attr('style')+'---');
            return false;
           
        }
        
        // To open the forgot-password popup
        function forgotPasswordPopup() {
            console.log("dddddd");
            //Hide login popup
            //Show new user popup
            $("body").append(''); 
            $(".popup2").show();
            $(".overlay").show();

            $('.button.dropdown').removeClass('open');
            $('#areatoggle').removeAttr('aria-expanded');
            $('.dropdown-menu.over').addClass('hide');
            $('#loginFormId').hide();
            
            $('#areatoggle').removeClass('dropdown-toggle');
            return false;
        }
        
        // To hide the overlay
        function hideOverlay () {
            console.log("cccccc");
            $('.dropdown-menu.over').hide();
            $('.button.dropdown').removeClass('open');
            $('#areatoggle').removeAttr('aria-expanded');
            $(".overlay").hide();
        }
        
        // To close the sign up pop
        function close(){
            console.log("bbbbb");
            $(".popup, .overlay").hide();
//            $('.dropdown-menu.over').show();
            $('#areatoggle').addClass('dropdown-toggle');
        }
        
        // To close the forgot-password pop
        function closeForgot(){
            console.log("aaaaa");
            $(".popup2, .overlay").hide();
            $('.dropdown-menu.over').show();
            $('#areatoggle').addClass('dropdown-toggle');
        }
        
        // To register a user
        function register() {
            
            vm.newuser.role= 'b_owner';
            $localStorage.registerProcess = vm.newuser;
            $state.go('anon.membership');
            /*AuthService.registerUser(vm.newuser, function (result) {
                if (result === true) {
                    vm.newuser = {};
                    vm.signup.$setPristine();
                    vm.signup.$setUntouched();
                    toastr.success('Success! Please check your email.');
                    $(".popup, .overlay").hide();
                } else {
                    if(result.status==201){
                        toastr.error(result.msg);
                    }else{
                        toastr.error('Error! Please try again.');
                   }
                }
            });*/
        }
        
        // To login a user
        function login() {
            console.log("aaaaaaaa");
            AuthService.loginUser(vm.user,function(result){
                if (result.status === 200) {
//                   if(result.data.role == 'referrer' || result.data.role == 'admin'){
//                        vm.user = {};
//                        vm.loginForm.$setPristine();
//                        vm.loginForm.$setUntouched();
//                        hideOverlay();
//                    }
                    vm.user = {};
                    vm.loginForm.$setPristine();
                    vm.loginForm.$setUntouched();
                    $('.dropdown-menu.over').hide();
                    $(".overlay").hide();
                    //console.log("$localStorage.currentUser.info",$localStorage.currentUser.info.role);
                    if($localStorage.currentUser.info.role === 's_owner'){
                        $state.go('anon.s_dashboard');
                    }if($localStorage.currentUser.info.role === 'b_owner'){
                        $state.go('anon.dashboard');
                    }
                    
                    
                } else {
                    toastr.error(result.msg);
                }
            });
        }
        // To remove the session of user
        function logout() {
             $(".overlay").hide();
            AuthService.logoutUser(function (result) {
                hideOverlay();
            });
        }
        
         // To login a user
        function forgotPwd() {
            AuthService.forgotpwd(vm.forgotpwd,function(result){
                if (result === true) {
                    vm.forgotpwd = {};
                    vm.forgotpwdform.$setPristine();
                    vm.forgotpwdform.$setUntouched();
                    $(".popup2, .overlay").hide();
                    $('.dropdown-menu.over').show();
                    $('#areatoggle').addClass('dropdown-toggle');
                    toastr.success('A reset password link has been sent to your email account.Please check your email and junk mail box as well too.');
                } else {
                    toastr.error('Either email or password is incorrect.');
                    
                }
            });
        }

        function getCategories() {
            AuthService.getCategories(function(result){
                if (result.status === 200) {
                    vm.categories = result.data
                } else {
                    toastr.error('Either email or password is incorrect.');
                }
            });
        }


        // To open the sign up pop
        function clearAll(){
            
           vm.newuser = {};
           vm.signup.$setPristine();
           vm.signup.$setUntouched();
        }

        // To open the sign up pop
        function openPaymentPop(plan_id,plan_name,plan_type,price){
            $("#payment-popup").show();
            $(".overlay").show();
            vm.membershipDetails.plan_id = plan_id;
            vm.membershipDetails.plan_name = plan_name;
            vm.membershipDetails.plan_type = plan_type;
            vm.membershipDetails.amount = price;
        }

        function closePaymentPop(){
            $("#payment-popup").hide();
            $(".overlay").hide();
        }

        function addPayment(){
            $("#submit_div").hide();
            $("#loader_img").show();
            vm.card.email = $localStorage.registerProcess.email;
            console.log(vm.card);
            AuthService.registerCard(vm.card, function (result) {
                if(result.status==201){
                    toastr.error(result.data.message);
                    $("#submit_div").show();
                    $("#loader_img").hide();
                }else if(result.status==200){
                    vm.newuser = $localStorage.registerProcess;
                    vm.membershipDetails.stripe_customer_id = result.data;
                    vm.newuser.membershipDetails = vm.membershipDetails;
                    console.log(vm.newuser);
                   AuthService.registerUser(vm.newuser, function (result) {
                        if (result === true) {
                            $localStorage.registerProcess = {};
                            toastr.success('Success! Please check your email and junk mail box as well too');
                            $("#payment-popup").hide();
                            $(".overlay").hide();
                            $("#submit_div").show();
                            $("#loader_img").hide();
                        } else {
                            if(result.status==201){
                                toastr.error(result.msg);
                                $("#submit_div").show();
                                $("#loader_img").hide();
                            }else{
                                toastr.error('Error! Please try again.');
                                $("#submit_div").show();
                                $("#loader_img").hide();
                           }
                        }
                    });
                }
            });
        }

        function getPlansFromAdmin(planType){ 

            var query = {};
            query.planType= planType;
            AuthService.getPlansFromAdmin(query, function (result) {
                console.log("racccccc",result);
                vm.ShowMemeberShipPlan = result.data;
            });
        
       }

       function selectPlanType(planType){ 

            console.log(planType);
            if(planType == "monthly")
            {
                $("#monthly_plan_div").show();
                $("#yearly_plan_div").hide();
                getPlansFromAdmin("monthly");
            }else{
                $("#monthly_plan_div").hide();
                $("#yearly_plan_div").show();
                getPlansFromAdmin("yearly");
            }
        
       }


       function viewSalesowner(){
            
            AuthService.viewSalesowner({staffId: $localStorage.currentUser.info._id}, function (result) {
                if (result) {
                    if(result.addedBy.membershipDetails.plan_name=="basic")
                    {
                        $rootScope.allow_offers = false;
                        $rootScope.allow_refferers = false;
                        $rootScope.allow_redeem_coupon = true;
                    }else{
                        $rootScope.allow_offers = result.allow_offer;
                        $rootScope.allow_refferers = result.allow_refferer;
                        $rootScope.allow_redeem_coupon = result.allow_redeem_coupon;
                    }
                    
                }else{
                    $rootScope.allow_offers = false;
                    $rootScope.allow_refferers = false;
                    $rootScope.allow_redeem_coupon = false;
                } 
            });
        }

        function viewBusinessowner(){
            
            ProfileService.getUserInfo({_id: $localStorage.currentUser.info._id}, function (result) {
                if(result){
                   if(result.membershipDetails.plan_name=="premium" || result.membershipDetails.plan_name=="Premium")
                   {
                        $rootScope.allow_reports = true;
                   }else{
                        $rootScope.allow_reports = false;
                   }
                }
            });
        }

    }
})();
