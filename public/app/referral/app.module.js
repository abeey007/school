(function (angular) {
    'use strict';

    angular.module('referral-app',
            [
                'ngAnimate', 'ngSanitize', 'ui.router', 'ui.bootstrap', 'satellizer', 'ngStorage', 'angularMoment', 'angular-svg-round-progressbar', 'ngFileUpload','naif.base64','ui.bootstrap','toastr'
            ])

            .factory('authInterceptor', ['$localStorage', authInterceptor])
            .config(routeConfig)
            .run(runConfig);

    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider'];
    runConfig.$inject = ['$rootScope', '$state', '$http', '$localStorage'];

    function authInterceptor($localStorage) {

        return {
            request: function (config) {
                config.headers = config.headers || {};
                if ($localStorage.currentUser) {
                    config.headers.Authorization = 'JWT ' + $localStorage.currentUser.token;
                }
                return config;
            }
        };
    }

    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

//      $locationProvider.hashPrefix("!");
//        $locationProvider.html5Mode({
//          enabled: true,
//          requireBase: false
//        });
        var isLoggedIn = function($q, $timeout, $location, $localStorage) {
            // Initialize a new promise
            var deferred = $q.defer();

            if ($localStorage.currentUser)
                $timeout(deferred.resolve);
            else {
                $timeout(deferred.reject);
                $location.url('/');
            }
            return deferred.promise;
        };
        var checkLoggedIn = function($q, $timeout, $location, $localStorage) {
            // Initialize a new promise
           var deferred = $q.defer();
           if (typeof $localStorage.currentUser == 'undefined'){
               $timeout(deferred.resolve);
           }else {
               $timeout(deferred.reject);
               $location.url('/dashboard');
           }
           return deferred.promise;
        };

        var checkRegisterProcess = function($q, $timeout, $location, $localStorage) {
            console.log($localStorage.registerProcess);
            var deferred = $q.defer();

            if ($localStorage.registerProcess)
                $timeout(deferred.resolve);
            else {
                $timeout(deferred.reject);
                $location.url('/');
            }
            return deferred.promise;
        };

         var urlPermission = function($q, $timeout, $location, $localStorage,$state) {
            // Initialize a new promise
           var deferred = $q.defer();
            if ($localStorage.currentUser && $localStorage.currentUser.info.role =="s_owner")
                $location.url('/salesdashboard');
            else {
                $timeout(deferred.resolve);
            }
            return deferred.promise;
        };


        $httpProvider.interceptors.push('authInterceptor');

        $stateProvider
                .state('anon', {
                    abstract: true,
                    template: '<ui-view/>',
                    data: {
                        access: 0
                    }
                })
                .state('anon.home', {
                    url: '/',
                    templateUrl: '/app/referral/home/index.html',
                    controller: 'HomeController',
//                    controllerAs: 'vm',
                    resolve: {
                        checkLoggedIn: checkLoggedIn
                    }
                })
                .state('anon.signup', {
                    url: '/signup',
                    templateUrl: '/app/referral/home/signup.html',
                    controller: 'HeaderController',
                    controllerAs: 'vm',
                    resolve: {
                        checkLoggedIn: checkLoggedIn
                    }
                })
                .state('anon.membership', {
                    url: '/membership',
                    templateUrl: '/app/referral/home/membership.html',
                    controller: 'HeaderController',
                    controllerAs: 'vm',
                    resolve: {
                        checkLoggedIn: checkLoggedIn,
                        checkRegisterProcess: checkRegisterProcess
                    }
                })
                .state('anon.test', {
                    url: '/test',
                    templateUrl: '/app/referral/home/test.html',
                    controller: '',
                    resolve: {
                        checkLoggedIn: checkLoggedIn
                    }
                })
               
                .state('anon.resetpwd', {
                    url: '/reset-password/:token',
                    templateUrl: '/app/layout/resetPassword.html',
                    controller: 'PasswordController',
                    controllerAs: 'vm',
                    resolve: {
                        removeCredentials: function($localStorage) {
                            if($localStorage.currentUser) {
                                delete $localStorage.currentUser;
                            }
                            $localStorage.loginstatus = false;
                        }
                    }
                })
                .state('anon.verify-email', {
                    url: '/verifyemail/:token',
                    templateUrl: '/app/referral/home/accountActivation.html',
                    controller: 'HomeController',
                    controllerAs: 'vm',
                    resolve: {
                        
                    }
                })
               
                .state('anon.dashboard', {
                    url: '/dashboard',
                    templateUrl: '/app/referral/dashboard/dashboard.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn,
                        urlPermission : urlPermission
                    }
              
                })
                .state('anon.s_dashboard', {
                    url: '/redeem-coupon',
                    templateUrl: '/app/referral/dashboard/s_dashboard.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
              
                })
                .state('anon.view-sales-info', {
                    url: '/view-info/:staffId',
                    templateUrl: '/app/referral/dashboard/viewSales.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        verifyEmail: function () {
                            return;
                        }
                    }
                })
                .state('anon.termsconditions', {
                    url: '/termsconditions',
                    templateUrl: '/app/referral/home/termsConditions.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm'
              
                })
                .state('anon.faqs', {
                    url: '/faqs',
                    templateUrl: '/app/referral/home/faqs.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm'
              
                })
                .state('anon.contact', {
                    url: '/contact-us',
                    templateUrl: '/app/referral/home/contactUs.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm'
                })
                 .state('anon.referralPal', {
                    url: '/why-referral-pal',
                    templateUrl: '/app/referral/home/referralPal.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm'
              
                })
                 .state('anon.privacy', {
                    url: '/privacy-policy',
                    templateUrl: '/app/referral/home/privacyPolicy.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm'
              
                })
                
                .state('anon.mngprofile', {
                    url: '/manage-profile',
                    templateUrl: '/app/referral/profile/manageProfile.html',
                    controller: 'ProfileController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.mngmembership', {
                    url: '/manage-membership',
                    templateUrl: '/app/referral/dashboard/membership.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.addoffer', {
                    url: '/add-offer',
                    templateUrl: '/app/referral/offer/addOffer.html',
                    controller: 'OfferController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.offers', {
                    url: '/offers',
                    templateUrl: '/app/referral/offer/offers.html',
                    controller: 'OfferController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.edit-offer', {
                    url: '/edit-offer/:offerId',
                    templateUrl: '/app/referral/offer/editOffer.html',
                    controller: 'OfferController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.view-offer', {
                    url: '/view-offer/:viewofferId',
                    templateUrl: '/app/referral/offer/viewOffer.html',
                    controller: 'OfferController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.view-offer-referrar', {
                    url: '/view-offer-referrar/:viewofferId/:coupon',
                    templateUrl: '/app/referral/offer/viewOfferReferrar.html',
                    controller: 'OfferController',
                    controllerAs: 'vm'
              
                })
                .state('anon.refferer', {
                    url: '/refferer/:refferer_id_profile',
                    templateUrl: '/app/referral/refferer/refferer.html',
                    controller: 'ReffererController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.all-refferers', {
                    url: '/all-refferers',
                    templateUrl: '/app/referral/refferer/all-refferers.html',
                    controller: 'ReffererController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.referrerals', {
                    url: '/referrerals/:refferer_id',
                    templateUrl: '/app/referral/refferer/referrerals.html',
                    controller: 'ReffererController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                .state('anon.reports', {
                    url: '/reports',
                    templateUrl: '/app/referral/reports/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                         isLoggedIn : isLoggedIn
                    }
                })
                ; 
        $urlRouterProvider.otherwise('/');
        $locationProvider.html5Mode(true);
    }

    function runConfig($rootScope, $state, $http, $localStorage) {
        $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
            window.scrollTo(0, 0);
            $rootScope.loginstatus = $localStorage.loginstatus;
        });
    }
    
})(window.angular);
