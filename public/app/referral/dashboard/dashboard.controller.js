(function () {
    'use strict';

    angular
            .module('referral-app')
            .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$state','$rootScope', 'AuthService', 'DashboardService', '$localStorage','$stateParams','toastr','$scope'];


    function DashboardController($state,$rootScope, AuthService, DashboardService, $localStorage, $stateParams, toastr,$scope) {

        var vm = this;
        
        vm.init = init;
        vm.logout = logout;
        vm.openPopup = openPopup;
        vm.editPopup = editPopup;
        vm.deletePop = deletePop;
        vm.close = close;
        vm.closeEdit = closeEdit;
        vm.Close2 = Close2;
        vm.Close3 = Close3;
        vm.addEmp = addEmp;
        vm.editEmp = editEmp;
        vm.deleteEmp = deleteEmp;
        vm.searchUser = searchUser;
        vm.onLoad = onLoad;
        vm.editOnLoad = editOnLoad;
        vm.viewSalesowner = viewSalesowner;
        vm.countStaff = countStaff;
        vm.pageChanged = pageChanged;
        vm.membershipUpdated = membershipUpdated;
        vm.searchCoupon = searchCoupon;
        vm.redeemCoupon = redeemCoupon;
        vm.rejectCoupon = rejectCoupon;
        vm.contactEmail =  contactEmail;
        vm.viewMemberShip =  viewMemberShip;
        vm.selectPlanType = selectPlanType;
        vm.updatePlan = updatePlan;
        vm.renewConfirm = renewConfirm;
        vm.renewMembership = renewMembership;
        vm.renewCancel = renewCancel;
        vm.cancelMembership = cancelMembership;
        vm.ShowMemeberShipPlan = {};
        $rootScope.allowOfferCheck = false;
        
        // variables
        vm.sales = {};
        vm.editUser = {
            personalInfo:{
                
            }
        };
        vm.editsales = {};
        vm.contact = {};
        vm.saleuser = {};
        vm.membership = {};
       
        vm.conditions = {};
        vm.membershipPlan = {};
        vm.pageData = {};
        vm.salesUsers = [];
        vm.searchquery = '';
        vm.userProfile = '';
        vm.newuser = {
            personalInfo:{
                profileImg: "/images/thumbnail.jpg"
            },
            business:{}

        };
        if(vm.editUser.personalInfo.profileImg === 'user.png'){
            vm.editUser.personalInfo.profileImg = "/images/thumbnail.jpg";
        }


        // pagination variables
        vm.currentPage = 1;
        vm.maxsize = 2;
        vm.itemsPerPage = 5;
        vm.staffId = '';
        init();
        
        // Initialisation
        function init() {
            if( $localStorage.currentUser){
                $rootScope.loggeduser = $localStorage.currentUser.info;
                countStaff();
                viewallstaff();
                viewMemberShip();
                getPlansFromAdmin("monthly");

            }
            
            if($stateParams.staffId){
                console.log("fdfd: ",$stateParams.staffId);
                viewSalesowner();  
            }

            var state = $state.current.name;
            console.log(state);
            if(state == "anon.referralPal")
            {
                getStaticData("5902e43434519b5cba0e5610");
            }

            if(state == "anon.privacy")
            {
                getStaticData("5902e45534519b5cba0e5611");
            }

            if(state == "anon.termsconditions")
            {
                getStaticData("5902e45b34519b5cba0e5612");
            }

            if(state == "anon.faqs")
            {
                getStaticData("5993f07afa89ee08a7c53da9");
            }

            
        }
        
        function membershipUpdated(plan){
           
            if(plan === 1){
                vm.membershipPlan ={
                    _id : $localStorage.currentUser.info._id,
                    plan_name: "Bronze",
                    amount: 99,
                    offers_limit: 1,
                    refferer_limit: 75,
                    duration: "monthly"
                };
            } else if(plan === 2){
                vm.membershipPlan ={
                    _id : $localStorage.currentUser.info._id,
                    plan_name: "Gold",
                    amount: 169,
                    offers_limit: 1,
                    refferer_limit: 75,
                    duration: "monthly"
                };
            } else if(plan === 3){
                vm.membershipPlan ={
                    _id : $localStorage.currentUser.info._id,
                    plan_name: "Platinum",
                    amount: 250,
                    offers_limit: 1,
                    refferer_limit: 75,
                    duration: "monthly"
                };
            } else{
            }
            DashboardService.membershipDetails(vm.membershipPlan, function (result) {
                if (result === true) {
                    toastr.success('Plan updated successfully.');
                } else {
                    toastr.error('Error occured while updating.');
                }
            });
        }
        
        function countStaff(){
            vm.addedBy= $localStorage.currentUser.info._id;
            DashboardService.countStaff({addedBy: vm.addedBy}, function (count) {
                vm.totalItems  = count;
                vm.noofPages  = Math.ceil(vm.totalItems / vm.itemsPerPage);
                console.log("vm.totalItems",vm.totalItems);
                console.log("vm.noofPages",vm.noofPages);
            });
        }
        
        function viewallstaff(){
            vm.conditions.skip = ((vm.currentPage - 1) * vm.itemsPerPage);
            vm.conditions.limit = 5;
            vm.conditions.addedBy= $localStorage.currentUser.info._id;
            DashboardService.viewallSalesowners(vm.conditions,function (result) {
               if (result) {
                   console.log("Data", result);
                   vm.salesUsers =  result;
               } else {
                   
               }
           });
        }
        
        function pageChanged(){
            viewallstaff();
        }

        // To logout 
        function logout() {
            AuthService.logoutUser();
        }
        
        // To open pop up
        function openPopup(){
            vm.newuser = {
                personalInfo:{
                    profileImg: "/images/thumbnail.jpg"
                },
                business:{}
            };
            
            $('.dropdown-menu.over').hide();
            $(".popup").show();
            $(".overlay").show();
            $('.button.dropdown').removeClass('open');
            $('#areatoggle').removeAttr('aria-expanded');
            $('#areatoggle').removeClass('dropdown-toggle');
            console.log($localStorage.currentUser.info.membershipDetails.plan_name);
            if($localStorage.currentUser.info.role === 'b_owner' && $localStorage.currentUser.info.membershipDetails.plan_name=="basic"){
                    $rootScope.allowOfferCheck = false;
                }else{
                    $rootScope.allowOfferCheck = true;
                }

            return false;
        }
        
        // To open edit popup
        function editPopup(staff){
            console.log("Edit popup");
            vm.editUser=staff;
            $('.dropdown-menu.over').hide();
            $(".popup3").show();
            $(".overlay").show();
            $('.button.dropdown').removeClass('open');
            $('#areatoggle').removeAttr('aria-expanded');
            $('#areatoggle').removeClass('dropdown-toggle');

            if($localStorage.currentUser.info.role === 'b_owner' && $localStorage.currentUser.info.membershipDetails.plan_name=="basic"){
                    $rootScope.allowOfferCheck = false;
                }else{
                    $rootScope.allowOfferCheck = true;
                }

            return false;
        }
        
        function deletePop(salesId){
            console.log("delete popup",salesId);
            vm.staffId = salesId;
            $(".popup2").show();
            $(".overlay").show();
            $('.button.dropdown').removeClass('open');
            $('#areatoggle').removeAttr('aria-expanded');
            $('.dropdown-menu.over').hide();
            $('#areatoggle').removeClass('dropdown-toggle');
            return false;
        }
        // To close pop up
        function close(){
            $(".popup, .overlay").hide();
            $(".popup3, .overlay").hide();
            
            $('#areatoggle').addClass('dropdown-toggle');
        }
        
        // To close eidt pop up
        function closeEdit(){
            $(".popup3, .overlay").hide();
            $('#areatoggle').addClass('dropdown-toggle');
        }
        
        function Close2() {
            $(".popup2, .overlay").hide();
            $('.dropdown-menu.over').show();
            $('#areatoggle').addClass('dropdown-toggle');
        }

        function Close3() {
            $(".popup3, .overlay").hide();
            $('.dropdown-menu.over').show();
            $('#areatoggle').addClass('dropdown-toggle');
        }
        
        function onLoad(file){
            vm.userProfile = '';
            
            DashboardService.uploadpic({file: file.base64}, function (result) {
                if (result.status === 200) {
                    console.log("result:",result.data);
                    vm.userProfile = result.data;
                   
                    vm.newuser.personalInfo.profileImg = vm.userProfile;
                    toastr.success('Image uploaded successfully.'); 
                } else {
                    toastr.error('Error! Please try again.');
                   
                }
            });
        }
         function editOnLoad(file){
            
            DashboardService.uploadpic({file: file.base64}, function (result) {
                if (result.status === 200) {
                    console.log("result:",result.data);
                    vm.userProfile = result.data;
                    vm.editUser.personalInfo.profileImg = vm.userProfile;
                    toastr.success('Image uploaded successfully.'); 
                } else {
                    toastr.error('Error! Please try again.');
                    
                }
            });
        }
        // To add new sales manager
        function addEmp(valid){
            console.log("ss",vm.userProfile);
            if(typeof vm.userProfile=="string"){
                vm.newuser.personalInfo.profileImg = vm.userProfile;
            }
            vm.newuser.role= 's_owner';
            vm.newuser.addedBy= $localStorage.currentUser.info._id;
            console.log(vm.newuser);
            vm.newuser.business.name = $localStorage.currentUser.info.business.name;
            vm.newuser.business.category = $localStorage.currentUser.info.business.category;
            vm.newuser.business.description = $localStorage.currentUser.info.business.description;
            DashboardService.addUserDetail(vm.newuser, function (result) {
                if (result === true) {
                    vm.newuser = {};
                    vm.userProfile= {};
                    vm.sales.$setPristine();
                    vm.sales.$setUntouched();
                    toastr.success('Success! user added successfully.');
                    $(".popup, .overlay").hide();
                    countStaff();
                    
                    viewallstaff();
                }else if(result == 201){
                     toastr.error('Error ! Email Address already exist.');
                }else if(result == 501){
                     toastr.error('Error ! Your Sales Owner limit has been reached or your membership has been expired. Please upgrade your membership.');
                }else if(result == 505){
                     toastr.error('Error ! Please renew your membership.');
                } else {
                    toastr.error('Error while adding new store owner.');
                }
            });
        }
        
        // To edit sales manager info
        function editEmp(){
            console.log("Edited successfully.");
            if(vm.editUser.personalInfo.profileImg === ''){
                vm.editUser.personalInfo.profileImg = vm.userProfile;
            }
            DashboardService.editUserDetail(vm.editUser, function (result) {
                console.log(result);
                if (result === true) {
                    toastr.success('User details updated successfully.');                    
                    $(".popup3, .overlay").hide();
                    viewallstaff();
                } else {
                    toastr.error('Error occured while updating user details.');   
                    
                }
            });
        }
        
        // To delete sales manager info
        function deleteEmp(){
            console.log("deleted successfully.",vm.staffId);
            DashboardService.deleteUser({_id: vm.staffId,addedBy:$localStorage.currentUser.info.addedBy}, function (result) {
                if (result === true) {
                    vm.staffId ='';
                    toastr.success('User deleted successfully.');    
                    if($stateParams.staffId){
                        Close2();
                        $state.go('anon.dashboard');
                    }else{
                        Close2();
                    }
                    countStaff();
                    viewallstaff();
                } else {
                    toastr.error('Error occured while deleting user.');
                }
            });
           
        }
        
        // To search sales manager info
        function searchUser(searchQuery){
            console.log("search successfully.");
             
            var query = {};
                query.addedBy = $localStorage.currentUser.info._id;
                query.searchData = searchQuery;
                 
            DashboardService.searchUser(query, function (result) {
                if (result.length > 0) {
                    vm.salesUsers = result;
                    countStaff();
                    toastr.success('Success !'); 
                   
                } else if(result.length === 0){
                    toastr.error('No user found !'); 
                   
                }else{
                    toastr.error('No user found !'); 
                }
            });
        }
    
        
        // To view sales manager info
        function viewSalesowner(){
            
            DashboardService.viewSalesowner({staffId: $stateParams.staffId}, function (result) {
                if (result) {
                    vm.saleuser = result;
                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }

        function searchCoupon()
        {
            var query = {};
            query.couponCode = vm.searchquery;
            query.userCateogry = $localStorage.currentUser.info.business.category;
            DashboardService.searchCoupon(query, function (result) {
                console.log(result);
                if (result.length > 0) {
                    console.log(result);
                   result[0].offerDetails.remuneration =  parseFloat(result[0].offerDetails.remuneration).toFixed(2);
                   result[0].offerDetails.discount = parseFloat(result[0].offerDetails.discount).toFixed(2);
                    vm.user = result;
                    toastr.success('Success!'); 
                   
                } else{
                    toastr.error('You are not authorized to check this coupon'); 
                }
            });
        }


        function redeemCoupon()
        {
            var currentDate = new Date();
            var offerEndDate = new Date(vm.user[0].offerDetails.endDate);
            console.log(currentDate);
            console.log(offerEndDate);
            console.log(formatDate(currentDate));
            if(formatDate(currentDate) <= formatDate(offerEndDate))
            {
               var query = {};
                query.couponCode = vm.user[0].couponCode;
                query.redeemed = true;
                query.redeemedBy = $localStorage.currentUser.info._id;
                query.status = "Valid";
                query.date = new Date().toString();
                query.referrerEarning = vm.user[0].offerId.remuneration;

                query.userId= $localStorage.currentUser.info._id;
               
                if($localStorage.currentUser.info.role == "s_owner")
                {
                   query.userId= $localStorage.currentUser.info.addedBy;
                }

                console.log(query);
                DashboardService.redeemCoupon(query, function (result) {
                    console.log(result);
                    if (result==200) {
                        console.log(result);
                        toastr.success('Success ! Coupon is successfully redeemed');
                        vm.user[0].status =  query.status;
                       
                    }else if(result == 201){
                        toastr.error('This coupon is already used.'); 
                       
                    }else{
                        toastr.error('No coupon details found !'); 
                    }
                });
            }else{
                toastr.error('This coupon is expired !'); 
            }

        }

        function rejectCoupon()
        {
            var currentDate = new Date();
            var offerEndDate = new Date(vm.user[0].offerId.endDate);
            if(formatDate(currentDate) <= formatDate(offerEndDate))
            {
                var query = {};
                query.couponCode = vm.user[0].couponCode;
                query.redeemed = true;
                query.redeemedBy = $localStorage.currentUser.info._id;
                query.status = "Invalid";
                query.date = new Date().toString();
                query.userId= $localStorage.currentUser.info._id;
                query.referrerEarning = vm.user[0].offerId.remuneration;
               
                if($localStorage.currentUser.info.role == "s_owner")
                {
                   query.userId= $localStorage.currentUser.info.addedBy;
                }
                DashboardService.redeemCoupon(query, function (result) {
                    console.log(result);
                    if (result==200) {
                        console.log(result);
                        toastr.success('Success ! Coupon is marked invalid');
                        vm.user[0].status =  query.status;
                       
                    }else if(result == 201){
                        toastr.error('This coupon is already used.'); 
                       
                    }else{
                        toastr.error('No coupon details found !'); 
                    }
                });
            }else{
                 toastr.error('This coupon is expired !'); 
            }    
        }


        // To edit sales manager info
        function contactEmail(){
            console.log(vm.contact);
            DashboardService.contactSend(vm.contact, function (result) {
                if (result == 200) {
                    toastr.success('Your email has been sent successfully.');   
                    vm.contact= {};
                    vm.contactForm.$setPristine();
                    vm.contactForm.$setUntouched();                 
                    
                } else {
                    toastr.error('Error occured while sending email.');   
                    
                }
            });
        }

       function viewMemberShip(){
            var query = {};
            query._id= $localStorage.currentUser.info._id;
            DashboardService.viewMemberShip(query, function (result) {
                    console.log(result);
                    vm.membership = result;
                    vm.membership.membershipDetails.amount= vm.membership.membershipDetails.amount.toFixed(2);
                    $localStorage.currentUser.info.membershipDetails.plan_name = vm.membership.membershipDetails.plan_name;
                });
       }

       function getPlansFromAdmin(planType){ 

            var query = {};
            query.planType= planType;
            DashboardService.getPlansFromAdmin(query, function (result) {
                console.log(result);
                vm.ShowMemeberShipPlan = result.data;
            });
        
       }

       function selectPlanType(planType){ 

            console.log(planType);
            if(planType == "monthly")
            {
                $("#monthly_plan_div").show();
                $("#yearly_plan_div").hide();
                getPlansFromAdmin("monthly");
            }else{
                $("#monthly_plan_div").hide();
                $("#yearly_plan_div").show();
                getPlansFromAdmin("yearly");
            }
        
       }


       function updatePlan(plan_id,plan_name,plan_type,price){
            var query = {};
            query.id = $localStorage.currentUser.info._id;
            query.membership = vm.membership.membershipDetails;
            query.membership.plan_id = plan_id;
            query.membership.plan_name = plan_name;
            query.membership.plan_type = plan_type;
            query.membership.amount = price;
            DashboardService.updatePlan(query, function (result) {
                    console.log("rrr",result);
                    if(result==200)
                    {
                        toastr.success('Success ! Membership upgraded successfully.');
                    }else if(result==205){
                        toastr.error('Error ! You are not allowed to degrade the plan.');
                    }else if(result==206){
                        toastr.error('Error ! This plan is already selected.');
                    }else{
                       toastr.error('Error, some problem occoured.');   
                    }
                     viewMemberShip();
                    //vm.membership = result;
                    //vm.membership.membershipDetails.amount= result.membershipDetails.amount.toFixed(2);
                });
        }


        function renewConfirm(){
            $(".popup2").show();
            $(".overlay").show();
        }

        function renewMembership(){
            $("#submit_div").hide();
            $("#loader_img").show();
            var query = {};
            query.id = $localStorage.currentUser.info._id;
            DashboardService.renewMembership(query, function (result) {
                    $("#submit_div").show();
                    $("#loader_img").hide();
                    if(result.status==200)
                    {
                         toastr.success('Success ! Membership plan renewed successfully');
                         viewMemberShip();
                         $(".popup2").hide();
                         $(".overlay").hide();
                        
                    }else if(result.status==201){
                            toastr.error(result.data.msg);
                    }else{
                        toastr.error('Error, some problem occoured.');
                    }        
                            
                    
                });
        }


        function renewCancel(){
            $(".popup3").show();
            $(".overlay").show();
        }


        function cancelMembership(plan_id,plan_name,plan_type,price){
            $("#submit_div").hide();
            $("#loader_img").show();
            var query = {};
            query.id = $localStorage.currentUser.info._id;
            DashboardService.cancelMembership(query, function (result) {
                    $("#submit_div").show();
                    $("#loader_img").hide();
                    if(result.status==200)
                    {
                         toastr.success('Success ! Membership plan cancelled successfully');
                         viewMemberShip();
                         $(".popup2").hide();
                         $(".popup3").hide();
                         $(".overlay").hide();
                        
                    }else if(result.status==201){
                            toastr.error(result.data.msg);
                    }else{
                        toastr.error('Error, some problem occoured.');
                    }        
                            
                    
                });
        }

        function getStaticData(id){
            var query = {};
            query.id = id;
            DashboardService.getStaticData(query, function (result) {
                    console.log(result);
                    vm.pageData = result.data;
                    $scope.myHTML =result.data.description; 
                });
        }

        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [year, month, day].join('-');
        }


   
    }
})();

 angular.module('referral-app').filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    };
});