(function () {
    'use strict';

    angular
            .module('referral-app')
            .factory('DashboardService', DashboardService);

    DashboardService.$inject = ['$http','$localStorage','$state'];
    function DashboardService($http, $localStorage, $state) {

        var service = {
            viewallSalesowners: viewallSalesowners,
            addUserDetail: addUserDetail,
            editUserDetail: editUserDetail,
            deleteUser: deleteUser,
            searchUser: searchUser,
            uploadpic: uploadpic,
            viewSalesowner: viewSalesowner,
            countStaff: countStaff,
            membershipDetails: membershipDetails,
            searchCoupon: searchCoupon,
            redeemCoupon: redeemCoupon,
            contactSend:contactSend,
            viewMemberShip:viewMemberShip,
            updatePlan:updatePlan,
            renewMembership:renewMembership,
            cancelMembership:cancelMembership,
            getPlansFromAdmin:getPlansFromAdmin,
            getStaticData:getStaticData
        };

        return service;

        function viewallSalesowners(postData, callback) {
            $http.post(webservices.viewallSalesowners, postData)
                    .then(function (successCallback, errorCallback) {
                        if(successCallback.status === 200) {
                            console.log("service:", successCallback.data.data);
                            callback(successCallback.data.data);
                        } else{
                            callback(errorCallback);
                        }
                    });
        }
        
        function addUserDetail(postData,callback) {
            $http.post(webservices.addUserDetail, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else if(successCallback.data.status === 201 || successCallback.data.status === 501){
                           callback(successCallback.data.status); 
                    }else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }
        
        function editUserDetail(postData,callback) {
            $http.post(webservices.editUserDetail, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }
        
        function deleteUser(postData,callback) {
            $http.post(webservices.deleteUser, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }
        
        function searchUser(postData,callback) {
            $http.post(webservices.searchStaff, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }
        function uploadpic(postData, callback) {
            $http.post(webservices.uploadpic, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        function viewSalesowner(postData, callback) {
            var serviceUrl = '/view-staff' + '/' + postData.staffId;
            console.log("servicce:",serviceUrl);
            $http.get(serviceUrl)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data.data);
                        callback(successCallback.data.data);
                    } else{
                        callback(errorCallback);
                    }
                });
        }
        function countStaff(postData, callback) {
            $http.post(webservices.countStaff, postData)
                    .then(function (successCallback, errorCallback) {
                        if(successCallback.status === 200) {
                            console.log("service:", successCallback.data.data);
                            callback(successCallback.data.data);
                        } else{
                            callback(errorCallback);
                        }
                    });
        }
        function membershipDetails(postData,callback) {
            $http.post(webservices.membershipDetails, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function searchCoupon(postData,callback) {
            $http.post(webservices.searchcoupon, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }


        function redeemCoupon(postData,callback) {
            $http.post(webservices.redeemcoupon, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status) {
                        callback(successCallback.data.status);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function contactSend(postData,callback) {
            $http.post(webservices.contactsend, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status) {
                        callback(successCallback.data.status);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function viewMemberShip(postData,callback) {
            $http.post(webservices.viewmembership, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status==200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function updatePlan(postData,callback) {
            $http.post(webservices.updateplan, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status==200) {
                        callback(successCallback.data.status);
                    } else {
                        callback(successCallback.data.status);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function renewMembership(postData,callback) {
            $http.post(webservices.renewplan, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status==200) {
                        callback(successCallback.data);
                    }else if (successCallback.data.status==201) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function cancelMembership(postData,callback) {
            $http.post(webservices.cancelplan, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status==200) {
                        callback(successCallback.data);
                    }else if (successCallback.data.status==201) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }


        function getPlansFromAdmin(postData,callback) {
            $http.post(webservices.getPlansFromAdmin, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status==200) {
                        callback(successCallback.data);
                    }else if (successCallback.data.status==201) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        function getStaticData(postData,callback) {
            $http.post(webservices.getStaticData, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status==200) {
                        callback(successCallback.data);
                    }else if (successCallback.data.status==201) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });
        }

        

        

        ;
    }

})();