(function () {
    'use strict';

    angular
            .module('referral-app')
            .factory('ReffererService', ReffererService);

    ReffererService.$inject = ['$http','$localStorage','$state'];
    function ReffererService($http, $localStorage, $state) {

        var service = {
            viewReffrer: viewReffrer,
            acceptRefferer: acceptRefferer,

            rejectRefferer:rejectRefferer,
            acceptedRefferers: acceptedRefferers,
            listAllRefferers: listAllRefferers,
            countRefferers: countRefferers,
            searchRefferer: searchRefferer,
            allcoupons:allcoupons,
            countCoupons:countCoupons,
            searchCoupon:searchCoupon,
            totalEarning:totalEarning,
            successRefferes:successRefferes,
            pendingRefferes:pendingRefferes,
            pendingEarning:pendingEarning,
            paymentStatus: paymentStatus

        };

        return service;
       
        function viewReffrer(postData, callback) {
            var serviceUrl = '/view-owner' + '/' + postData.ownerId + '/'+postData.addedBy;
            console.log("servicce:",serviceUrl);
            $http.get(serviceUrl)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data);
                        callback(successCallback.data);
                    } else{
                        callback(errorCallback);
                    }
                });
        }


        function acceptRefferer(postData, callback) {
            $http.post(webservices.acceptrefferer, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200 || successCallback.data.status === 201 || successCallback.data.status === 501 || successCallback.data.status === 502 || successCallback.data.status === 505) {
                        callback(successCallback.data.status);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function rejectRefferer(postData, callback) {
            $http.post(webservices.rejectrefferer, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200 || successCallback.data.status === 201) {
                        callback(successCallback.data.status);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function acceptedRefferers(postData, callback) {
            $http.post(webservices.acceptedrefferers, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function listAllRefferers(postData, callback) {
            $http.post(webservices.listallrefferers, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function countRefferers(postData, callback) {
            $http.post(webservices.countrefferers, postData)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data.data);
                        callback(successCallback.data.data);
                    } else{
                        callback(errorCallback);
                    }
                });
        }

        function searchRefferer(postData, callback) {
            $http.post(webservices.searchrefferer, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function allcoupons(postData, callback) {
            $http.post(webservices.allcoupons, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function countCoupons(postData, callback) {
            $http.post(webservices.countcoupons, postData)
                .then(function (successCallback, errorCallback) {
                    if(successCallback.status === 200) {
                        console.log("service:", successCallback.data.data);
                        callback(successCallback.data.data);
                    } else{
                        callback(errorCallback);
                    }
                });
        }

        function searchCoupon(postData, callback) {
            $http.post(webservices.searchcoupons, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function totalEarning(postData, callback) {
            $http.post(webservices.totalearning, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        

        function successRefferes(postData, callback) {
            $http.post(webservices.successrefferes, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function pendingRefferes(postData, callback) {
            $http.post(webservices.pendingrefferes, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function pendingEarning(postData, callback) {
            $http.post(webservices.pendingearning, postData)
                .then(function (successCallback, errorCallback) {
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function paymentStatus(postData, callback) {
            $http.post(webservices.paymentstatus, postData)
                .then(function (successCallback, errorCallback) {
                    console.log(successCallback);
                    if (successCallback.data.status === 200 || successCallback.data.status === 201 || successCallback.data.status === 202) {
                        callback(successCallback.data.status);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        
        
       ;
    }

})();