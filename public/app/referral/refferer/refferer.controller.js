(function () {
    'use strict';

    angular
            .module('referral-app')
            .controller('ReffererController', ReffererController);

    ReffererController.$inject = ['$state', '$rootScope', 'AuthService', 'ReffererService', '$localStorage', 'toastr','$compile','$stateParams','$location'];


    function ReffererController( $state, $rootScope, AuthService, ReffererService, $localStorage, toastr,$compile,$stateParams,$location) {

        var vm = this;
        vm.init = init;
        vm.viewRefferer = viewRefferer;
        vm.acceptProfile = acceptProfile;
        vm.acceptedRefferers = acceptedRefferers;
        vm.listAllRefferers = listAllRefferers;
        vm.countRefferers = countRefferers;
        vm.searchRefferer = searchRefferer;
        vm.allcoupons = allcoupons;
        vm.pageChangedCoupon = pageChangedCoupon;
        vm.searchCoupon = searchCoupon;
        vm.changePaymentStatus = changePaymentStatus;
        

        vm.rejectPopup = rejectPopup;
        vm.Close2 = Close2;
        vm.rejectRefferer = rejectRefferer;
        vm.conditions = {};
        vm.conditionsAll = {};

        vm.user = [];
        vm.alluser = [];
        vm.acceptUser = {};
        vm.rejectUser ={};
        vm.approved_by ="";
        vm.extraData ={};

        vm.currentPage = 1;
        vm.maxsize = 2;
        vm.itemsPerPage = 5;

        vm.currentPageAll = 1;
        vm.maxsizeAll = 2;
        vm.itemsPerPageAll = 5;


        vm.pageChanged = pageChanged;
        vm.pageChangedAll = pageChangedAll;

        vm.userIndex = "";
        vm.userType = "";

        init();
       

     
        // Initialisation
        function init() {
           if( $localStorage.currentUser){
                $rootScope.loggeduser = $localStorage.currentUser.info;
            }

            
            if($stateParams.refferer_id){
                allcoupons();
                countCoupons();
                
            }else if($stateParams.refferer_id_profile){
                viewRefferer();
                totalEarning();
                successRefferes();
                pendingRefferes();
                pendingEarning();
            }else{
                acceptedRefferers();
                countRefferers("accepted");
            }

            

        }


        function viewRefferer() {
          
           if($localStorage.currentUser.info.role == "s_owner") 
            {
                var approved_by = $localStorage.currentUser.info.addedBy;
                
            }else{
                var approved_by = $localStorage.currentUser.info._id;
            }

           ReffererService.viewReffrer({ownerId: $stateParams.refferer_id_profile,addedBy:approved_by}, function (result) {
                if (result) {
                    console.log("aaaaacccccc",result.extraData);

                    vm.user = result.data;
                    vm.extraData = result.extraData
                
                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }


        function acceptProfile(id,index,type)
        {

            vm.acceptUser.userId= id;
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                vm.acceptUser.approved_by = $localStorage.currentUser.info.addedBy;
                vm.acceptUser.salesOwner = $localStorage.currentUser.info.personalInfo.name;
            }else{
                vm.acceptUser.approved_by = $localStorage.currentUser.info._id;
            }
            
            console.log(vm.acceptUser);
            ReffererService.acceptRefferer(vm.acceptUser, function (result) {
                console.log(result);
                if (result==200) {
                     toastr.error('Success! You have accepted the referrer.');
                     vm.alluser.splice(index, 1);
                } else if(result==201){
                    toastr.error('Error! This referrer is already in your list.');
                }else if(result==501){
                    toastr.error('Error! Your Referrer limit has been reached. Please upgrade your membership');
                }else if(result==502){
                    toastr.error('Error! Your Referrer limit has been reached for this month.');
                }else if(result==505){
                    toastr.error('Error! Please renew your membership.');
                }else {
                    toastr.error('Error! Please try again.');
                }
            });
        }


        function rejectPopup(user_id){
            
            $(".popup2").show();
            $(".overlay").show();
            $('.button.dropdown').removeClass('open');
            $('#areatoggle').removeAttr('aria-expanded');
            $('.dropdown-menu.over').hide();
            $('#areatoggle').removeClass('dropdown-toggle');
            vm.user._id = user_id;
            return false;
        }

        function Close2() {
            $(".popup2, .overlay").hide();
            $('.dropdown-menu.over').show();
            $('#areatoggle').addClass('dropdown-toggle');
        }

        function rejectRefferer()
        {
            console.log(vm.user);
            vm.rejectUser.userId= vm.user._id;
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                vm.rejectUser.approved_by = $localStorage.currentUser.info.addedBy;
            }else{
                vm.rejectUser.approved_by = $localStorage.currentUser.info._id;
            }
            
            vm.rejectUser.reason = vm.reject.reason;
            console.log(vm.rejectUser);
            ReffererService.rejectRefferer(vm.rejectUser, function (result) {
                if (result==200) {
                     toastr.error('Success! You have rejected the referrer.');
                     if(vm.userType =="accepted")
                     {
                        vm.user.splice(vm.userIndex, 1);
                     }
                     $(".popup2, .overlay").hide();
                     $('.dropdown-menu.over').show();
                     $('#areatoggle').addClass('dropdown-toggle');
                } else if(result==201){
                    toastr.error('Error! This referrer is already rejected.');
                    $(".popup2, .overlay").hide();
                    $('.dropdown-menu.over').show();
                    $('#areatoggle').addClass('dropdown-toggle');
                }else {
                    toastr.error('Error! Please try again.');
                }
            });
        }


        function acceptedRefferers()
        {
            vm.conditions.skip = ((vm.currentPage - 1) * vm.itemsPerPage);
            vm.conditions.limit = 5;

            if($localStorage.currentUser.info.role == "s_owner") 
            {
                vm.conditions.approved_by = $localStorage.currentUser.info.addedBy;
            }else{
                vm.conditions.approved_by = $localStorage.currentUser.info._id;
            }
            
            console.log(vm.conditions);
            ReffererService.acceptedRefferers(vm.conditions , function (result) {
               console.log(result);
               vm.user =result;
            });
        }


        function countRefferers(type){
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                vm.conditions.approved_by = $localStorage.currentUser.info.addedBy;
            }else{
                vm.conditions.approved_by = $localStorage.currentUser.info._id;
            }
            vm.conditions.type = type;
            ReffererService .countRefferers(vm.conditions, function (count) {
                if(type == "all"){
                    vm.totalItemsAll  = count;
                    vm.noofPagesAll  = Math.ceil(vm.totalItems / vm.itemsPerPage);
                }else{
                    vm.totalItems  = count;
                    vm.noofPages  = Math.ceil(vm.totalItems / vm.itemsPerPage);
                }
            });
        }

        function listAllRefferers()
        {

            if($localStorage.currentUser.info.role == "s_owner") 
            {
                vm.conditionsAll.approved_by = $localStorage.currentUser.info.addedBy;
            }else{
                vm.conditionsAll.approved_by = $localStorage.currentUser.info._id;
            }
            vm.conditionsAll.skip = ((vm.currentPageAll - 1) * vm.itemsPerPageAll);
            vm.conditionsAll.limit = 5;
            
            countRefferers("all");

            ReffererService.listAllRefferers(vm.conditionsAll , function (result) {
               console.log(result);
               vm.alluser =result;
            });
        }

        function pageChanged(){
            acceptedRefferers();
        }

        function pageChangedAll(){
            listAllRefferers();
        }


        function searchRefferer(searchQuery,type)
        {
            var query = {};
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                query.approved_by = $localStorage.currentUser.info.addedBy;
            }else{
                query.approved_by = $localStorage.currentUser.info._id;
            }
               
            query.searchData = searchQuery;
            query.type = type;
            ReffererService.searchRefferer(query, function (result) {

                if (result.length > 0) {

                     if(type == "all"){
                            vm.alluser =result;
                            countRefferers("all");
                        }else{
                            vm.user =result;
                            countRefferers("accepted");
                        }

                    toastr.success('Success !');
                   
                } else if(result.length === 0){
                    toastr.error('No referrer found !'); 
                   
                }else{
                    toastr.error('No referrer found !'); 
                }
            });
        }


        function allcoupons() {
           console.log($state.current.name);
           vm.conditions.skip = ((vm.currentPage - 1) * vm.itemsPerPage);
           vm.conditions.limit = 5;
           vm.conditions.referrerId = $stateParams.refferer_id;
           if($localStorage.currentUser.info.role == "s_owner") 
            {
                vm.conditions.businessOwner = $localStorage.currentUser.info.addedBy;
            }else{
                vm.conditions.businessOwner = $localStorage.currentUser.info._id;
            }
           
           ReffererService.allcoupons(vm.conditions, function (result) {
               vm.couponData =result;
            });
        }
        

        function pageChangedCoupon(){
            allcoupons();
        }


        function countCoupons(){
            vm.conditions.referrerId = $stateParams.refferer_id;
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                vm.conditions.businessOwner = $localStorage.currentUser.info.addedBy;
            }else{
                vm.conditions.businessOwner = $localStorage.currentUser.info._id;
            }
            ReffererService.countCoupons(vm.conditions, function (count) {
                    vm.totalItems  = count;
                    vm.noofPages  = Math.ceil(vm.totalItems / vm.itemsPerPage);
            });
        }

        function searchCoupon(searchQuery)
        {
            var query = {};
            query.referrerId = $stateParams.refferer_id;
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                query.businessOwner = $localStorage.currentUser.info.addedBy;
            }else{
                query.businessOwner = $localStorage.currentUser.info._id;
            }
            query.searchData = searchQuery;
            ReffererService.searchCoupon(query, function (result) {

                if(result.length > 0) {
                    vm.couponData =result;
                    countCoupons();
                       
                    toastr.success('Success !');
                   
                } else if(result.length === 0){
                    toastr.error('No referrer found !'); 
                   
                }else{
                    toastr.error('No referrer found !'); 
                }
            });
        }


        function totalEarning()
        {
            var query = {};
            query.referrerId = $stateParams.refferer_id_profile;
            query.pageNo = 1;
            query.limit = 1;
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                query.businessOwner = $localStorage.currentUser.info.addedBy;
            }else{
                query.businessOwner = $localStorage.currentUser.info._id;
            }
           
            ReffererService.totalEarning(query, function (result) {
                if(result.length > 0) {
                    console.log(result);
                    result[0].referrerEarning =  parseFloat(result[0].referrerEarning).toFixed(2);
                    vm.earning =result[0];
                } else if(result.length === 0){
                   // toastr.error('No earnings found !'); 
                   
                }else{
                    //toastr.error('No earnings found !'); 
                }
            });
        }


        function successRefferes()
        {
            var query = {};
            query.referrerId = $stateParams.refferer_id_profile;
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                query.businessOwner = $localStorage.currentUser.info.addedBy;
            }else{
                query.businessOwner = $localStorage.currentUser.info._id;
            }
           
            ReffererService.successRefferes(query, function (result) {
               vm.successRefferer =result;
            });
        }

        function pendingRefferes()
        {
            var query = {};
            query.referrerId = $stateParams.refferer_id_profile;
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                query.businessOwner = $localStorage.currentUser.info.addedBy;
            }else{
                query.businessOwner = $localStorage.currentUser.info._id;
            }
           
            ReffererService.pendingRefferes(query, function (result) {
               vm.pendingRefferes =result;
            });
        }

        function pendingEarning()
        {
            var query = {};
            query.referrerId = $stateParams.refferer_id_profile;
            if($localStorage.currentUser.info.role == "s_owner") 
            {
                query.businessOwner = $localStorage.currentUser.info.addedBy;
            }else{
                query.businessOwner = $localStorage.currentUser.info._id;
            }
            ReffererService.pendingEarning(query, function (result) {
               result[0].referrerEarning =  parseFloat(result[0].referrerEarning).toFixed(2);
               vm.pendingEarning =result[0];
            });
        }

        

        function changePaymentStatus()
        {
            console.log(vm.paymentStatus);
            if(vm.paymentStatus!="")
            {
                var query = {};
                query.couponCode = vm.paymentStatus;
                
                ReffererService.paymentStatus(query, function (result) {
                    console.log(result);
                    if(result==200)
                    {
                        toastr.success('Success !');
                    }else if(result==201){
                        toastr.error('Error ! Coupon is not reedemed or invalid'); 
                    }else{
                        toastr.error('Error ! Coupon is already paid'); 
                    }
                });
            
            }
        }


        
        
    }
})();
