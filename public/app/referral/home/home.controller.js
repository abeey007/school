(function () {
    'use strict';

    angular
            .module('referral-app')
            .controller('HomeController', HomeController);

    HomeController.$inject = ['$state','$http','$localStorage','AuthService','$timeout','$stateParams'];

    function HomeController($state, $http, $localStorage, AuthService,$timeout,$stateParams) {

        var vm = this;
        vm.isAuthenticated = false;
        vm.init = init;
  
        
        init();

        // Initialisation
        function init() {

            if( $localStorage.currentUser){
                $rootScope.loggeduser = $localStorage.currentUser.info;
            }

            if($stateParams.token)
            {
                verifyEmail();
            }

            
            
        }

        function verifyEmail(){
            return AuthService.verifyEmail($stateParams.token, function (result) {
                console.log("result:",result);
                return result;
            });
        }
       
    }

})();


