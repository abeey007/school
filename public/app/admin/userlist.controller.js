(function () {
    'use strict';

    angular
            .module('adminapp')
            .controller('UserlistController', UserlistController);

    UserlistController.$inject = ['$state','$timeout','$stateParams','AuthService','toastr'];

    function UserlistController($state, $timeout, $stateParams,AuthService,toastr) {
        
       var vm = this;
        vm.isAuthenticated = false;
        vm.animationsEnabled = true;
        vm.init = init;
        vm.viewAllUser = viewAllUser;
        vm.searchUser = searchUser;
        vm.user = [];
        vm.reffer = [];
        vm.total_sales_owner = '';
        vm.total_offer = '';
       
        
        vm.maxsize = 20;
        vm.itemsPerPage = 20;
        vm.currentPage =1;
        vm.pageChanged = pageChanged;
        //vm.search_string = '';
        vm.viewUserInfo = viewUserInfo;
        vm.viewReffererInfo = viewReffererInfo;
        vm.changeBusnessOwnerStatus = changeBusnessOwnerStatus;
        init();
       
        // Initialisation
        function init() { 
        viewAllUser();
        viewUserInfo();
        viewReffererInfo(); 
        }

        /*View All User*/
         function viewAllUser(page) {
            //console.log("after ",page);
            AuthService.viewAllUser({page:page},function (result) {
                //console.log("sssss",result);
                vm.search_string = '';
                vm.user = result.data;
                vm.totalrefers = result.totaluser;
                vm.noofPages = Math.ceil(vm.totalrefers /vm.maxsize);
            });
        }

        /*Search reffre user*/

        function searchUser(search_string) {
             vm.currentPage =1;
             //console.log(search_string);
             vm.user = '';
            AuthService.searchUser({"search_string":search_string},function (result) {
                //console.log("sssss",result);
                vm.user = result;

                if (result.length > 0) { 
                   vm.user = result;
                }else{
                    toastr.error('No Record found !'); 
                }
            });
        }


        /*get reffet user info*/

        function viewUserInfo() {
             
             console.log("hi state",$stateParams.id);             
            AuthService.viewUserInfo({id: $stateParams.id},function (result) {
                //console.log("sssss",result.data.length);
                vm.reffer = result.data;
                vm.total_reffered = result.total_reffered;
                vm.total_sales_owner = result.total_sales_owner;
                vm.total_offer = result.total_offer;                
            });
        }

        function pageChanged(page){
            //console.log('hdhdhdhdhddhdh',page);
            vm.viewAllUser(page);
        }





        function viewReffererInfo() {
             
             console.log("hi state",$stateParams.id);             
            AuthService.viewReffrererInfo({id: $stateParams.id},function (result) {
                //console.log("sssss",result.data.length);
                vm.reffer = result.data;
                vm.sucess_reffer = result.sucess_reffer;
                vm.pending_reffer = result.pending_reffer;
                vm.total_earning = result.total_earning;                           
                /*
                if (result.data.length > 0) { 
                   vm.reffer = result.data;
                    
                   }else{
                    toastr.error('No Record found !'); 
                }*/
            });
        }

        /*CHANGE USER STATUS AS ACTIVE/INACTIVE*/

        function changeBusnessOwnerStatus(id,userStatus,salesowner) {
             
             //console.log(userStatus);
             //console.log('tetete',salesowner);
             //console.log(id);
             vm.b_owner = '';
            AuthService.changeBusnessOwnerStatus({id:id,userStatus:userStatus,salesowner:salesowner},function (result) {
                //console.log("sssss",result);
                toastr.success('Updated Successfully !');
                if(salesowner =='3'){
                    viewAllUser(); 
                }
                
            });
        }


        








    }

     

})();

  