(function () {
    'use strict';
    angular
            .module('adminapp')
            .directive('ckEditor', ckEditor);
    
    ckEditor.$inject = [];
    
    function ckEditor() {
        return {
            require: '?ngModel',
            link: function(scope, elm, attr, ngModel) {
              var ck = CKEDITOR.replace(elm[0]);
              //console.log("aaaa",ck);
              if (!ngModel) return;

              ck.on('pasteState', function() {
                scope.$apply(function() {
                  ngModel.$setViewValue(ck.getData());
                });
              });

              ngModel.$render = function(value) {
                //console.log("aaaaaaa");
                ck.setData(ngModel.$viewValue);
              };
            }
          };
    };
})();

