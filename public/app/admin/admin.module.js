(function () {
    'use strict';

    angular.module('adminapp',
            [
                'ngAnimate', 'ngSanitize', 'ui.router', 'ngStorage', 'ngFileUpload', 'ui.bootstrap','naif.base64','toastr'
            ])

            .factory('authInterceptor', ['$localStorage', authInterceptor])
            .config(routeConfig)
            .run(runConfig);

    routeConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider'];
    runConfig.$inject = ['$rootScope', '$state', '$http', '$localStorage'];

    function authInterceptor($localStorage) {

        return {
            request: function (config) {
                config.headers = config.headers || {};
                if ($localStorage.adminUser) {
                    config.headers.Authorization = 'JWT ' + $localStorage.adminUser.token;
                }
                return config;
            }
        };

    }


    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $location) {
        $locationProvider.hashPrefix("!");
        $httpProvider.interceptors.push('authInterceptor');
        var isLoggedIn = function($q, $timeout, $location, $localStorage) {
            // Initialize a new promise
            var deferred = $q.defer();

            if ($localStorage.adminUser)
                $timeout(deferred.resolve);
            else {
                $timeout(deferred.reject);
                $location.url('/');
            }
            return deferred.promise;
        };
        $stateProvider
                .state('anon', {
                    abstract: true,
                    template: '<ui-view/>',
                    data: {
                        access: 3
                    }
                })
                .state('anon.login', {
                    url: '/',
                    templateUrl: '/app/admin/login.html',
                    controller: 'AdminController',
                    controllerAs: 'vm'
                })
                .state('anon.forgotpwd', {
                    url: '/forgot-pwd',
                    templateUrl: '/app/admin/forgotPassword.html',
                    controller: 'HeaderController',
                    controllerAs: 'vm',
                    resolve: {
                       access: function($localStorage){
                           if($localStorage.adminstatus === true){
                               $location.path('/dashboard');
                           }
                       }
                    }
                })
                 .state('anon.resetpwd', {
                    url: '/reset-password/:token',
                    templateUrl: '/app/admin/resetPassword.html',
                    controller: 'HeaderController',
                    controllerAs: 'vm',
                    resolve: {
                       access: function($localStorage){
                           if($localStorage.adminstatus === true){
                               $location.path('/dashboard');
                           }
                       }
                    }
                })
                ;

        $stateProvider
                .state('admin', {
                    abstract: true,
                    template: '<ui-view/>',
                    data: {
                        access: 2
                    }
                })
                .state('admin.home', {
                    url: '/',
                    templateUrl: '/app/admin/dashboard.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
                .state('admin.dashboard', {
                    url: '/dashboard',
                    templateUrl: '/app/admin/dashboard.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
                .state('admin.owners', {
                    url: '/owners',
                    templateUrl: '/app/admin/owners.html',
                    //controller: 'DashboardController',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
                .state('admin.pages', {
                    url: '/pages',
                    templateUrl: '/app/admin/pages.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
                .state('admin.plans', {
                    url: '/plans',
                    templateUrl: '/app/admin/plans.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
                .state('admin.settings', {
                    url: '/settings',
                    templateUrl: '/app/admin/settings.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
                .state('admin.profile', {
                    url: '/profile',
                    templateUrl: '/app/admin/profile.html',
                    controller: 'HeaderController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
                .state('admin.userlist', {
                    url: '/userlist',
                    templateUrl: '/app/admin/user-list.html',
                    controller: 'UserlistController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.searchuser', {
                    url: '/searchuser',
                    templateUrl: '/app/admin/user-list.html',
                    controller: 'UserlistController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.viewuserinfo', {
                    url: '/viewuserinfo/:id',
                    templateUrl: '/app/admin/view-user.html',
                    controller: 'UserlistController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })


                .state('admin.searchsalesowner', {
                    url: '/searchsalesowner',
                    templateUrl: '/app/admin/owners.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.businessowners', {
                    url: '/businessOwnerData',
                    templateUrl: '/app/admin/business-owners.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.searchbusinessowners', {
                    url: '/searchBusnessOwner',
                    templateUrl: '/app/admin/owners.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
                .state('admin.changebusinessownerstatus', {
                    url: '/changeBusnessOwnerStatus',
                    templateUrl: '/app/admin/owners.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.businessreffererlist', {
                    url: '/businessReffererListData/:id',
                    templateUrl: '/app/admin/business_reffers.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.offerlist', {
                    url: '/businessOwnersOffers/:id',
                    templateUrl: '/app/admin/offers.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.businessalesownerlist', {
                    url: '/businesssalesownerlist/:id',
                    templateUrl: '/app/admin/business_sales_owner.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.searchbusinessalesownerlist', {
                    url: '/searchBusnessSalesOwner',
                    templateUrl: '/app/admin/business_sales_owner.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.reffererinfo', {
                    url: '/reffererinfo/:id',
                    templateUrl: '/app/admin/view-refferer.html',
                    controller: 'UserlistController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })


                .state('admin.getpageinfo', {
                    url: '/getpageinfo/:id',
                    templateUrl: '/app/admin/privacy-policy.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.updatePage', {
                    url: '/updatePage',
                    templateUrl: '/app/admin/privacy-policy.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.getdashboardinfo', {
                    url: '/getdashboardinfo',
                    templateUrl: '',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                /*30-5-17-*/
                .state('admin.offerreferrdetails', {
                    url: '/offerreferrdetailsData/:id',
                    templateUrl: '/app/admin/offer-referr-view.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                /*30-5-17-*/
                .state('admin.offerreferrlist', {
                    url: '/offerreferrlistData/:id',
                    templateUrl: '/app/admin/offer-referr.html',
                    controller: 'OwnerController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                /*REPORT SECTION*/
                .state('admin.viewreport', {
                    url: '/viewreport/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                 /*REPORT SECTION*/
                .state('admin.totalReff', {
                    url: '/totalReff/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.totalBusinessReff', {
                    url: '/totalBusinessReff/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.totalBusinessReffPending', {
                    url: '/totalBusinessReffPending/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.pendingEarnings', {
                    url: '/pendingEarnings/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.exportOverallReferal', {
                    url: '/exportOverallReferal/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                .state('admin.acceptedRefferers', {
                    url: '/acceptedRefferers/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                 .state('admin.allOffers', {
                    url: '/allOffers/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                  .state('admin.allSalesPerson', {
                    url: '/allSalesPerson/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                   .state('admin.viewReport', {
                    url: '/viewReport/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                   .state('admin.viewEarningReport', {
                    url: '/viewEarningReport/:id',
                    templateUrl: '/app/admin/reports.html',
                    controller: 'ReportController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                  .state('admin.getMembershipPlans', {
                    url: '/getMembershipPlans',
                    templateUrl: '/app/admin/membership.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })

                  .state('admin.getMembershipPlansInfo', {
                    url: '/getMembershipPlansInfo/:id',
                    templateUrl: '/app/admin/membership_plan_info.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })


                  .state('admin.updatePlaninfo', {
                    url: '/updatePlaninfo/:id',
                    templateUrl: '/app/admin/membership_plan_info.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })


                  .state('admin.CanceledBusinessPlan', {
                    url: '/CanceledBusinessPlan',
                    templateUrl: '/app/admin/CanceledBusinessPlan.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    resolve: {
                        isLoggedIn : isLoggedIn
                    }
                })
  
  

  




        
                ;
        $urlRouterProvider.otherwise('/');
    }

    function runConfig($rootScope, $state, $http, $localStorage) {
        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            window.scrollTo(0, 0);
             $rootScope.adminstatus = $localStorage.adminstatus;
//            $rootScope.adminstatus = true;

            if($state.current.name === 'anon.login') {
                $rootScope.adminstatus = false;
                $localStorage.adminstatus = false;
                delete $localStorage.adminUser;
            }
        });

        // redirect to login page if not logged in and trying to access a restricted page
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            $rootScope.adminstatus = $localStorage.adminstatus;
//            var restrictedPage = toState.name.indexOf('admin') > -1;
//            if (restrictedPage && !$localStorage.adminUser ) {
//                event.preventDefault();
//                $state.go('anon.login');
//            } 
//            
        });
    }

})();