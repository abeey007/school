(function () {
    'use strict';

    angular
            .module('adminapp')
            .controller('AdminController', AdminController);

    AdminController.$inject = ['$state', '$timeout', 'AuthService','$localStorage'];

    function AdminController($state, $timeout, AuthService, $localStorage) {

        var vm = this;
        vm.isAuthenticated = false;
        vm.animationsEnabled = true;
        vm.init = init;
        vm.loginstaff = loginstaff;
        init();

        // Initialisation
        function init() {

        }
        function loginstaff() {
            AuthService.loginStaff(vm.user, function (result) {
                console.log(result);
                if (result === true) {
                    $state.go('admin.dashboard');   
//                    $state.reload();
                } else {
                    vm.status = {class: 'alert alert-danger fade in', message: "Authentication failed! Please try again."};
                }
            }
            );
        }
       


    }

})();
