(function () {
    'use strict';

    angular
            .module('adminapp')
            .factory('AuthService', AuthService);

    AuthService.$inject = ['$http','$localStorage','$state', '$rootScope'];
    function AuthService($http, $localStorage, $state, $rootScope) {

        var service = {
            loginStaff: loginStaff,
            logoutStaff : logoutStaff,
            forgotpwd: forgotpwd,
            resetpwd: resetpwd,
            updateprofile: updateprofile,
            uploadpic: uploadpic,
            viewAllUser: viewAllUser,
            viewAllOwner:viewAllOwner,
            searchUser:searchUser,
            viewUserInfo: viewUserInfo,
            searchSalesOwner: searchSalesOwner,
            businessOwnerData : businessOwnerData,
            searchBusnessOwner:searchBusnessOwner,
            changeBusnessOwnerStatus: changeBusnessOwnerStatus,
            businessReffererListData: businessReffererListData,
            searchbusinessReffererListData: searchbusinessReffererListData,
            allbusinessOfferList:allbusinessOfferList,
            businessSalesOwnerList:businessSalesOwnerList,
            searchbusinessSalesOwnerList:searchbusinessSalesOwnerList,
            viewReffrererInfo:viewReffrererInfo,
            getpages:getpages,
            getpageinfo:getpageinfo,
            updatePage:updatePage,
            getdashboardinfo:getdashboardinfo,
            offerreferrdetailsData:offerreferrdetailsData,
            offerreferrlistData:offerreferrlistData,
            viewReport:viewReport,
            totalReff:totalReff,
            totalBusinessReff:totalBusinessReff,
            totalBusinessReffPending:totalBusinessReffPending,
            pendingEarnings:pendingEarnings,
            exportOverallReferal:exportOverallReferal,
            allSalesPerson:allSalesPerson,
            acceptedRefferers:acceptedRefferers,
            allOffers:allOffers,
            viewEarningReport:viewEarningReport,
            getMembershipPlans:getMembershipPlans,
            getMembershipPlansInfo :getMembershipPlansInfo,
            updatePlaninfo:updatePlaninfo,
            CanceledBusinessPlan:CanceledBusinessPlan
        };

        return service;

        function loginStaff(postData, callback) {
            if(postData){
                callback(true);
            }
            $http.post(webservices.stafflogin, postData)
                    .then(function (response) {
                        console.log(response.data);
                        if (response.data.token) {
                            $localStorage.adminUser = {token: response.data.token, data:response.data.data};
                            $localStorage.adminstatus = true;
                            if(response.data.data){
                                $rootScope.loggedAdmin = response.data.data;
                            }
                            $http.defaults.headers.common.Authorization = 'JWT ' + response.data.token;
                            callback(true);
                        } else {
                            callback(false);
                        }
                    });
        };
        
        function logoutStaff() {
            delete $localStorage.adminUser;
            delete $localStorage.adminstatus;
            $rootScope.adminstatus = false;
            $http.defaults.headers.common.Authorization = undefined;
            $state.go('anon.login');
        };
        
        function forgotpwd(postData, callback) {
            $http.post(webservices.forgotpwd, postData)
                .then(function (successCallback, errorCallback) {
                    console.log(successCallback.data)
                    if (successCallback.data.status === 200) {
                            callback(true);
                    } else {
                            callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        function resetpwd(postData, callback) {
            $http.post(webservices.resetpwd, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi reset",successCallback.data)
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        function updateprofile(postData, callback) {
            $http.post(webservices.updateprofile, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }
        
         function uploadpic(postData, callback) {
            $http.post(webservices.uploadpic, postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function viewAllUser(postData,callback) {
            $http.post(webservices.userlist,postData)
                .then(function (successCallback, errorCallback) {
                    //console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                       //callback(successCallback.data.data); to get no of pages
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }




        function searchUser(postData,callback) {
            $http.post(webservices.searchuser,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function viewUserInfo(postData,callback) {
            $http.post(webservices.viewuserinfo,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }



         function viewAllOwner(postData,callback) {
            $http.post(webservices.owners,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function searchSalesOwner(postData,callback) {
            $http.post(webservices.searchsalesowner,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }



        function businessOwnerData(postData,callback) {
            $http.post(webservices.businessowners,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("bowner ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function searchBusnessOwner(postData,callback) {
            $http.post(webservices.searchbusinessowners,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function changeBusnessOwnerStatus(postData,callback) {
            $http.post(webservices.changebusinessownerstatus,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function businessReffererListData(postData,callback) {
            $http.post(webservices.businessreffererlist,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Heleleeleel ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function searchbusinessReffererListData(postData,callback) {
            $http.post(webservices.searchbusinessreffererlist,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("hellog ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function allbusinessOfferList(postData,callback) {
            $http.post(webservices.offerlist,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("hellog ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function businessSalesOwnerList(postData,callback) {
            $http.post(webservices.businesssalesownerlist,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("hellog ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function searchbusinessSalesOwnerList(postData,callback) {
            $http.post(webservices.searchbusinesssalesownerlist,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("search-i ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function viewReffrererInfo(postData,callback) {
            $http.post(webservices.reffererinfo,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function getpages(postData,callback) {
            $http.post(webservices.getpages,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function getpageinfo(postData,callback) {
            $http.post(webservices.getpageinfo,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function updatePage(postData,callback) {
            //console.log('hsdsdsahjdask',postData);
            $http.post(webservices.updatePage,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                        //callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function getdashboardinfo(callback) {
            $http.get(webservices.getdashboardinfo)
                .then(function (successCallback, errorCallback) {
                    console.log("Hi ",successCallback)
                    if (successCallback.data.status === 200) {
                        //callback(successCallback.data.data);
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        /*30-5-17-*/

        function offerreferrdetailsData(postData,callback) {
            $http.post(webservices.offerreferrdetails,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("offer details ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function offerreferrlistData(postData,callback) {
            $http.post(webservices.offerreferrlist,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("offer details ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function viewReport(postData,callback) {
            $http.post(webservices.viewreport,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("reports ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

         function totalReff(postData,callback) {
            //console.log('poooooo',postData);
            $http.post(webservices.totalReff,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("totalReff ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function totalBusinessReff(postData,callback) {
            //console.log('jdjdhjdhjdjhdjhddjjhd',postData);
            $http.post(webservices.totalBusinessReff,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("totalBusinessReff ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

         function totalBusinessReffPending(postData,callback) {
            $http.post(webservices.totalBusinessReffPending,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("totalBusinessReff ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function pendingEarnings(postData,callback) {
            $http.post(webservices.pendingEarnings,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("earning ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function exportOverallReferal(postData,callback) {
            $http.post(webservices.exportOverallReferal,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("download function",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function acceptedRefferers(postData,callback) {
            $http.post(webservices.acceptedRefferers,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("download ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function allOffers(postData,callback) {
            console.log('checjkin',postData);
            $http.post(webservices.allOffers,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("download ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


         function allSalesPerson(postData,callback) {            
            $http.post(webservices.allSalesPerson,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("download ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function viewReport(postData,callback) {
            $http.post(webservices.viewReport,postData)
                .then(function (successCallback, errorCallback) {
                    console.log("download ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function viewEarningReport(postData,callback) {
            //console.log('newpostdata',postData);
            $http.post(webservices.viewEarningReport,postData)
                .then(function (successCallback, errorCallback) {
                    //console.log("download ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        /*memeber ship plans*/

         function getMembershipPlans(postData,callback) {
            //console.log('newpostdata',postData);
            $http.post(webservices.getMembershipPlans,postData)
                .then(function (successCallback, errorCallback) {
                    //console.log("download ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

        function getMembershipPlansInfo(postData,callback) {
            //console.log('newpostdata',postData);
            $http.post(webservices.getMembershipPlansInfo,postData)
                .then(function (successCallback, errorCallback) {
                    //console.log("download ",successCallback)
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }

         function updatePlaninfo(postData,callback) {
            console.log('postingplan',postData);
            $http.post(webservices.updatePlaninfo,postData)
                .then(function (successCallback, errorCallback) {                    
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }


        function CanceledBusinessPlan(postData,callback) {
            console.log('CanceledBusinessPlan',postData);
            $http.post(webservices.CanceledBusinessPlan,postData)
                .then(function (successCallback, errorCallback) {                    
                    if (successCallback.data.status === 200) {
                        callback(successCallback.data);
                    } else {
                        callback(false);
                    }
                    if (errorCallback) {
                        console.log(errorCallback);
                    }
                });     
        }













    }

})();