(function () {
    'use strict ';

    angular
            .module('adminapp')
            .controller('ReportController', ReportController);

    //ReportController.$inject = ['$state', '$rootScope', 'AuthService', 'ReportService', '$localStorage', 'toastr','$compile','$stateParams','$location',"$sce"];


   // function ReportController( $state, $rootScope, AuthService, ReportService, $localStorage, toastr,$compile,$stateParams,$location,$sce) {
ReportController.$inject = ['$state','$timeout','$stateParams','AuthService','toastr'];

    function ReportController($state, $timeout, $stateParams,AuthService,toastr) {
        var vm = this;
        vm.init = init;

        vm.users = {};
        vm.offers ={};
        vm.sales = {};

        vm.open1 = open1;
        vm.open2 = open2;

        vm.totalReffs = totalReffs;
        vm.totalReffsVar = {};

        vm.totalBusinessReff = totalBusinessReff;
        vm.totalbusinessRefVar = {};
        //vm.totalbusinessRefVar.referrerEarning =vm.totalbusinessRefVar.referrerEarning;
        vm.totalbusinessRefVar.referrerEarning = {};


        vm.totalBusinessPendingVar = "";
        vm.totalPendingEarningVar = "";

        vm.exportOverallReferal  = exportOverallReferal;

        vm.tab2Functions = tab2Functions;
        vm.acceptedRefferers = acceptedRefferers;

        vm.viewReport = viewReport;
        vm.viewEarningReport = viewEarningReport;
        vm.reportResponse = {};


        vm.popup1 = { opened: false };
        vm.popup2 = { opened: false };
        vm.format = 'MM-dd-yyyy';
        vm.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            startingDay: 1
          };

        vm.dateOptions2 = {
            formatYear: 'yy',
            maxDate: new Date(2020, 5, 22),
            startingDay: 1
          };    
        
        init();
        // Initialisation
        function init() {
            console.log("asfasfaf");          
            $( "#accordion" ).accordion();
           
            acceptedRefferers();
            totalReffs();
            totalBusinessReff();
            totalBusinessReffPending();          

        }


        function totalReffs()
        {

            AuthService.totalReff({id:$stateParams.id},function (result) {
                //console.log("sssss",result);
                vm.totalReffsVar = result.data;
                //console.log("sddasdasdadad",vm.totalReffsVar);
            });
        }


        function totalBusinessReff()
        {
            AuthService.totalBusinessReff({id:$stateParams.id},function (result) {
                //console.log('hellotestdata',result);
                vm.totalbusinessRefVar = result.data[0];
                vm.totalbusinessRefVar.referrerEarning = parseFloat(vm.totalbusinessRefVar.referrerEarning).toFixed(2);
            });

        }


        function totalBusinessReffPending()
        {
            var query = {};
            query.id = $stateParams.id;
            query.cond = "pending";
             AuthService.totalBusinessReffPending(query,function (result) {

                 vm.totalBusinessPendingVar = result.data;
            });

        }

        function pendingEarnings()
        {
            var query = {};
            query.id = $stateParams.id;
            query.type = "business";
            AuthService.pendingEarnings(query, function (result) {                
               result[0].referrerEarning =  parseFloat(result[0].referrerEarning).toFixed(2);
               vm.totalPendingEarningVar =result[0];
            });
        }

        function exportOverallReferal(data1,data2,data3,data4,data5){
            var query = {};
            query.data1 = data1;
            query.data2 = data2;
            query.data3 = data3;
            query.data4 = data4;
            query.data5 = data5;

            AuthService.exportOverallReferal(query, function (result) {
               console.log(result);
               window.location = "/downloadcsv/"+result.data;
            });           
        }

        function open1 () {
            vm.popup1.opened = true;
        };
        
        function open2 () {
            vm.popup2.opened = true;
        };


        function tab2Functions(){
            acceptedRefferers();
            allOffers();
            allSalesPerson();

        }

        function acceptedRefferers()
        {            
           var query = {};
           query.approved_by = $stateParams.id;
           
            AuthService.acceptedRefferers(query , function (result) {
               vm.users =result.data;               
            });
        }


        function allOffers()
        {           
           var query = {};
           query.userId = $stateParams.id;
           
            AuthService.allOffers(query , function (result) {
               console.log("rrr",result);
               vm.offers =result.data;
            });
        }


        function allSalesPerson()
        {         
           var query = {};
           query.addedBy = $stateParams.id;
           query.pagination = false;
           
            AuthService.allSalesPerson(query , function (result) {                 
               vm.sales = result;
            });
        }


        function viewReport()
        {
            vm.report.businessOwner = $stateParams.id;          
            AuthService.viewReport(vm.report , function (result) {
                //console.log(result);
               if(result.data=="empty"){
                    toastr.error('Error ! No result found.');
               }else{
                    window.location = "/downloadcsv/"+result.data;
               }
            });

        }

        function viewEarningReport()
        {
            vm.report.businessOwner = $stateParams.id;
            console.log(vm.report);
            AuthService.viewEarningReport(vm.report , function (result) {
               //console.log("bdbddbdbddbd",result);
               if(result.data=="empty"){
                    $("#resultReports").hide();
                    toastr.error('Error ! No result found.');
               }else{

                   $("#resultReports").show(); 
                    vm.reportResponse = result.data;
                    vm.reportResponse.dataEarning[0].referrerEarning =  parseFloat( vm.reportResponse.dataEarning[0].referrerEarning).toFixed(2); 
                    vm.reportResponse.dataPending[0].referrerEarning =  parseFloat( vm.reportResponse.dataPending[0].referrerEarning).toFixed(2);
                   window.location = "/downloadcsv/"+result.data.csvFileName;
               }
            });

        }

        
       
        
    }
})();
