var baseUrl = "";
var webservices = {
    "stafflogin": baseUrl + "/admin/login",
    "forgotpwd": baseUrl + "/admin/forgot-password",
    "resetpwd": baseUrl + "/admin/reset-password/:token",
    "updateprofile": baseUrl + "/admin/update-user",
    "uploadpic": baseUrl + "/admin/upload-profilepic",
    /*NEW ADDED MEthod*/ 
    "userlist": baseUrl + "admin/user-list",
    "searchuser": baseUrl + "admin/search-user",
    "owners": baseUrl + "admin/owner-list",
    "viewuserinfo": baseUrl + "admin/user-info",

    "reffererinfo": baseUrl + "admin/refferer-info",

    /*SALES OWNER (s-owners)*/
    "searchsalesowner": baseUrl + "admin/search-s-owner",
    "businessowners": baseUrl + "admin/business-owner-list",
    "searchbusinessowners": baseUrl + "admin/search-business-owner",
    "changebusinessownerstatus": baseUrl + "admin/updte-status-business-owner",
    "businessreffererlist": baseUrl + "admin/business-referer-list",
    "searchbusinessreffererlist": baseUrl + "admin/search-business-referer-list",
    "offerlist": baseUrl + "admin/offers-list",
    /*NEW ADDED 31-5-17*/
    "offerreferrdetails": baseUrl + "admin/offer-referr-details",
    "offerreferrlist": baseUrl + "admin/offer-referr-list",

    "businesssalesownerlist": baseUrl + "admin/business-sales-owner-list",
    "searchbusinesssalesownerlist": baseUrl + "admin/search-business-sales-owner-list",

    /*Pages(content pages)*/
    "getpages": baseUrl + "admin/getpages",
    "getpageinfo": baseUrl + "admin/getpageinfo",
    "updatePage": baseUrl + "admin/update-Page",
    "getdashboardinfo": baseUrl + "admin/getdashboardinfo",

    /*REPORT SECTION*/
     
     "totalReff": baseUrl + "admin/totalReff",
     "totalBusinessReff": baseUrl + "admin/totalBusinessReff",
     "totalBusinessReffPending": baseUrl + "admin/totalBusinessReffPending",
     "pendingEarnings": baseUrl + "admin/pendingEarnings",
     /*---------*/
     "exportOverallReferal": baseUrl + "admin/exportOverallReferal",
    "acceptedRefferers": baseUrl + "admin/acceptedRefferers",
    "allOffers": baseUrl + "admin/allOffers",
    "allSalesPerson": baseUrl + "admin/allSalesPerson",

    "viewReport": baseUrl + "admin/viewReport",
    "viewEarningReport": baseUrl + "admin/viewEarningReport",

    /******************Membership**************/
    "getMembershipPlans": baseUrl + "admin/getMembershipPlans",
    "getMembershipPlansInfo": baseUrl + "admin/getMembershipPlansInfo",
    "updatePlaninfo": baseUrl + "admin/updatePlaninfo",
    "CanceledBusinessPlan": baseUrl + "admin/CanceledBusinessPlan",






};
