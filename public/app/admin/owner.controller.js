(function () {
    'use strict';

    angular
            .module('adminapp')
            .controller('OwnerController', OwnerController);

    OwnerController.$inject = ['$state','$timeout','$stateParams','AuthService','toastr'];

    function OwnerController($state, $timeout,$stateParams, AuthService,toastr) {
        
       var vm = this;
        vm.isAuthenticated = false;
        vm.animationsEnabled = true;
        vm.init = init;
        vm.viewAllOwner = viewAllOwner;
        vm.searchSalesOwner = searchSalesOwner;
        vm.businessOwners = businessOwners;
        vm.searchBusnessOwner = searchBusnessOwner;
        vm.changeBusnessOwnerStatus = changeBusnessOwnerStatus;
        vm.owner = [];
        vm.b_owner =[];
        vm.r_owner =[];
        vm.r_offer =[];
        vm.s_owner =[];
        vm.offer_refer_view =[];
        vm.totalowner ='';
        vm.total_offer = '';
        vm.noofPages ='';
        vm.noofPages1 ='';
        vm.noofPages_o_r ='';
        vm.totalsalesowner='';
        
        vm.maxsize = 20;
        vm.itemsPerPage = 20;
        vm.currentPage = 1;

        vm.pageChangedOwner = pageChangedOwner;
        vm.pageChangedBusinessOwner = pageChangedBusinessOwner;
        vm.businessOwnersReffers = businessOwnersReffers;
        vm.searchbusinessOwnersReffers = searchbusinessOwnersReffers;
        vm.pagechangebusinessOwnersReffers = pagechangebusinessOwnersReffers;
        vm.businessOwnersOffers = businessOwnersOffers;
        vm.pageChangedOffers = pageChangedOffers;
        vm.businessSalesOwnersList = businessSalesOwnersList;
        vm.searchbusinessSalesOwnersList = searchbusinessSalesOwnersList;
        vm.pageChangedBusinessSalesOwner = pageChangedBusinessSalesOwner;
        vm.businessOwnersOfferReffers = businessOwnersOfferReffers;
        vm.businessOwnersOfferReffersView =businessOwnersOfferReffersView;
        init();
       
        // Initialisation
        function init() { 
        viewAllOwner(); 
        businessOwners();    
        vm.businessOwnersReffers();
        businessOwnersOffers();
        businessSalesOwnersList();
        businessOwnersOfferReffers();
        businessOwnersOfferReffersView();   
           
        }

         function viewAllOwner(page) {
            AuthService.viewAllOwner({page:page},function (result) {
                //console.log("sssss",result);
                vm.search_string = '';
                vm.owner = result.data; 
                vm.totalowner = result.totalowner;
                vm.noofPages = Math.ceil(vm.totalowner /vm.maxsize);
            });
        }


         /*Search reffre user*/

        function searchSalesOwner(search_string) {
             
             console.log(search_string);
             vm.owner = '';
             vm.currentPage = 1;
            AuthService.searchSalesOwner({"search_string":search_string},function (result) {
                //console.log("sssss",result);
                vm.owner = result.data;

                if (result.data.length > 0) { 
                   //vm.owner = result;
                   vm.owner = result.data; 
                   vm.totalowner = result.totalowner;
                   //vm.noofPages = Math.ceil(vm.totalowner /vm.maxsize);
                }else{
                    toastr.error('No Record found !'); 
                }
            });
        }

         function businessOwners(page) {
             vm.b_owner = '';             
            AuthService.businessOwnerData({page:page},function (result) {
                //console.log("vxvxvxvx",result);
                vm.b_owner = result.data;                
                vm.ownercount_data = result.ownercount;
                vm.noofPages1 = Math.ceil(vm.ownercount_data / vm.maxsize);
                console.log("vxvxvxvx",vm.noofPages1);
            });
        }


         function searchBusnessOwner(search_string) {
             
             console.log(search_string);
             vm.b_owner = '';             
             vm.noofPages1 ='';
             vm.currentPage = 1;
            AuthService.searchBusnessOwner({"search_string":search_string},function (result) {
                //console.log("sssss",result);
                  //vm.b_owner = result.data;

                if (result.data.length > 0) { 
                vm.b_owner = result.data;                
                vm.ownercount_data = result.owner_count;
                vm.noofPages1 = Math.ceil(vm.ownercount_data / vm.maxsize);
                }else{
                    toastr.error('No Record found !'); 
                }
            });
        }

        function changeBusnessOwnerStatus(id,userStatus,salesowner) {
             
             console.log(userStatus);
             console.log('tetete',salesowner);
             console.log(id);
             vm.b_owner = '';
            AuthService.changeBusnessOwnerStatus({id:id,userStatus:userStatus,salesowner:salesowner},function (result) {
                //console.log("sssss",result);
                toastr.success('Updated Successfully !');
                if(salesowner =='1'){
                    viewAllOwner(); 
                }else{
                    businessOwners();  
                }
                
            });
        }

       

         function businessOwnersReffers(page) {
            //console.log("hello",$stateParams);
            vm.search_string ='';
            AuthService.businessReffererListData({id: $stateParams.id, page: page},function (result) {
                console.log("dtfound",result);
                vm.r_owner = result.data;                
                vm.totalowner_ref = result.totalowner;
                vm.noofPages2 = Math.ceil(vm.totalowner_ref /vm.maxsize);

            });
        }


         function searchbusinessOwnersReffers(search_string) {
            //console.log("hello",$stateParams);
            vm.search_string ='';
            vm.r_owner ='';
            vm.currentPage = 1;
            AuthService.searchbusinessReffererListData({id: $stateParams.id,search_string:search_string},function (result) {
                console.log("dtfound",result);
                vm.r_owner = result.data;                
                vm.totalowner_ref = result.totalowner;
                vm.noofPages2 = Math.ceil(vm.totalowner_ref /vm.maxsize);

            });
        }




        function businessOwnersOffers(page) {
            //console.log("hello",$stateParams);
            vm.search_string ='';
            AuthService.allbusinessOfferList({id: $stateParams.id, page: page},function (result) {
                console.log("dtfound",result);
                vm.r_offer = result.data;                
                vm.total_offer = result.total_offer;
                vm.noofPages3 = Math.ceil(vm.total_offer /vm.maxsize);

            });
        }



        function businessSalesOwnersList(page) {
            //console.log("hello",$stateParams);
            vm.search_string ='';
            AuthService.businessSalesOwnerList({id: $stateParams.id, page: page},function (result) {
                console.log("dtfound",result);
                vm.s_owner = result.data;                
                vm.totalsalesowner = result.totalowner;
                vm.noofPages4 = Math.ceil(vm.totalowner /vm.maxsize);

            });
        }


         function searchbusinessSalesOwnersList(search_string) {
             
             console.log(search_string); 
             vm.currentPage = 1;            
            AuthService.searchbusinessSalesOwnerList({"id": $stateParams.id,"search_string":search_string},function (result) {               
                vm.owner = result.data;

                if (result.data.length > 0) {                   
                   vm.s_owner = result.data;  
                   vm.totalowner = result.totalowner;
                   vm.noofPages4 = Math.ceil(vm.totalowner /vm.maxsize);
                }else{
                    toastr.error('No Record found !'); 
                }
            });
        }



        function businessOwnersOfferReffers(page) {
            //console.log("hello",$stateParams);
            vm.search_string ='';
            AuthService.offerreferrlistData({id: $stateParams.id, page: page},function (result) {
                console.log("offer-data",result);
                vm.offer_refer = result.data;                
                vm.totl_offer_ref = result.totl_offer_ref;
                vm.noofPages_o_r = Math.ceil(vm.totl_offer_ref /vm.maxsize);

            });
        }

         function businessOwnersOfferReffersView() {
            //console.log("hello",$stateParams);
            vm.search_string ='';
            AuthService.offerreferrdetailsData({id: $stateParams.id},function (result) {
                console.log("offer-data-view",result);
                vm.offer_refer_view = result.data; 
            });
        }



        function pageChangedOwner(page){
            vm.viewAllOwner(page);
        }

        function pageChangedBusinessOwner(page){
            vm.businessOwners(page);
        }



        function pagechangebusinessOwnersReffers(page){
            vm.businessOwnersReffers(page);
        }


        function pageChangedOffers(page){
            vm.businessOwnersOffers(page);
        }


        function pageChangedBusinessSalesOwner(page){
            vm.businessSalesOwnersList(page);
        }

        function pageChangedbusinessOwnersOfferReffers(page){
            vm.businessOwnersOfferReffers(page);
        }






    }

})();

  