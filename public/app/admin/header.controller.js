(function () {
    'use strict';

    angular
            .module('adminapp')
            .controller('HeaderController', HeaderController);

    HeaderController.$inject = ['$state', 'AuthService','$rootScope','$stateParams', '$localStorage','$timeout','toastr'];

    function HeaderController($state, AuthService, $rootScope, $stateParams, $localStorage, $timeout, toastr) {

        var vm = this;
        vm.init = init;
//        vm.loginstaff = loginstaff;
        vm.forgotpwd = forgotpwd;
        vm.resetPassword = resetPassword;
        vm.logout = logout;
        vm.onLoad = onLoad;
        vm.updateProfile = updateProfile;
        
        vm.forgotuser = {};
       
        vm.forgotPwd = {};
        vm.resetuser ={};
        vm.loginUser = {};
        init();
        vm.mngprofile = {}; 
        
        if($localStorage.adminUser){
            vm.user = $localStorage.adminUser.data;
        }
        
//        console.log("Logged user",vm.user);
        vm.userProfile = '';
        // Initialisation
        function init() {
            if( $localStorage.adminUser){
                $rootScope.loggedAdmin =  $localStorage.adminUser.data;
            }
        }
        
   
        
        function forgotpwd() {
            console.log(vm.forgotuser);
            AuthService.forgotpwd(vm.forgotuser, function (result) {
                console.log("response:",result)
                if (result === true) {
                    vm.forgotuser = {};
                    vm.forgotPwd.$setPristine();
                    vm.forgotPwd.$setUntouched();
                    toastr.success('Success! Please check your email.');
                   
                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }
        function resetPassword() {
            vm.status = {};
            vm.resetuser.token = $stateParams.token;
            AuthService.resetpwd(vm.resetuser, function (result) {
                if (result === true) {
                    vm.resetuser = {};
                    toastr.success('Success! Password updated successfully.');
                    $state.go('anon.login');
                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }
        function logout() {
            AuthService.logoutStaff();
        }
        function onLoad(file){
//            console.log("uploaded",file);
//            vm.userProfile = file.base64;
            AuthService.uploadpic({file: file.base64}, function (result) {
                if (result.status == 200) {
                    console.log("result:",result.data);
                    vm.userProfile = result.data;
                    vm.user.personalInfo.profileImg = vm.userProfile;
                    toastr.success('Success! Image uploaded successfully.');
                    
                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }
        function updateProfile(){
            if(vm.user.personalInfo.profileImg === ''){
                vm.user.personalInfo.profileImg = vm.userProfile;
            }
           
            AuthService.updateprofile(vm.user, function (result) {
                if (result === true) {
//                    vm.user = {};
                    toastr.success('Success! updated successfully.');
                    $state.go('admin.dashboard');
                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }
        
    }

})();
