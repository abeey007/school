(function () {
    'use strict';

    angular
            .module('adminapp')
            .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$state','$timeout','$stateParams','AuthService','toastr'];

    function DashboardController($state, $timeout,$stateParams, AuthService,toastr) {
        
        var vm = this;
        vm.isAuthenticated = false;
        vm.animationsEnabled = true;
        vm.getpagesInfo = getpagesInfo;
        vm.updatePage = updatePage;
        vm.init = init;
        vm.pages = [];
        vm.pagesinfo =[];
        vm.description ='';
        vm.pageForm = [];
        vm.dasborddata = [];
        vm.membershipplans =[];
        vm.membershipplansinfo=[];
        vm.updatePlaninfo = updatePlaninfo;
        vm.noofPages1 ='';
        vm.cancel_plan=[];
        vm.maxsize = 20;
        vm.itemsPerPage = 20;
        vm.currentPage = 1;
        vm.pageChangedCancelPlan =pageChangedCancelPlan;
        vm.CanceledBusinessPlan = CanceledBusinessPlan;
        
        vm.data = [
                {"name": "arun","email": "arun@gmail.com"},
                {"name": "arun","email": "arun@gmail.com"},
                {"name": "arun","email": "arun@gmail.com"},
                {"name": "arun","email": "arun@gmail.com"}
            ];
        init();
        
        // Initialisation
        function init() {
            getpages();
            getpagesInfo();
            getdashboardinfo();
            getmembership();
            getMembershipPlansInfo();
            CanceledBusinessPlan();
        }



        function getpages() {             
                        
            AuthService.getpages({id: $stateParams.id},function (result) {
                //console.log("sssss",result.data);
                vm.pages = result.data;
                                         
                /*
                if (result.data.length > 0) { 
                   vm.reffer = result.data;
                    
                   }else{
                    toastr.error('No Record found !'); 
                }*/
            });
        }


         function getpagesInfo() {                         
                      
            AuthService.getpageinfo({id:$stateParams.id},function (result) {
                console.log("ndndndndndn",result.data);
                vm.pagesinfo = result.data;
            });
        }


        function updatePage() {            
                                 
            AuthService.updatePage(vm.pagesinfo,function (result) { 

                toastr.success('Updated successfully !');
                //$state.go('admin.pages');
            });
        }


         function getdashboardinfo() {             
                        
            AuthService.getdashboardinfo(function (result) {
                //console.log("sssss",result.data);
                vm.dasborddata = result.data;
                vm.total_business_owner = result.totalb_owner;
                vm.total_refferer = result.totalrefferer;
                vm.total_sales_owner = result.totals_owner; 
            });
        }


        function getmembership() {             
                        
            AuthService.getMembershipPlans({id: $stateParams.id},function (result) {
                //console.log("ldldldldldldldldldl",result.data);
                vm.membershipplans = result.data;                                         
               
            });
        }


        function getMembershipPlansInfo() {             
                        
            AuthService.getMembershipPlansInfo({id: $stateParams.id},function (result) {
                //console.log("ldldldldldldldldldl",result.data);
                vm.membershipplansinfo = result.data;
                                         
                /*
                if (result.data.length > 0) { 
                   vm.reffer = result.data;
                    
                   }else{
                    toastr.error('No Record found !'); 
                }*/
            });
        }


         function updatePlaninfo() {            
                                 
            AuthService.updatePlaninfo(vm.membershipplansinfo,function (result) { 

                toastr.success('Updated successfully !');
                $state.go('admin.getMembershipPlans');
            });
        }


         function CanceledBusinessPlan(page) {
             vm.cancel_plan = '';             
            AuthService.CanceledBusinessPlan({page:page},function (result) {
                //console.log("bcbcbccbc",result);
                vm.cancel_plan = result.data;                
                vm.ownercount_data = result.ownercount;
                vm.noofPages1 = Math.ceil(vm.ownercount_data / vm.maxsize);
                //console.log("vxvxvxvx",vm.noofPages1);
            });
        }


         function pageChangedCancelPlan(page){
            vm.CanceledBusinessPlan(page);
        }








    }

})();

  