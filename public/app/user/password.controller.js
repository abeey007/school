(function () {
    'use strict';

    angular
            .module('referral-app')
            .controller('PasswordController', PasswordController);

    PasswordController.$inject = ['$state','$stateParams', 'AuthService', '$localStorage', 'toastr'];


    function PasswordController($state, $stateParams, AuthService, $localStorage, toastr) {

        var vm = this;
        vm.init = init;
        vm.resetUserPassword = resetUserPassword;
        vm.logout = logout;
        
        vm.resetuser = {
            newPassword: ''
        };
        
        init();

        // Initialisation
        function init() {
           vm.resetuser = {
               newPassword: ''
           };
        }
        
        function resetUserPassword() {
            console.log("aaaa");
            vm.resetuser.token = $stateParams.token;
            AuthService.resetpwd(vm.resetuser, function (result) {
                console.log("response reset:",result)
                if (result === true) {
                    vm.resetuser = {};
                    toastr.success('Success ! Please check your email and junk mail box as well too');
                    $state.go('anon.home');
                } else {
                    toastr.error('Error! Please try again.');
                }
            });
        }


        function logout() {
            AuthService.logoutUser();
        }
    }
})();


